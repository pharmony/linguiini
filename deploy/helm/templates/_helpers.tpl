{{/*
Expand the name of the chart.
*/}}
{{- define "linguiini.app-name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "linguiini.mailhog-name" -}}
{{- printf "%s-%s" (include "linguiini.app-name" .) "mailhog" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "linguiini.selenium-name" -}}
{{- printf "%s-%s" (include "linguiini.app-name" .) "selenium" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "linguiini.webpack-name" -}}
{{- printf "%s-%s" (include "linguiini.app-name" .) "webpack" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "linguiini.worker-name" -}}
{{- printf "%s-%s" (include "linguiini.app-name" .) "sidekiq" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "linguiini.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "linguiini.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "linguiini.app.labels" -}}
helm.sh/chart: {{ include "linguiini.chart" . }}
{{ include "linguiini.app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "linguiini.mailhog.labels" -}}
helm.sh/chart: {{ include "linguiini.chart" . }}
{{ include "linguiini.mailhog.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "linguiini.selenium.labels" -}}
helm.sh/chart: {{ include "linguiini.chart" . }}
{{ include "linguiini.selenium.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "linguiini.webpack.labels" -}}
helm.sh/chart: {{ include "linguiini.chart" . }}
{{ include "linguiini.webpack.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "linguiini.worker.labels" -}}
helm.sh/chart: {{ include "linguiini.chart" . }}
{{ include "linguiini.worker.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "linguiini.app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "linguiini.app-name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/role: web
{{- end }}

{{- define "linguiini.mailhog.selectorLabels" -}}
app.kubernetes.io/name: {{ include "linguiini.mailhog-name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/role: mailcatcher
{{- end }}

{{- define "linguiini.selenium.selectorLabels" -}}
app.kubernetes.io/name: {{ include "linguiini.selenium-name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/role: selenium
{{- end }}

{{- define "linguiini.webpack.selectorLabels" -}}
app.kubernetes.io/name: {{ include "linguiini.webpack-name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/role: webpack
{{- end }}

{{- define "linguiini.worker.selectorLabels" -}}
app.kubernetes.io/name: {{ include "linguiini.worker-name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/role: worker
{{- end }}

{{/*
Nginx
*/}}
{{- define "nginx.selectorLabels" -}}
app.kubernetes.io/name: ingress-nginx
app.kubernetes.io/instance: ingress-nginx
app.kubernetes.io/component: controller
{{- end }}

{{/*
Redis master
*/}}
{{- define "redis-master.selectorLabels" -}}
app.kubernetes.io/name: redis
app.kubernetes.io/component: master
{{- end }}

{{/*
Redis replicas
*/}}
{{- define "redis-replicas.selectorLabels" -}}
app.kubernetes.io/instance: redis-{{ .Values.helmEnv }}{{ .Values.gitlabMergeRequestIID }}
app.kubernetes.io/component: replica
{{- end }}

{{/*
RethinkDB proxy
*/}}
{{- define "rethinkdb-proxy.selectorLabels" -}}
app: rethinkdb-proxy
release: rethinkdb-{{ .Values.helmEnv }}{{ .Values.gitlabMergeRequestIID }}
{{- end }}

{{/*
RethinkDB cluster pods
*/}}
{{- define "rethinkdb-cluster.selectorLabels" -}}
app: rethinkdb-cluster
release: rethinkdb-{{ .Values.helmEnv }}{{ .Values.gitlabMergeRequestIID }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "linguiini.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "linguiini.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
