# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '>= 2.0.0', '< 3'

# rails-react-devise-bootstrap repository dependencies, please don't touch them.
gem 'bootsnap', '~> 1.4.4', require: false
gem 'devise', '~> 4.7.0'
gem 'puma', '~> 4'
gem 'rails', '~> 6.1'
gem 'sass-rails', '~> 6.0'
gem 'uglifier', '>= 4.2.0'
gem 'webpacker', '~> 5.2.1'

# Add your dependencies after this lines.
# This will avoids conflicts when merging the upstream project.
gem 'aasm', '~> 5.1.1'
gem 'activejob-interactor', '~> 0.0.1'
gem 'activejob-uniqueness', '~> 0.2.5'
gem 'active_model_serializers', '~> 0.10.12'
gem 'changelog-notifier', '~> 1.5.0'
gem 'devise-i18n', '~> 1.9.2'
gem 'devise_invitable', '~> 2.0.3'
gem 'devise-nobrainer', '~> 0.5.0'
gem 'git', '~> 1.8.0'
gem 'gitlab', '~> 4.17'
gem 'interactor', '~> 3.1.2'
gem 'interactor-contracts', '~> 0.3.0'
gem 'kaminari-nobrainer', '~> 0.1.0'
gem 'nobrainer', '~> 0.41.1'
gem 'redis', '< 5'
gem 'sidekiq', '~> 6'
gem 'sidekiq-cron', '~> 1.10.0'
gem 'sidekiq-failures', '~> 1.0'
gem 'slack-notifier', '~> 2.3.2'
gem 'sshkey', '~> 2.0.0'

group :development do
  gem 'listen', '~> 3.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '~> 3.34.0'
  gem 'capybara-screenshot', '~> 1.0.25'
  gem 'colorize', '~> 0.8.1'
  gem 'cucumber-rails', '~> 2.5.0', require: false
  gem 'database_cleaner-nobrainer', '~> 1.0.0'
  gem 'email_spec', '~> 2.2.0'
  gem 'ffaker', '~> 2.17.0'
  gem 'launchy', '~> 2.5.0'
  gem 'nobrainer-rspec', '~> 1.1.2'
  gem 'rspec-expectations', '~> 3.10.0'
  gem 'rspec-rails', '~> 4.0.1'
  gem 'selenium-webdriver', '~> 3.142.7'
  gem 'site_prism', '~> 3.7'
  gem 'vcr', '~> 6.1.0'
  gem 'webmock', '~> 3.14'
end

group :development, :test do
  gem 'eslint-webpacker', '~> 1.1.0'
  gem 'rubocop', '~> 1.28.2', require: false
  gem 'rubocop-rails', '~> 2.14.2', require: false
  gem 'rubocop-rspec', '~> 2.10.0', require: false
end
