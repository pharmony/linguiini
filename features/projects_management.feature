@vcr-gitlab-repositories
Feature: Projects management
  In order to translate my applications
  As an admin
  I want to create projects

  Scenario: Access the project list without configure integration
    When I go to the projects page
    Then I should see the no configured integration message
    When I configure an integration
    When I go back to the projects page
    Then I should be able to create new projects

  Scenario: Create a project targeting a repository which doesn't exist anymore
    Given a configured integration
    And the project's repository have supported translation file format
    When I create a project
    Then I should see my created project
    And the string import task has run
    Given the project's repository has been deleted
    When I request to synchronize the project
    Then I should see my project in the import failure state

  @wip
  Scenario: Create a project targeting a repository where translation files can't be found
    Given a configured integration
    When I create a project
    Then I should see my created project
    And the string import task has run
    Given the project's repository doesn't have translation files anymore
    When I request to synchronize the project
    Then I should see my project in the import failure state

  @wip
  Scenario: Create a project targeting a repository with unsupported translation file format
    Given a configured integration
    When I create a project
    Then I should see my created project
    And the string import task has run
    Given the project's repository have unsupported translation file format
    When I request to synchronize the project
    Then I should see my project in the import failure state

  Scenario: Create a project targeting a repository with supported translation file format
    Given a configured integration
    And a "English (France)" locale with the code "en_FR" exist
    And the project's repository have supported translation file format
    When I create a project
    Then I should see my created project
    And the string import task has run
    And I should see my project in the import success state
    And I should see my project's progress chart

  Scenario: Edit a project
    Given the project's repository have supported translation file format
    And an existing project
    When I rename a project
    Then I should see the project renamed

  Scenario: Delete a project
    Given the project's repository have supported translation file format
    And an existing project
    When I delete a project
    Then I should not see any projects
