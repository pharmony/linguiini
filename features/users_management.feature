Feature: Users management
  In order to invite people
  As an admin
  I want a user list
  And I want to be able to invite new users
  And I want to be able to remove users

  Scenario: Listing users while having no other users
    Given no other users than me
    When I go to the user list
    Then I should only see my user
    And I should not be able to delete my own user

  Scenario: Listing users with existing other users
    Given another user
    When I go to the user list
    Then I should see 2 users

  Scenario: Invite a new admin
    Given a clear email queue
    When I invite someone as an admin
    And he accepts the invitation
    Then the new admin should be able to login

  Scenario: Invite a new translator
    Given a clear email queue
    When I invite someone as a translator
    And he accepts the invitation
    Then the new translator should be able to login

  Scenario: Remove a user
    Given another user
    When I remove the other user
    Then I should not see any other users than me
    And the other user should not be able to login

  Scenario: Change user's role
    Given another user
    When I change the other user's role
    Then I should see the new other user's role
