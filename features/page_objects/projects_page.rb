# frozen_string_literal: true

class ProjectsPage < SitePrism::Page
  set_url '/projects'

  # Dashboard
  element :no_configured_integration_message, '#projects #no-integration'
  element :create_project_button, '#projects .card[name="create"]'

  # Project list
  elements :projects, '#projects .card[name="project"]:not([name="create"])'

  # Project creation modal
  #
  element :project_modal, '.modal'
  # Step 1
  # Project name field
  element :name_field, '.modal form#step-1 input#name'
  element :locales_select, '.modal form#step-1 #locales > div'
  elements :locales_options,
           '.modal form#step-1 #locales div[class$="-menu"] div'

  #
  # Step 2
  element :repositories_select, '.modal form#step-2 #repositories > div'
  elements :repositories_options,
           '.modal form#step-2 #repositories div[class$="-menu"] div'
  element :locales_path, '.modal form#step-2 input#paths'

  # Import status
  element :import_status_is_succeeded,
          '#projects .card:not([name="create"]) :last-child.card-subtitle ' \
          '.badge-success'
  element :import_status_is_failure_or_success,
          '#projects .card:not([name="create"]) :last-child.card-subtitle ' \
          '.badge-danger,' \
          '#projects .card:not([name="create"]) :last-child.card-subtitle ' \
          '.badge-success'

  # "Next step" button
  element :previous_step_button, '.modal .modal-footer .btn-primary:first-child'
  element :next_step_button, '.modal .modal-footer .btn-primary:last-child'

  # Edit modal
  element :edit_modal_name_field, '.modal form#edit input#name'
  element :edit_modal_save_button, '.modal .modal-footer .btn-primary'

  # Delete modal
  element :confirm_modal, '.modal'
  element :confirm_delete_button, '.modal .modal-footer .btn-danger'

  element :destroying_project, '#projects .card.opacity-50'

  # Synchronize modal
  element :synchronize_modal_confirm_button,
          '.modal .modal-footer button.btn-primary'

  def create_a_new_project
    create_project_button.click

    fill_creation_modal_step1
    fill_creation_modal_step2

    # Modal step 3 (Fetching locale files)
    #  -> On failure: Back to the previous step, show errors on the fields
    #  -> On succes: Close modal
  end

  def delete_a_project
    project = projects.first

    click_project_menu('remove', project)

    confirm_delete_button.click
  end

  # rubocop:disable Naming/PredicateName
  def has_project_with_name?(name)
    projects.detect { |project| project.find('.card-title').text == name }
  end
  # rubocop:enable Naming/PredicateName

  def rename_a_project
    project = projects.first
    click_project_menu('edit', project)

    new_name = FFaker::Name.name

    # Edit modal
    edit_modal_name_field.set(new_name)
    edit_modal_save_button.click

    new_name
  end

  def synchronize_project
    project = projects.first

    click_project_menu('force_import', project)

    synchronize_modal_confirm_button.click
  end

  private

  def click_project_menu(entry_name, project)
    project.find('[name="menu"]').click
    project.find(".dropdown-menu [name='#{entry_name}']").click
  end

  def fill_creation_modal_step1
    name_field.set(FFaker::Product.product_name)
    locales_select.click
    locales_options.first.click
    next_step_button.click
  end

  def fill_creation_modal_step2
    repositories_select.click
    repositories_options.detect do |repository|
      repository.text == 'Linguiini / linguiini'
    end.click

    locales_path.set('config/locales/,app/javascript/src/locales/')
    next_step_button.click
  end
end
