# frozen_string_literal: true

class SettingsPage < SitePrism::Page
  set_url '/settings'

  element :no_configured_integration_message, '#projects #no-integration'

  gitlab = 'section[name="integrations"] section[name="gitlab"]'
  element :integration_private_token, "#{gitlab} input#gitlab-private-token"
  element :integration_submit_button, "#{gitlab} button.btn-primary"

  element :configured_integration,
          "#{gitlab} .card .btn-badge.btn-outline-success"

  def configure_integration
    integration_private_token.set(GitlabHelpers.private_token)
    integration_submit_button.click

    wait_until_configured_integration_visible
  end
end
