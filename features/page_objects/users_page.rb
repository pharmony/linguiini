# frozen_string_literal: true

class UsersPage < SitePrism::Page
  set_url '/users'

  element :modal, '.modal'

  # Access elements
  element :invite_button, 'button#invite-new-user'

  # Form elements
  element :email_field, '.modal form input[name="email"]'
  element :role_list, '.modal form select[name="roles"]'
  element :send_invitation_button, '.modal form button.btn-primary'

  # users table
  elements :users, 'table tbody tr'
  element :my_delete_button, 'table tbody tr.me button[name="remove"]'

  def change_someone_else_role
    change_user_role_for_element(the_other_guy)
  end

  def change_user_role_for_element(element)
    element.find('button[name="edit"]').click

    edit_modal = UserEditModal.new
    edit_modal.change_user_role
  end

  def invite_new(role)
    invitation_email = FFaker::Internet.email

    invite_button.click

    email_field.set(invitation_email)
    role_list.select(role)
    send_invitation_button.click

    wait_until_modal_invisible

    invitation_email
  end

  def remove_someone_else
    remove_user_element(the_other_guy)
  end

  def remove_user_element(element)
    user_email = element.find('td:first-child').text

    element.find('button[name="remove"]').click

    confirm_modal = UserRemoveModal.new
    confirm_modal.confirm_button.click

    user_email
  end

  def the_other_guy
    @the_other_guy ||= begin
      user = users.detect do |item|
        item.text.include?(UserHelpers.original_admin_email) == false
      end

      unless user
        raise 'Unable to find another user than ' \
              "'#{UserHelpers.original_admin_email}'"
      end

      user
    end
  end

  def the_other_guy_role
    the_other_guy.find('td:nth-child(2)')
  end
end
