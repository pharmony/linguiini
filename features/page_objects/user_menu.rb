# frozen_string_literal: true

class UserMenu < SitePrism::Page
  element :menu_button, '.navbar #user-menu'
  element :logout_entry, '.navbar .dropdown button[name="logout_entry"]'

  def logout
    menu_button.click
    logout_entry.click
  end
end
