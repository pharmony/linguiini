# frozen_string_literal: true

class UserRemoveModal < SitePrism::Page
  element :confirm_button, '.modal button.btn-danger'
end
