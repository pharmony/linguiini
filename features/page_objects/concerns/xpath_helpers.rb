# frozen_string_literal: true

module XpathHelpers
  extend ActiveSupport::Concern

  class_methods do
    def with_class(name)
      "contains(@class, '#{name}')"
    end

    def with_text(text)
      "contains(text(), '#{text}')"
    end
  end
end
