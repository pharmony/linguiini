# frozen_string_literal: true

class UserEditModal < SitePrism::Page
  element :selected_role, '.modal select[name="roles"] option:checked'

  element :save_button, '.modal button.btn-primary'

  elements :roles, '.modal select[name="roles"] option'
  element :role_dropdown, '.modal select[name="roles"]'

  def change_user_role
    selected_role_name = selected_role.text

    other_roles = roles.collect(&:value).reject do |role|
      role == selected_role_name
    end

    new_role = other_roles.sample
    role_dropdown.select(new_role)

    save_button.click

    new_role
  end
end
