# frozen_string_literal: true

class InvitationAcceptationPage < SitePrism::Page
  element :password_field, 'form#edit-user input#user-password'
  element :confirmation_password_field,
          'form#edit-user input#user-password-confirmation'
  element :submit_button, 'form#edit-user button[name="commit"]'

  def create_password
    password_field.set(UserHelpers.password)
    confirmation_password_field.set(UserHelpers.password)
    submit_button.click
  end
end
