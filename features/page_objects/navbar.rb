# frozen_string_literal: true

class Navbar < SitePrism::Page
  element :settings_link, '.navbar a[href="/settings"]'
  element :users_link, '.navbar a[href="/users"]'
end
