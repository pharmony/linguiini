# frozen_string_literal: true

class LoginPage < SitePrism::Page
  set_url '/users/sign_in'

  element :login_form, 'form#login-form'
  element :email_field, 'form #login-email'
  element :password_field, 'form #login-password'
  element :login_button, 'form button.btn-primary'

  element :login_error, '.alert-danger'

  def login_as_the_original_admin
    login_with(email: UserHelpers.original_admin_email,
               password: UserHelpers.password)
  end

  def login_with(params)
    email_field.set(params[:email])
    password_field.set(params[:password])

    login_button.click
  end
end
