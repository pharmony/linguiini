# frozen_string_literal: true

Then(/^the string import task has run$/) do
  expect(Sidekiq::Queues['imports'].size).to be_zero,
                                             'The "imports" queue should be ' \
                                             'empty'
end
