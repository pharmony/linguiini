# frozen_string_literal: true

Given(/^a configured integration$/) do
  # Creates the Integration
  integration_configuration = IntegrationHelpers.configure_gitlab
  expect(integration_configuration).to be_success

  # Runs the integration Sidekiq job
  Sidekiq::Worker.drain_all

  expect(
    Integration.find(integration_configuration.integration.id).state
  ).to eql('fetching_succeeded')
end

When(/^I configure an integration$/) do
  on(Navbar).settings_link.click

  on(SettingsPage) do |page|
    Sidekiq::Testing.inline! do
      page.configure_integration
    end

    expect(page).to have_configured_integration
  end
end
