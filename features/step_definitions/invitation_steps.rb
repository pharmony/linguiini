# frozen_string_literal: true

When(/^I invite someone as an? (admin|translator)$/) do |role|
  step 'I login as the original admin'

  invitation_email = nil

  on(UsersPage, visit: true) do |page|
    Sidekiq::Testing.inline! do
      invitation_email = page.invite_new(role)
    end
  end

  user_found = false
  timeout = 10

  until user_found
    timeout -= 1

    if timeout <= 0
      raise "Unable to find the user with email #{invitation_email}!"
    end

    user_found = on(UsersPage, visit: true).users.detect do |user|
      user.text.include?(invitation_email)
    end
  end
end
