# frozen_string_literal: true

When(/^I change my firstname and lastname$/) do
  step 'I login with my new account'

  user_profile_page = on(UserProfilePage)
  user_profile_page.fillin(%i[firstname lastname])
  user_profile_page.save_button.click
end

When(/^I change my password$/) do
  # Be sure to have a word of at least 6 caracters
  @new_password_after_password_change = begin
    word = ''
    word = FFaker::Lorem.word until word.size > 5
    word
  end

  user_profile_page = on(UserProfilePage)
  user_profile_page.fillin(%i[password password_confirmation],
                           with: @new_password_after_password_change)
  user_profile_page.change_password_button.click
end

When(/^he accepts the invitation$/) do
  step 'I logout'

  the_other_guy = User.where(
    :email.not => UserHelpers.original_admin_email
  ).first
  @the_other_guy_email_address = the_other_guy.email

  expect(the_other_guy).to be_present

  step "\"#{the_other_guy.email}\" opens the email with subject " \
       '"Invitation instructions"'
  step 'he clicks the 2nd link in the email'

  expect(current_url).to match(%r{/users/[\w\d]+/invitation/accept})

  on(InvitationAcceptationPage).create_password
end

Then(/^I should see my updated profile$/) do
  last_user = User.last
  expect(last_user.firstname).to be_present
  expect(last_user.lastname).to be_present

  user_description = "#{last_user.firstname} #{last_user.lastname}"

  user_menu = UserMenu.new
  expect(user_menu.user_description.text).to eql(user_description)
end

Then(/^the new (?:admin|translator) should be able to login$/) do
  expect(@the_other_guy_email_address).to be_present

  on(LoginPage).login_with(
    email: @the_other_guy_email_address,
    password: UserHelpers.password
  )
end

Then(/^the other user should not be able to login$/) do
  expect(@the_other_guy_email_address).to be_present

  expect(User.where(email: @the_other_guy_email_address).count).to be_zero
end
