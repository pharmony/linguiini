# frozen_string_literal: true

When(/^I login as the original admin$/) do
  on(LoginPage, visit: true) do |page|
    page.wait_until_login_form_visible
    page.login_as_the_original_admin
    page.wait_until_login_form_invisible
  end
end

When(/^I logout$/) do
  on(UserMenu).logout
end

Then(/^I be logged in as an admin user$/) do
  expect(Navbar.new).to have_users_link
end
