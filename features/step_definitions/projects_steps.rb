# frozen_string_literal: true

Given(/^an existing project$/) do
  step 'a configured integration'
  step 'I create a project'
end

Given(/^the project's repository has been deleted$/) do
  VCR.insert_cassette('vcr-gitlab-repository-deleted', record: :new_episodes)

  File.write(
    Rails.root.join(File.join('features', 'fixtures', 'git-error')),
    <<-GIT_ERROR
      ERROR: Repository not found.
      fatal: Could not read from remote repository.

      Please make sure you have the correct access rights
      and the repository exists.
    GIT_ERROR
  )
end

Given(/^the project's repository have supported translation file format$/) do
  VcrHelpers.reinsert_cassette_if_needed(
    'cucumber_tags/vcr-gitlab-repositories-with-empty-deploy-keys',
    record: :new_episodes
  )
end

When(/^I go to the projects page$/) do
  step 'I login as the original admin'

  on(ProjectsPage, visit: true)
end

When(/^I create a project$/) do
  step 'I go to the projects page'

  on(ProjectsPage) do |page|
    Sidekiq::Testing.inline! do
      page.create_a_new_project

      page.wait_until_project_modal_invisible
    end
  end

  # We're not supposed to refresh the page since the project import status is
  # updated automatically with ActionCable ... but for an unknown reason it
  # doesn't refresh, so let's workaround that for now.
  on(ProjectsPage, visit: true, &:wait_until_import_status_is_succeeded_visible)
end

When(/^I should see the no configured integration message$/) do
  expect(ProjectsPage.new).to have_no_configured_integration_message
end

When(/^I go back to the projects page$/) do
  on(ProjectsPage, visit: true)
end

When(/^I rename a project$/) do
  page = ProjectsPage.new

  @first_project_name_after_rename = page.rename_a_project

  page.wait_until_project_modal_invisible
end

When(/^I delete a project$/) do
  on(ProjectsPage) do |page|
    Sidekiq::Testing.inline! do
      page.delete_a_project

      page.wait_until_confirm_modal_invisible

      page.wait_until_destroying_project_invisible
    end
  end
end

When(/^I request to synchronize the project$/) do
  on(ProjectsPage) do |page|
    Sidekiq::Testing.inline! do
      page.synchronize_project

      page.wait_until_confirm_modal_invisible

      page.wait_until_import_status_is_failure_or_success_visible
    end
  end
end

Then(/^I should be able to create new projects$/) do
  expect(ProjectsPage.new).to have_create_project_button
end

Then(/^I should see my created project$/) do
  expect(ProjectsPage.new).to have_projects
end

Then(/^I should see the project renamed$/) do
  expect(ProjectsPage.new)
    .to have_project_with_name(@first_project_name_after_rename)
end

Then(/^I should not see any projects$/) do
  expect(ProjectsPage.new.projects.size).to be_zero
end

Then(
  /^I should see my project in the import (success|failure) state$/
) do |state|
  on(ProjectsPage, visit: true) do |page|
    last_project = Project.last

    project = page.projects.detect do |element|
      element.find('.card-title').text == last_project.name
    end

    expect(project)
      .to be_present,
          "Expected page to have a project card named #{last_project.name}, " \
          "but it couldn't be found."

    project_status = project.find('.card-subtitle .badge').value

    final_state = case state
                  when 'success' then 'succeeded'
                  when 'failure' then 'failed'
                  end

    expect(project_status).to eql("importing_#{final_state}")
  end
end

Then(/^some strings to be translated should be available$/) do
  last_project = Project.last

  project = ProjectsPage.new.projects.detect do |element|
    element.find('.card-title').text == last_project.name
  end

  expect(project)
    .to be_present,
        "Expected page to have a project card named #{last_project.name}, " \
        "but it couldn't be found."

  project_strings = project.find(
    'section[name="translation-progress-widget"] article[name="total-strings"]'
  ).text.gsub(/ strings/, '').to_i

  expect(project_strings)
    .not_to be_zero,
            'Expected project total strings to be greater than zero, ' \
            'but was zero'
end

Then(/^I should see my project's progress chart$/) do
  on(ProjectsPage) do |page|
    expect(page.projects.first)
      .to have_css('article[name="progression-percentage"]')
  end
end
