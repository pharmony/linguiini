# frozen_string_literal: true

Given(/^a "([^"]*?)" locale with the code "([^"]*?)" exist$/) do |name, code|
  if Locale.where(english_name: name, code: code).first.blank?
    interactor = Api::Locales::Create.call(name: name, code: code)

    expect(interactor).to be_success
  end
end
