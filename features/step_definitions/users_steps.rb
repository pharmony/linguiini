# frozen_string_literal: true

Given(/^another user$/) do
  UserHelpers.create_new_admin
end

Given(/^no other users than me$/) do
  User.where(:email.not => UserHelpers.original_admin_email).destroy_all
end

When(/^I go to the user list$/) do
  step 'I login as the original admin'

  on(UsersPage, visit: true)
end

When(/^I remove the other user$/) do
  step 'I go to the user list'

  @the_other_guy_email_address = UsersPage.new.remove_someone_else
end

When(/^I change the other user's role$/) do
  step 'I go to the user list'

  @the_other_guy_new_role = UsersPage.new.change_someone_else_role
end

Then(/^I should (?:only )?see (?:my user|(\d+) users)$/) do |n_users|
  expect(UsersPage.new.users.size).to eql(n_users.nil? ? 1 : n_users.to_i)
end

Then(/^I should not see any other users than me$/) do
  expect(UsersPage.new.users.size).to eql(1)
end

Then(/^I should see the new other user's role$/) do
  expect(@the_other_guy_new_role).to be_present
  expect(UsersPage.new.the_other_guy_role.text).to eql(@the_other_guy_new_role)
end

Then(/^I should not be able to delete my own user$/) do
  expect(UsersPage.new.my_delete_button).to be_disabled
end
