# frozen_string_literal: true

module PageObject
  #
  # Module to facilitate to creating of page objects in step definitions.
  # You can make the methods below available to all of your step definitions
  # by adding this module to World.
  # This idea was first discussed in Alister Scott's blog
  # entry http://watirmelon.com/2011/06/07/removing-local-page-references-from-cucumber-steps/.
  #
  # @example Making the PageFactory available to your step definitions
  #   World PageObject::PageFactory
  #
  # @example using a page that has already been visited in a Scenario
  #   on_page MyPageObject do |page|
  #     page.name.should == 'Cheezy'
  #   end
  #
  module PageFactory
    #
    # Create a page object.
    #
    # @param [PageObject, String] a class that has included the PageObject
    #                             module or string containing the name of the
    #                             class
    # @param [Boolean] a boolean indicating if the page should be visited?
    #                  default is false.
    # @param [block] an optional block to be called
    # @return [PageObject] the newly created page object
    #
    def on_page(page_class, visit: false, &block)
      page_class = class_from_string(page_class) if page_class.is_a?(String)
      @current_page = page_class.new

      # Load the page object URL if visit is true
      @current_page.load if visit

      # Runs the `before_actions` method if any
      @current_page.before_actions if @current_page.respond_to?(:before_actions)

      block&.call @current_page
      @current_page
    end

    # Support 'on' for readability of usage
    alias on on_page

    private

    def class_from_string(str)
      str.split('::').inject(Object) do |mod, class_name|
        mod.const_get(class_name)
      end
    end
  end
end

World(PageObject::PageFactory)
