# frozen_string_literal: true

module UserHelpers
  def self.build_new_visitor
    Struct.new(:email, :password).new(FFaker::Internet.email, password)
  end

  def self.create_new_admin
    User.create!(
      email: FFaker::Internet.email,
      password: password,
      password_confirmation: password,
      role: 'admin'
    )
  end

  def self.original_admin_email
    'admin@linguiini'
  end

  def self.password
    'linguiini'
  end
end
