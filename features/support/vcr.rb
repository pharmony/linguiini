# frozen_string_literal: true

require 'vcr'

VCR.configure do |c|
  c.cassette_library_dir = 'features/cassettes'
  c.ignore_hosts Socket.gethostname, 'selenium', 'linguiini.linguiini-dev'
  c.ignore_localhost = true
  c.hook_into :webmock
  # c.debug_logger = Kernel
end

VCR.cucumber_tags do |t|
  t.tag '@vcr-gitlab-repositories'
end

module VcrHelpers
  def self.current_cassette_path
    VCR.current_cassette
       .file.split('features/cassettes/')
       .last
       .gsub(/\.yml$/, '')
  end

  def self.reinsert_cassette_if_needed(path, options = {})
    VCR.eject_cassette if current_cassette_path == path

    VCR.insert_cassette(path, options)
  end
end

#
# Eject all the cassettes after a scenario
After do
  VCR.cassettes.each { |cassette| VCR.eject_cassette(cassette) }
end
