# frozen_string_literal: true

# Capybara defaults to CSS3 selectors rather than XPath.
# If you'd prefer to use XPath, just uncomment this line and adjust any
# selectors in your step definitions to use the XPath syntax.
# Capybara.default_selector = :xpath

# rubocop:disable Metrics/BlockLength
Capybara.register_driver :selenium_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    build: "p#{ENV.fetch('CI_PIPELINE_ID', nil) || 'missing'}",
    chromeOptions: {
      # https://peter.sh/experiments/chromium-command-line-switches/
      args: [
        '--auto-open-devtools-for-tabs',
        '--disable-default-apps',
        '--disable-extensions',
        '--disable-infobars',
        '--disable-notifications',
        '--disable-password-generation',
        '--disable-password-manager-reauthentication',
        '--disable-password-separated-signin-flow',
        '--disable-popup-blocking',
        '--disable-save-password-bubble',
        '--disable-translate',
        '--incognito',
        '--mute-audio',
        '--no-default-browser-check',
        '--window-size=1400,1024'
      ]
    },
    idleTimeout: ENV['SELENIUM_TIMEOUT'] ? ENV['SELENIUM_TIMEOUT'].to_i : 90,
    prefs: {
      download: { prompt_for_download: false },
      credentials_enable_service: false,
      profile: {
        password_manager_enabled: false,
        default_content_settings: { popups: 0 },
        content_settings: {
          pattern_pairs: {
            '*' => { 'multiple-automatic-downloads' => 1 }
          }
        }
      }
    },
    screenResolution: ENV.fetch('SELENIUM_RESOLUTION', nil) || '1440x900'
  )

  client = Selenium::WebDriver::Remote::Http::Default.new
  client.read_timeout = ENV.fetch('SELENIUM_TIMEOUT', nil)&.to_i || 120

  selenium_url = ENV.fetch('SELENIUM_URL', nil)
  unless selenium_url
    selenium_host = ENV.fetch('SELENIUM_HOST', nil) || 'localhost'
  end
  selenium_port = ENV.fetch('SELENIUM_PORT', nil) || 4444 unless selenium_url
  selenium_url ||= "http://#{selenium_host}:#{selenium_port}/wd/hub"

  Capybara::Selenium::Driver.new(
    app,
    browser: :remote,
    desired_capabilities: capabilities,
    http_client: client,
    url: selenium_url
  )
end
# rubocop:enable Metrics/BlockLength

# Defines headless chrome as the Capybara JavaScript driver
Capybara.javascript_driver = :selenium_chrome

# Forces all tests to run in Javascript mode
Capybara.default_driver = Capybara.javascript_driver

Capybara.default_max_wait_time = 15

# `server_host` is used by site_prism page objects in order to determine the
# host where to access the app.
Capybara.server_host = ENV.fetch('CAPYBARA_SERVER_HOST', Socket.gethostname)
Capybara.server_port = 3001
Capybara.always_include_port = true

# Uses the Capybara server_host to configure action_mailer.default_url_options
# and to configure CSRF cookie domain.
ENV['RAILS_ENV_BASE_DOMAIN'] = Capybara.server_host

Capybara.server = :puma, {
  Host: '0.0.0.0' # Binds Puma to container's 0.0.0.0
}

Capybara.disable_animation = true
