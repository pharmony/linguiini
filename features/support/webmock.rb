# frozen_string_literal: true

require 'webmock/cucumber'

WebMock.disable_net_connect!(allow: %w[app selenium])
