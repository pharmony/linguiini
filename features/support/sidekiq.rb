# frozen_string_literal: true

require 'sidekiq/testing'
Sidekiq::Testing.fake! # stores jobs in a 'jobs' array, do not push to Redis

Before { Sidekiq::Worker.clear_all }

After do
  Sidekiq::Queues.jobs_by_queue.each do |queue, jobs|
    next if jobs.size.zero?

    Kernel.puts "\nWARNING: Sidekiq queue has #{jobs.size} remaining job(s) " \
                "in the '#{queue}' queue".yellow
  end
end
