# frozen_string_literal: true

require 'capybara-screenshot/cucumber'

# Keep only the screenshots generated from the last failing test suite
Capybara::Screenshot.prune_strategy = :keep_last_run
