# frozen_string_literal: true

module IntegrationHelpers
  def self.configure_gitlab
    Api::OrganizeIntegrationCreation.call(
      params: {
        'platform' => 'gitlab',
        'token' => GitlabHelpers.private_token
      }
    )
  end
end
