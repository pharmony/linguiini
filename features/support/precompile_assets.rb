# frozen_string_literal: true

BeforeAll do
  $stdout.puts "🐢 Precompiling assets.\n"
  original_stdout = $stdout.clone
  # Use test-prof now 'cause it couldn't be monkey-patched
  # (e.g., by Timecop or similar)
  start = Time.current
  begin
    # Silence Webpacker output
    $stdout.reopen(File.new('/dev/null', 'w'))
    # next 3 lines to compile webpacker before running our test suite
    require 'rake'
    Rails.application.load_tasks
    Rake::Task['webpacker:compile'].execute
  ensure
    $stdout.reopen(original_stdout)
    $stdout.puts "Finished in #{(Time.current - start).round(2)} seconds"
  end
end
