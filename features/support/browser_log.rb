# frozen_string_literal: true

# Raises exception when something went wrong in the JavaScript

class DriverJSError < StandardError; end

AfterStep do |_|
  errors = page.driver.browser.manage.logs.get(:browser)
               .select { |e| e.level == 'SEVERE' && e.message.present? }
               .map(&:message)
               .to_a

  # Ignore connector errors
  errors = errors.grep_v(
    /You are currently using minified code outside of NODE_ENV/
  )

  raise DriverJSError, errors.join("\n\n") if errors.present?
end
