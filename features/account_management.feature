Feature: Account management
  In order to use the application
  As an admin or translation user
  I want to be invited
  And be able to login
  And change my password

  Scenario: Login as the original admin
    Given a clear email queue
    When I login as the original admin
    Then I be logged in as an admin user
