# linguiini 🇮🇹️

TODO : Write a descent description

## TODO

- [ ] Allow setting first and last names
- [ ] Restrict access to data based on user's role
- [ ] Show the active link in the navbar
- [ ] Locales label seems disconnected from the Select field

## Development

### Prerequisite

The dev environments are all based on Docker so you need to install it.

### Docker compose

This section describe how to prepare the development environment using Docker
compose.

1. Clone the repository on your machine and `cd` into it
2. Only for Mac users:
  - Create a `.env` file at the root of the repository
  - Copy the `SSH_AGENT_AUTH_SOCK` variable from `.env` file from the One repository.
3. Use earthly: `earthly +dev`
4. Start the stack
  * Linux users: `docker-compose up`
  * Mac users:
    * install docker-sync: `gem install docker-sync`
    * Start the stack: `docker-sync-stack start`

When all is up and running, you should be good to go to http://localhost:3000 and you should get the UI login page.

Run the following command in order to create the first admin user:

```
docker-compose exec app bundle exec rails nobrainer:sync_schema
docker-compose exec app bundle exec rails nobrainer:seed
```

You should see the username and password to use in order to log in.

### Kubernetes / Tilt

This section describe how to prepare the development environment using Kubernetes
with the help of [Tilt](https://tilt.dev/).

1. Install a Kubernetes cluster. We are using k3s:
```
k3d cluster create --registry-create k3s-registry --port "80:80@loadbalancer" --port "443:443@loadbalancer" --volume ${PWD}:/application
```
2. Install Tilt
```
curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash
```
3. Install helmfile by following the instructions from [the installation page](https://helmfile.readthedocs.io/en/latest/#installation)
4. Install Helm by following the installation instructions from [their documentation](https://helm.sh/docs/intro/install/).
5. Create the `deploy/releases/values/dev/common-secrets.yaml` file (you can find those information from your `~/.docker/config.json` file after you successfully loged in to your docker/gitlab registries):
```yaml
docker:
    dockerConfigJson: '{"auths":{"https://index.docker.io/v1/":{"username":"<your username>","password":"<your password>","email":"<your email>","auth":"<your auth>"}}}'
gitlab:
    dockerConfigJson: '{"auths":{"registry.gitlab.com":{"auth":"<your auth>"}}}'
```
6. Run Tilt
```
tilt up
```

When all the services are ready, you can access the app at http://linguiini.localhost.

When you need to run a command in the application Pod, like for adding/upgrading
a gem or an NPM package, run the tests and so on, use the following command:

```
kubectl exec deployment/app --container app --namespace linguiini-dev -- yarn
```

## Running the tests

After you have a up and running environment, you can then run all the tests
(Rubocop, Rspec, Cucumber) with the following command:

```
kubectl exec deployment/app --container app --namespace linguiini-dev -- bundle exec rake
```

With docker-compose the same command is the following:

```
docker-compose exec app bundle exec rake
```

### Updating the dummy-repository fixture archive

This project mock calls to the Gitlab API (other platform later?) using
WebMock/VCR, and it uses a mock script in order to mock the `git` command
(See `bin/git`).

That script uses a commited archive file containing all the files from the
[test repository](https://gitlab.com/linguiini/linguiini/) for this project.

In the case you need to add files to that test repo, you then have to update the
archive so that it reflects the changes.\
Here is how you can do it:

```
cd features/fixtures/
rm -f dummy-repository.tar.gz
g clone git@gitlab.com:linguiini/linguiini.git dummy-repository
tar -zcvf dummy-repository.tar.gz dummy-repository
rm -rf dummy-repository/
cd -
```

Now running Cucumber will use the updated archive. You can commit it.

### Managing git error from the `bin/git` script

The mock script allows you to give an expected error message that git would
raise.

We have a Cucumber scenario which tests Linguiini when trying to import the
translation files from a deleted git repository
(See the scenario "Create a project targeting a repository which doesn't exist
anymore" from the
[features/projects_management.feature](features/projects_management.feature))
file.

It calls the "the project's repository has been deleted" step which create
the file `features/fixtures/git-error` with the error to be raised.\
When the `bin/git` script runs, it looks for that file, and if it exits,
it `cat` the file and exit with the code `1`.

## Deployment

This app has a `deploy/` folder which configures deploying it in a Kubernetes
cluster using Helm/Helmfile.

When pushing code to the project's default branch, a new pipeline is started and
will eventually deploy the application creating an Ingress following your 
configuration.

Once the app runs in Kubernetes, don't forget to run the database initialization
commands :

```
docker-compose exec app bundle exec rails nobrainer:sync_schema
docker-compose exec app bundle exec rails nobrainer:seed
```

You should now being able to access the application using the Administration
account `admin@linguiini` and using the passowrd shown from the above commands.

Once logged in, there's one remaining step before to start importing your 
projects which is to configure an integration from the Settings page.

### Configure a Gitlab integration

In order to configure a Gitlab integration you need to create a Gitlab Personal 
Access Token to be used by Linguiini in order to access your repositories, push
SSH keys (in order to clone/read/write on your repository) and create MRs.

1. First you need to decide which Gitlab user you'd like to use in order to
   create MRs and commit/push in it. It could be a new user, concidered as a bot
   or an existing real user.
2. Login as the user which will be used by Linguiini and go to its [Personal 
   Access Tokens page](https://gitlab.com/-/profile/personal_access_tokens)
3. Enter a "Token name" like "Linguiini" for example
4. Select the `api` scope for that user allowing him to access the entire Gitlab
   API resources.
5. Click the "Create personal access token" button and copy the token

Linguiini can see the repository as the above Gitlab user sees them. If you want
to restrict the accessible projects that Linguiini can access to, invite the 
Gitlab user only in those projects **as a Maintainer** in order to allow 
Linguiini to push SSH keys and create MRs.

Now you can go to the Linguiini' Settings page from your deployed environment, 
past that token in the Gitlab integration input field and click the "Activate"
button.

When the Gitlab integration status switched to "success" you're good to go into 
the Projects page and import your projects.

### changelog-notifier

This project use the [changelog-notifier gem](https://gitlab.com/zedtux/changelog-notifier) which posts the release notes from the `CHANGELOG.md` file so keep it up-to-date folllowing the conventions from https://keepachangelog.com/.

## Releasing new version

1. Update the `CHANGELOG.md` file in order to take all the points from the "Unreleased" section into a new version
2. Don't forget to add the version link to the bottom of the file and updating the "Unreleased" link too
3. Update the `deploy/helm/Chart.yaml` file with the new version number
4. Commit the files with a tag for the version (See existing tags)
5. Push
6. Wait on the build to run and deploy the application to the production environement in Kubernetes
