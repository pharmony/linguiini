# Deploy

The aim of this file is to explain how to use `helmfile`, an orchestrator for
the `helm` command, in order to check and update an environment running Linguiini.

## Secrets encryption

This project use a private GPG key in order to encrypt secrets. The key has been
created and uploaded to the Kubernetes cluster manually.

You MUST have the key on your machine, which can be achieved using `kubectl`, so
you must have a working connection to the Kubernetes cluster.

When `kubectl` works on your machine, import the GPG key with the following 
command:
```bash
gpg --import <(kubectl get secret infra-pharmony-lu --namespace default --output jsonpath="{.data.key}" | base64 -d)
```

Then you can check the imported key:
```bash
gpg --list-secret-keys --keyid-format LONG
[keyboxd]
---------
sec   rsa3072/39D1C80E288155BA 2021-04-28 [SC]
      75F85D5C9B18AFCA08ADC6D539D1C80E288155BA
uid                 [ultimate] Pharmony SA <infra@pharmony.lu>
ssb   rsa3072/1D671FF4D6A5A6B8 2021-04-28 [E]
```

### Updating a secret

You can view/edit secrets from YAML files from the `deploy/releases/` folder
using a similar Helm warper script:
```bash
./bin/helm secrets edit deploy/releases/values/common-secrets.yaml
```

## Helmfile

In order to reduce the startup time here (avoind to waste time installing all
the tools and plugins), a script file can be used instead, which uses `docker`
to create a Docker image which has all the required tools and the GPG key 
available.

When you will run it the first time, as you will miss the docker image, it will
create it and then run you command:

```bash
./bin/helmfile --file deploy/helmfile.yaml --environment review --namespace linguiini-staging diff
WARN: The Docker image including the Pharmony GPG key doesn't exist on your machine, building it now..
[+] Building 0.1s (6/6) FINISHED                                                                                             docker:orbstack
 => [internal] load build definition from helmfile.dockerfile                                                                           0.0s
 => => transferring dockerfile: 176B                                                                                                    0.0s
[...]
 => => naming to docker.io/cablespaghetti/helmfile-docker:3.8.1-p6y                                                                     0.0s
Adding repo bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories
[...]
Decrypting secret /wd/deploy/releases/values/common-secrets.yaml
[helm-secrets] Decrypting /wd/deploy/releases/values/common-secrets.yaml

Comparing release=redis-review, chart=bitnami/redis
Comparing release=rethinkdb-review, chart=../rethinkdb-helm-chart
Comparing release=linguiini-review, chart=../helm
```

You will need 2 helmfile commands:
* `diff` to get a diff between what you ask, and what is in place
* `apply` to apply the requested changes

### Switching environments

The Linguiini project has 2 available permanent environments:
* staging
* production

The project has 2 `.env` files for those environments. The `.env` file declare
the required environment variables for Helm.

You have to use `ln -s [source] [destination]` in order to change the 
environment, and change your `helmfile` command arguments.

#### Staging

**WARNING:** Check the `CI_COMMIT_SHORT_SHA` variable from the last staging 
pipeline in order to use the right one (which should be the last commit SHA 
from the `master` branch).

```
ln -s ./.env.staging .env
./bin/helmfile --file deploy/helmfile.yaml --environment review --namespace linguiini-staging [COMMAND]
```

#### Production

**WARNING:** Check the `CI_COMMIT_SHORT_SHA` and `DOCKER_IMAGE_TAG` variables 
from the last production deployment.

```
ln -s ./.env.production .env
./bin/helmfile --file deploy/helmfile.yaml --environment production --namespace linguiini-production [COMMAND]
```
