# Migrate the database between Kubernetes clusters

The aim of this document is to describe how to migrate the database data from
one Kubernetes cluster to another one using Docker preventing installing
RethinkDB tools on your machine.

The "source cluster" is the Kubernetes cluster where is running Linguiini FROM
where you'd like to migrate.\
the "destination cluster" is the Kubernetes cluster where you'd like to migrate to.

1. Create a port-forwarding of the `rethinkdb-production-rethinkdb-proxy` service from the source cluster (to achieve that, I have used [Lens](https://k8slens.dev) but it should be possible to do it also using the `kubectl` CLI tool.)
2. Start a new RethinkDB container:
```
docker run --rm -it --net=host rethinkdb:2.4.4-bookworm-slim bash
```
3. Install the rethinkdb dump/restore plugin:
```
apt update && apt install -y python3 python3.11-venv
python3 -m venv ./venv
source venv/bin/activate
pip install rethinkdb
```
4. Dump the database (the `admin` password can be found from the Secrets):
```
rethinkdb dump -c 127.0.0.1 -p
```
5. Stop/delete the port-forwarding created at step 1
6. Create a port-forwarding of the `rethinkdb-production-rethinkdb-proxy` service from the destination cluster
7. Scale down to 0 the `app` deployment so that no-one can access the app while restoring
8. Delete all the existing tables from the database (here again I used Lens in order to forward the admin service on my local machine, giving me access to the RethinkDB web console)
9. Restore the dump (the `admin` password can be found from the Secrets):
```
rethinkdb restore ./rethinkdb_dump_*.tar.gz -c 127.0.0.1 -p --force
```

You're done!
