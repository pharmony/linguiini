# Gitlab Access Tokens

The aim of this document is to describe how to create Access Token in Gitlab so
that Linguiini is deployable and can clone/commit/push on your projects.

All the bellow tokens have to be created with the `pharmony-bot` user 
on [the Personal access tokens gitlab.com](https://gitlab.com/-/user_settings/personal_access_tokens).

## Access Token for Docker images

Linguiini runs in Kubernetes which needs an authorization in order to pull the
docker images when creating Pods. This token only need the `read_registry` scope 
in order to pull the Linguiini Docker images.

Create a new token named "Linguiini Pull Docker Images", with the bellow description:
> This token allow Kubernetes Pods to pull Linguiini Docker images.

and select the `read_registry` scope.

To update the Kubernetes secret `gitlab-registry`, you need first to update the
`linguiini/deploy/releases/values/common-secrets.yaml` file:

```bash
./bin/helm secrets edit deploy/releases/values/common-secrets.yaml
```

and change the `gitlab.dockerConfigJson` key with a `base64` version of 
the `username:pat`:

```bash
echo -n 'pharmony-bot:glpat-abcd1234' | base64
```

To apply the changes you can just commit/push and it will be continously deployed
through the Gitlab CI pipelines, or you can do it manually following [the Deploy doc](docs/deploy.md).

## Access Token to clone, commit and push on your projects

Linguiini uses `git` to access and update the translation files of your projects,
but it also uses the Gitlab APIs in order to fetch the list of projects, therefore
the Access Token to be configured in the Linguiini's UI needs the following scopes:
* `read_api` in order to fetch projects list
* `read_repository` in order to `git clone` and `git fetch`/`git pull`
* `write_repository` in order to `git push`

Now goto the Linguiini application, in the **Settings** page and fill the token
field with the PAT and click "Save". You should see Linguiini validating the
token.
