# https://github.com/tilt-dev/tilt-extensions
load('ext://earthly', 'earthly_build')
load('ext://namespace', 'namespace_create', 'namespace_inject')

# Not using any live_update here since we're using volumes with a hostPath to
# sync both ways the files.
earthly_build(
    context='.',
    target='+dev',
    ref='k3s-registry.localhost:5000/pharmony/linguiini',
    image_arg='IMAGE_NAME',
    # Ignoring all other folders than described from the .dockerignore file
    # since we're using a `hostPath` to sync both ways the files.
    ignore=[
        'app/',
        'bin/',
        'config/',
        'db/',
        'features/',
        'lib/',
        'public/',
        'spec/',
        'storage/',
        'vendor/',
        'Gemfile',
        'Gemfile.lock',
        'package.json',
        'yarn-error.log',
        'yarn.lock'
    ])

# Dependencies map
k8s_resource('redis-master', labels=["databases"])
k8s_resource('rethinkdb-rethinkdb-cluster', labels=["databases"], pod_readiness="wait")
k8s_resource('rethinkdb-rethinkdb-proxy', resource_deps=['rethinkdb-rethinkdb-cluster'], labels=["databases"])

k8s_resource('app', resource_deps=['redis-master', 'rethinkdb-rethinkdb-proxy', 'webpack'], labels=["backend"])
k8s_resource('sidekiq', resource_deps=['app'], labels=["backend"])

k8s_resource('mailhog', labels=["development"])
k8s_resource('selenium', labels=["development"])
k8s_resource('webpack', labels=["development"])

k8s_resource('linguiini-sync-indexes', resource_deps=['rethinkdb-rethinkdb-proxy'], labels=["jobs"])
k8s_resource('linguiini-db-seeds', resource_deps=['rethinkdb-rethinkdb-proxy'], labels=["jobs"])
k8s_resource('linguiini-data-migrations', resource_deps=['rethinkdb-rethinkdb-proxy'], labels=["jobs"])

namespace_create('linguiini-dev')

# https://docs.tilt.dev/api.html#api.k8s_yaml
k8s_yaml(namespace_inject(local("helmfile --file deploy/helmfile-dev.yaml template"), 'linguiini-dev'))
