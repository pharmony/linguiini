# frozen_string_literal: true

class ProjectStatsChannel < ApplicationCable::Channel
  def subscribed
    project_id = Project.where(id: params[:project_id]).first&.id

    reject unless project_id

    stream_for project_id
  end

  private

  def project
    Project.where(id: params[:project_id]).first
  end
end
