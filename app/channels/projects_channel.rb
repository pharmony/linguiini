# frozen_string_literal: true

class ProjectsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'ProjectsChannel'
  end
end
