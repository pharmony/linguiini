# frozen_string_literal: true

class IntegrationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'IntegrationsChannel'
  end
end
