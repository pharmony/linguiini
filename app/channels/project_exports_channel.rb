# frozen_string_literal: true

class ProjectExportsChannel < ApplicationCable::Channel
  def subscribed
    project_export_id = project.project_exports.where(id: params[:id]).first&.id

    reject unless project_export_id

    stream_for project_export_id
  end

  private

  def project
    Project.where(id: params[:project_id]).first
  end
end
