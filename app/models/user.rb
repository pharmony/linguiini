# frozen_string_literal: true

class User
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  # ~~~~ Callbacks ~~~~
  before_validation :sets_created_at_and_updated_at

  # ~~~~ Fields ~~~~
  field :email, type: String, required: true, default: '', uniq: true
  field :encrypted_password, type: String, required: true, default: ''

  ## Recoverable
  field :reset_password_token, type: String, uniq: true
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count, type: Integer, default: 0, required: true
  field :current_sign_in_at, type: Time
  field :last_sign_in_at, type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip, type: String

  ## Confirmable
  field :confirmation_token, type: String, uniq: true
  field :confirmed_at, type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email, type: String # Only if using reconfirmable

  ## Lockable
  # Only if lock strategy is :failed_attempts
  field :failed_attempts, type: Integer, default: 0, required: true
  # Only if unlock strategy is :email or :both
  field :unlock_token, type: String, uniq: true
  field :locked_at, type: Time

  field :created_at, type: Time, required: true
  field :updated_at, type: Time, required: true

  ## Invitable
  field :invitation_token, type: String
  field :invitation_created_at, type: Time
  field :invitation_sent_at, type: Time
  field :invitation_accepted_at, type: Time
  field :invitation_limit, type: Integer

  field :locale, type: String, default: 'en'
  field :role, type: String, required: true, in: %w[admin translator]

  # Include default devise modules. Others available are:
  # :registerable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable, :lockable, :recoverable,
         :rememberable, :trackable, :validatable, :invitable

  # ~~~~ Class Methods ~~~~
  # Devise fix with NoBrainer
  def self.increment_counter(*)
    true
  end

  # ~~~~ Instance Methods ~~~~
  # to fix an issue with devise
  # https://github.com/plataformatec/devise/issues/4542
  def will_save_change_to_email?; end

  # ActiveJob Integration
  #
  # If you are using Rails 4.2 and ActiveJob to deliver ActionMailer messages in
  # the background through a queuing back-end, you can send Devise emails
  # through your existing queue by overriding the send_devise_notification
  # method in your model.
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  private

  def sets_created_at_and_updated_at
    now = Time.zone.now
    self.created_at ||= now
    self.updated_at = now
  end
end
