# frozen_string_literal: true

class GitlabIntegration < Integration
  mattr_reader :platform_name, default: 'gitlab'
end
