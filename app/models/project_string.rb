# frozen_string_literal: true

class ProjectString
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  # ~~~~ Callbacks ~~~~
  before_save :set_values_type_if_needed
  before_save :set_with_issue_if_needed

  # ~~~~ Fields ~~~~
  # When a ProjectString with a value has his key deleted in the source code,
  # or the value been updated in the source code, the ProjectString is marked as
  # `conflict` so that one can review it and decide to really remove the
  # ProjectString, or take/ignore the new value.
  field :conflict, type: NoBrainer::Boolean, default: false, required: true

  # When at least two ProjectString have the same key, no matter the path,
  # `duplicated` is `true`, otherwise `false`.
  field :duplicated, type: NoBrainer::Boolean, default: false, required: true

  # `key` is the "absolute" or complete key of the string, which could include
  # the locale for keys where the root key is the actual locale.
  field :key, type: String, required: true, uniq: { scope: %i[project_id path] }

  # String locale like `fr_FR`
  field :locale, type: String, required: true

  # `true` when the source code file has a key like `'a.b.c': ''`, and
  # `false` when the source code file has a key like `{ a: { b: { c: '' } } }`
  field :nested_keys, type: NoBrainer::Boolean, default: false

  # `new_value` contains an update from the source that one will have to confirm
  # in order to apply it to `value`
  field :new_value

  # `new_values_type` behaves exactely the same like `values_type` but for the
  # `new_value` field.
  #
  # See bellow `values_type`.
  field :new_values_type, type: String

  # `old_value` is used to show the diff before to create a ProjectExport
  field :old_value

  field :path, type: String, required: true

  field :project_id, type: String, required: true, index: true

  # `relative_key` is the key of the string without the root key when the root
  # key is the locale for key.
  field :relative_key, type: String, required: true

  # `true` as soon as one updates the ProjectString from the UI
  field :translated, type: NoBrainer::Boolean, default: false

  # `value` is the current value either from the source code, either the
  # translated string.
  field :value

  # `values_type` is filled when `value` is an Array and contains the Array
  # element type so that we can cast back the values types when reading from the
  # database.
  #
  # This is required since NoBrainer converts arrays to Array.of(String), which
  # brakes Arrays of Symbols for example.
  field :values_type, type: String

  # `updated` is used in order to know when to show the export banner allowing
  # one to export the translations back to the source code.
  field :updated, type: NoBrainer::Boolean, default: false

  # `unused` strings are ProjectString for which the key no more exist in the
  # reference language(s) ProjectStrings.
  # They are then available from the Unused page where one can decide to delete
  # them from Linguiini.
  field :unused, type: NoBrainer::Boolean, default: false

  # The `with_issue` parameter is `true` when a ProjectString has `conflict`,
  # or `duplicated` being `true`, or when `locale` is empty.
  # See the `set_with_issue_if_needed` method.
  #
  # `with_issue` is used in order to filter the ProjectStrings to be shown from
  # the "Import issues" page in order to make faster query.
  field :with_issue, type: NoBrainer::Boolean, default: false

  # ~~~~ Associations ~~~~
  belongs_to :project

  # ~~~~ index ~~~~
  index :key_unicity_compound, %i[project_id path]
  index :project_conflicts_stats_compound, %i[project_id conflict]
  index :project_duplicated_stats_compound, %i[project_id duplicated]
  index :project_locales_compound, %i[project_id locale]
  index :project_values_conflict_stats_compound, %i[project_id conflict value]
  index :project_updated_compound, %i[project_id updated]
  index :project_unused_compound, %i[project_id unused]
  index :project_with_issues_compound, %i[project_id with_issue]

  # ~~~~ Instance Methods ~~~~
  %i[new_value value].each do |field|
    define_method(field) do
      # rubocop:disable Style/ClassEqualityComparison
      return super() unless super().class.name == 'Array'
      # rubocop:enable Style/ClassEqualityComparison

      return super().map(&:to_sym) if send("#{field}s_type") == 'Symbol'

      super()
    end
  end

  private

  #
  # Ensures the value is an Array, and all of its values have the same type,
  # and finaly return the Array values type, othewise fails the context
  # when the Array as many different value types.
  #
  def build_values_type_for(field)
    # rubocop:disable Style/ClassEqualityComparison
    return nil unless send(field).class.name == 'Array'
    # rubocop:enable Style/ClassEqualityComparison

    array_types = send(field).entries.collect(&:class).uniq

    send("#{field}s_type=", array_types.first.to_s) if array_types.size == 1

    errors.add(field, :invalid,
               message: I18n.t('errors.not_uniq_array_values_type',
                               field: field))
  end

  def set_values_type_if_needed
    %i[value new_value].each { |field| build_values_type_for(field) }
  end

  def set_with_issue_if_needed
    self.with_issue = conflict || duplicated || locale.blank?
  end
end
