# frozen_string_literal: true

class Project
  include NoBrainer::Document
  include AASM

  # ~~~~ Callbacks ~~~~
  before_save :before_broadcast_transition_if_any
  after_save :broadcast_transition_if_any

  # ~~~~ Virtual Attributes ~~~~
  attr_accessor :previous_changes

  # ~~~~ Fields ~~~~
  # `files_md5s` contains project's file paths and its MD5 sum.
  # It has the `lazy_fetch: true` since it is used only from the import,
  # in order to treat only changed files.
  field :files_md5s, type: Hash, lazy_fetch: true
  field :import_failure_reason, type: Text
  field :import_stats, type: Hash, default: {
    start: nil,
    end: nil,
    duration: nil
  }
  field :import_step, type: String
  field :integration_id, type: String, required: true
  field :local_path, type: String # Path where the project has been cloned
  field :locales, type: Array # Translated locales
  field :name, type: String, required: true, uniq: true
  field :paths, type: String, required: true
  field :reference_languages, type: Array, required: true
  # Remote project branch selected on creating a new project
  field :repository_branch_name, type: String
  # Remote project ID selected on creating a new project. When blank, default
  # branch is used.
  field :repository_id, type: Integer, required: true
  field :state, type: String, required: true, default: 'pending'
  field :strings_stats, type: Hash, required: true, default: {
    conflicts: 0,
    duplicates: 0,
    missing_locale_strings: 0,
    translated: 0,
    untranslated: 0
  }
  field :to_be_deleted, type: Boolean, default: false

  # ~~~~ Associations ~~~~
  belongs_to :integration
  has_many :project_exports, dependent: :delete
  has_many :project_strings, dependent: :delete

  aasm column: 'state' do
    state :pending, initial: true # Waiting for the import
    state :importing
    state :importing_succeeded
    state :importing_failed

    event :import do
      transitions from: %i[pending importing importing_succeeded
                           importing_failed],
                  to: :importing,
                  after: proc { reset_failure_reason_and_import_stats }
    end
    event :import_success do
      transitions from: :importing, to: :importing_succeeded,
                  after: proc {
                    clear_failure_reason
                    update_import_stats
                  }
    end
    event :import_failure do
      transitions to: :importing_failed,
                  after: proc { |payload|
                    failure_reason(payload)
                    update_import_stats
                  }
    end
  end

  # ~~~~ Instance Methods ~~~~
  def before_broadcast_transition_if_any
    self.previous_changes = changes
  end

  def broadcast_transition_if_any
    return if previous_changes.empty?

    ActionCable.server.broadcast(
      'ProjectsChannel',
      ActiveModelSerializers::SerializableResource.new(self)
    )
  end

  def clear_failure_reason
    self.import_failure_reason = nil
  end

  def failure_reason(payload)
    self.import_failure_reason = if payload.key?(:reason)
                                   payload[:reason]
                                 else
                                   payload
                                 end
  end

  def initialize_import_stats
    self.import_stats = {
      start: Time.zone.now,
      end: nil,
      duration: nil
    }
  end

  def reset_failure_reason_and_import_stats
    clear_failure_reason
    initialize_import_stats
  end

  def update_import_stats
    import_stats[:end] = Time.zone.now
    import_stats[:duration] = import_stats[:end] - import_stats[:start]
  end
end
