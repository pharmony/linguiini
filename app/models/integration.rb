# frozen_string_literal: true

class Integration
  include NoBrainer::Document
  include AASM

  # ~~~~ Callbacks ~~~~
  before_save :before_broadcast_transition_if_any
  after_save :broadcast_transition_if_any

  # ~~~~ Virtual Attributes ~~~~
  attr_accessor :previous_changes

  # ~~~~ Fields ~~~~
  field :ssh_key_fingerprint_md5, type: Text
  field :ssh_key_fingerprint_sha256, type: Text
  field :ssh_private_key, type: Text
  field :ssh_public_key, type: Text
  field :state, type: String, required: true, default: 'pending'
  field :token, type: String, required: true

  # rubocop:disable Metrics/BlockLength
  aasm column: 'state' do
    state :pending, initial: true # Waiting for Sidekiq to take our task

    state :authenticating # Trying to authenticate against the platform
    state :authentication_succeeded
    state :authentication_failed

    state :fetching # Fetching repositories from the platform
    state :fetching_succeeded
    state :fetching_failed

    event :authenticate do
      transitions from: %i[pending authentication_succeeded
                           authentication_failed fetching_failed
                           fetching_succeeded],
                  to: :authenticating
    end
    event :authenticate_success do
      transitions from: :authenticating, to: :authentication_succeeded
    end
    event :authenticate_failure do
      transitions from: :authenticating, to: :authentication_failed
    end

    event :fetch do
      transitions from: :authentication_succeeded, to: :fetching
    end
    event :fetch_success do
      transitions from: :fetching, to: :fetching_succeeded
    end
    event :fetch_failure do
      transitions from: :fetching, to: :fetching_failed
    end
  end
  # rubocop:enable Metrics/BlockLength

  # ~~~~ Instance Methods ~~~~
  def before_broadcast_transition_if_any
    self.previous_changes = changes
  end

  def broadcast_transition_if_any
    return if previous_changes.empty?

    ActionCable.server.broadcast(
      'IntegrationsChannel',
      ActiveModelSerializers::SerializableResource.new(self)
    )
  end

  def ssh_key_generated?
    [ssh_private_key, ssh_public_key].all?(&:present?)
  end
end
