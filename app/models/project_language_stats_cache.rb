# frozen_string_literal: true

#
# This model is used in order to cache the stats of translated and to be
# translated strings from a project, preventing from quering the entire
# ProjectString table when we "just" when to show the progress bar per language
#
class ProjectLanguageStatsCache
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :locale_id, type: String, required: true, uniq: { scope: :project_id }
  field :project_id, type: String, required: true
  field :translateds, type: Integer, required: true
  field :translations, type: Integer, required: true

  # ~~~~ Associations ~~~~
  belongs_to :locale
  belongs_to :project
end
