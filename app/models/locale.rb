# frozen_string_literal: true

class Locale
  include NoBrainer::Document

  field :code, type: String, required: true, uniq: true, length: (2..10)
  field :english_name, type: String, required: true
end
