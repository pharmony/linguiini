# frozen_string_literal: true

class ProjectExport
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps
  include AASM

  # ~~~~ Fields ~~~~
  field :export_failure_reason, type: String
  field :export_failure_trace, type: Text
  field :prev_state, type: String, default: ''
  field :project_id, type: String, required: true, index: true
  field :requester_id, type: String, required: true
  field :state, type: String, required: true, default: 'pending'
  # Stores the export diff
  field :updates, type: Array, lazy_fetch: true
  field :updated_project_string_ids, type: Array

  # ~~~~ Associations ~~~~
  belongs_to :project
  belongs_to :requester, class_name: 'User'

  # rubocop:disable Metrics/BlockLength
  aasm column: 'state' do
    state :pending, initial: true # Waiting for the export
    state :started # Export has started
    state :branch_cleaning # Checking remote branch + git reset --hard
    state :generating # Generating the translation files
    state :pushing # Pushing translation files via git
    state :exporting_failed
    state :exporting_succeeded

    after_all_transitions :save_previous_state

    event :export do
      transitions from: :pending, to: :started
    end

    event :branch_clean do
      transitions from: :started, to: :branch_cleaning
    end

    event :generate_files do
      transitions from: :branch_cleaning, to: :generating
    end

    event :push_files do
      transitions from: :generating, to: :pushing
    end

    event :export_success do
      transitions from: :pushing, to: :exporting_succeeded
    end

    event :export_failure do
      transitions to: :exporting_failed,
                  after: proc { |payload| failure_reason(payload) }
    end
  end
  # rubocop:enable Metrics/BlockLength

  # ~~~~ Instance Methods ~~~~
  def failure_reason(payload)
    if payload.key?(:reason) && payload.key?(:trace)
      self.export_failure_reason = payload[:reason]
      self.export_failure_trace = payload[:trace]
    else
      self.export_failure_reason = payload
    end
  end

  private

  def save_previous_state
    self.prev_state = state
  end
end
