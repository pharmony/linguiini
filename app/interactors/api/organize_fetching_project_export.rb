# frozen_string_literal: true

module Api
  class OrganizeFetchingProjectExport
    include Interactor::Organizer

    organize Projects::Find,
             ProjectExports::Find
  end
end
