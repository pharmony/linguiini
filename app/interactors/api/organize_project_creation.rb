# frozen_string_literal: true

module Api
  class OrganizeProjectCreation
    include Interactor::Organizer

    organize Integrations::Find,
             Projects::Create,
             Projects::EnqueueForceImport
  end
end
