# frozen_string_literal: true

module Api
  class OrganizeFetchingProjectLanguageStats
    include Interactor::Organizer

    organize Projects::Find,
             Locales::Find,
             ProjectLanguageStatsCaches::Fetch,
             ProjectLanguageStats::Build,
             ProjectLanguageStatsCaches::CreateOrUpdate
  end
end
