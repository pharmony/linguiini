# frozen_string_literal: true

module Api
  #
  # Checks for the given paths in the given repository
  #
  class OrganizeCheckingPaths
    include Interactor::Organizer

    organize Integrations::Find,
             Integrations::CheckIfPathsAreExisting
  end
end
