# frozen_string_literal: true

module Api
  class OrganizeProjectForceImport
    include Interactor::Organizer

    organize Projects::Find,
             Projects::EnqueueForceImport
  end
end
