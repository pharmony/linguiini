# frozen_string_literal: true

module Api
  class OrganizeFetchingBranches
    include Interactor::Organizer

    organize Integrations::Find,
             Integrations::FetchAllProjectRepositoryBranches
  end
end
