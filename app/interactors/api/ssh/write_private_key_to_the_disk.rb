# frozen_string_literal: true

module Api
  module Ssh
    class WritePrivateKeyToTheDisk
      include Interactor
      include Interactor::Contracts

      expects do
        required(:integration).filled
      end

      promises do
        required(:integration_ssh_private_key_path).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ssh_home = ensure_ssh_home_folder_exist

        build_key_path(ssh_home)

        write_ssh_key

        # Changes private key permissions in order to avoid
        # the "UNPROTECTED PRIVATE KEY FILE!" error.
        FileUtils.chmod(0o600, context.integration_ssh_private_key_path)

        Rails.logger.debug do
          "[#{self.class}] Private SSH key written at " \
            "#{context.integration_ssh_private_key_path} with 600 permission."
        end
      end

      private

      def build_key_path(ssh_home)
        context.integration_ssh_private_key_path = File.join(
          ssh_home,
          context.integration.id
        )
      end

      def ensure_ssh_home_folder_exist
        ssh_home = File.join(ENV.fetch('HOME'), '.ssh')

        FileUtils.mkdir_p(ssh_home, mode: 0o700)

        ssh_home
      end

      def write_ssh_key
        File.write(context.integration_ssh_private_key_path,
                   context.integration.ssh_private_key)
      end
    end
  end
end
