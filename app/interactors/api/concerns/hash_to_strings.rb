# frozen_string_literal: true

module Api
  module Concerns
    #
    # Given a hash (read from JSON or YAML file), this method will convert it to
    # exploitable strings.
    #
    # Something like:
    #
    # {
    #   "root": {
    #     "first": {
    #       "second": "value"
    #     }
    #   }
    # }
    #
    # will be converted to:
    #
    # {
    #   "root.first.second": "value"
    # }
    #
    module HashToStrings
      extend ActiveSupport::Concern

      def reduce_to_strings(hash, parent_key = nil)
        memo = {}
        hash.each do |key, value|
          current_key = [parent_key, key].compact.join('.')

          if value.is_a?(Hash)
            memo.merge!(reduce_to_strings(value, current_key))
          else
            memo.merge!(current_key => value)
          end
        end

        memo
      end
    end
  end
end
