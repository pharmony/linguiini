# frozen_string_literal: true

module Api
  module Concerns
    module GitlabConfigurator
      extend ActiveSupport::Concern

      def configure_gitlab_with_token(token)
        ::Gitlab.configure do |config|
          config.endpoint = 'https://gitlab.com/api/v4'
          config.private_token = token
        end
      end
    end
  end
end
