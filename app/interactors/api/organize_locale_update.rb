# frozen_string_literal: true

module Api
  class OrganizeLocaleUpdate
    include Interactor::Organizer

    organize Locales::Find,
             Locales::Update
  end
end
