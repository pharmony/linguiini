# frozen_string_literal: true

module Api
  #
  # Retrieves the ProjectString objects without a defined locale
  #
  class OrganizeFetchingProjectStringsAlerts
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStringsAlerts::FetchAll
  end
end
