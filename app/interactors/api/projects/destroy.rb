# frozen_string_literal: true

module Api
  module Projects
    class Destroy
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      promises do
        required(:destroyed_project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        find_project

        destroy_project_local_path! if context.destroyed_project.local_path

        context.destroyed_project.project_strings.delete_all

        context.destroyed_project.destroy
      end

      private

      def destroy_project_local_path!
        Rails.logger.warn "Removing project's folder at " \
                          "#{context.destroyed_project.local_path}"

        FileUtils.rm_rf(context.destroyed_project.local_path)
      end

      def find_project
        context.destroyed_project = Project.where(id: context.id).first

        return if context.destroyed_project

        context.fail!(
          errors: I18n.t('errors.resource_not_found',
                         resource_name: I18n.t('resources.project'))
        )
      end
    end
  end
end
