# frozen_string_literal: true

module Api
  module Projects
    class Fetch
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      promises do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project = Project.where(id: context.id).first
      end
    end
  end
end
