# frozen_string_literal: true

module Api
  module Projects
    class MarkAsToBeDeleted
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      promises do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.update(to_be_deleted: true)
      end
    end
  end
end
