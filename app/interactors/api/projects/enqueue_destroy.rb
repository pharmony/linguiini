# frozen_string_literal: true

module Api
  module Projects
    class EnqueueDestroy
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        job = DestroyProjectJob.perform_later(context.project.id)

        return if job

        context.fail!(errors: job.errors)
      end
    end
  end
end
