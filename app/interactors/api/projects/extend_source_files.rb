# frozen_string_literal: true

module Api
  module Projects
    #
    # This interactor updated the `context.strings_per_source_files` in order
    # to add the file's locale and if it is part of the project's reference
    # languages.
    # This will allow to :
    # * Go through the files to detect their locale only once
    # * Process the project's reference languages files first and then the other
    #   files.
    #
    # As ProjectStrings are created for all the locales when processing a file
    # from a reference language, it would be faster to first process the files
    # from the project's reference languages, and then update the ProjectStrings
    # from the other files.
    #
    # After having pass through this interactor,
    # the `context.strings_per_source_files` looks like the following :
    #
    # ```
    # {
    #   "/path/to/the/source/file.ext": {
    #     locale: {
    #       code: en,
    #       source: path/filename/infine
    #     },
    #     nested_keys: true/false,
    #     reference_language: true/false,
    #     strings: {
    #       {
    #         "translation.key.name": "translation value"
    #       },
    #       ...
    #     }
    #   }
    # }
    # ```
    #
    class ExtendSourceFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:strings_per_source_files)
      end

      promises do
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.strings_per_source_files.select do |path, strings|
          locale = try_to_detect_locale_from(path, strings)

          nested_keys = file_uses_nested_keys?(path)

          ref_lang = context.project.reference_languages.include?(
            locale && locale[:code]
          )

          update_source_file_with(path, ref_lang, locale, nested_keys)
        end
      end

      private

      def file_uses_nested_keys?(path)
        File.read(path).scan(/\"([\w\.]+)\":/).flatten.detect do |key|
          key.include?('.')
        end.present?
      end

      def try_to_detect_locale_from(path, strings)
        interactor = Api::StringsImports::DetectTranslationFilesLocales.call(
          locale_codes: context.locale_codes,
          path: path,
          project_local_path: context.project.local_path,
          strings: strings
        )

        # RethinkDB can't index null values as of writing this comment therefore
        # we have to use something else than `nil` so we're going with an empty
        # string.
        # See https://github.com/rethinkdb/rethinkdb/issues/1032
        return interactor.locale if interactor.success?

        # Should never go here, only in case of a technical issue
        context.fail!(errors: interactor.errors)
      end

      def update_source_file_with(path, ref_lang, locale, nested_keys)
        context.strings_per_source_files[path][:reference_language] = ref_lang
        context.strings_per_source_files[path][:locale] = locale
        context.strings_per_source_files[path][:nested_keys] = nested_keys
      end
    end
  end
end
