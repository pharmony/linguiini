# frozen_string_literal: true

module Api
  module Projects
    class UpdateImportStepToImportingValuesFromOtherLocales
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_string_count).filled(:integer)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Only execute on the first import
        return unless context.project_string_count.zero?

        context.project.update!(
          import_step: 'importing_values_from_other_locales'
        )
      end
    end
  end
end
