# frozen_string_literal: true

module Api
  module Projects
    class CheckNameAvailablity
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:exclude_project_id) { none? | str? }
        optional(:id) { none? | str? }
        optional(:name) { none? | str? }
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ensure_id_or_name_is_given

        return unless build_project_query.first

        context.fail!(errors: I18n.t('projects.errors.name_is_unavailable'))
      end

      private

      def build_project_query
        query = Project
        query = query.where(id: context.id) if context.id
        query = query.where(name: context.name) if context.name

        if context.exclude_project_id
          query = query.where(:id.not => context.exclude_project_id)
        end

        query
      end

      def ensure_id_or_name_is_given
        return if context.id || context.name

        context.fail!(errors: 'id or name required')
      end
    end
  end
end
