# frozen_string_literal: true

module Api
  module Projects
    class EnqueueProjectLanguageStatsCacheUpdate
      include Interactor
      include Interactor::Contracts

      expects do
        required(:language).filled
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        Api::ProjectLanguageStatsCacheUpdateJob.perform_later(
          context.language,
          context.project.id
        )
      end
    end
  end
end
