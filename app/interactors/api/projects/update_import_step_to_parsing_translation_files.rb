# frozen_string_literal: true

module Api
  module Projects
    class UpdateImportStepToParsingTranslationFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.update!(import_step: 'parsing_translation_files')
      end
    end
  end
end
