# frozen_string_literal: true

module Api
  module Projects
    class UpdateImportStepToDetectingDuplicatedStrings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.update!(import_step: 'detecting_duplicated_strings')
      end
    end
  end
end
