# frozen_string_literal: true

module Api
  module Projects
    #
    # This interactor builds the `context.project_locales` representing all the
    # locales of the current project.
    #
    class BuildProjectLocales
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale_codes).filled
        required(:project).filled
        required(:strings_per_source_files)
      end

      promises do
        required(:project_locales)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_locales = discover_project_locales
      end

      private

      def discover_project_locales
        context.strings_per_source_files.collect do |_, attributes|
          attributes[:locale] && attributes[:locale][:code]
        end.uniq.compact
      end
    end
  end
end
