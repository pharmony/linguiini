# frozen_string_literal: true

module Api
  module Projects
    class UpdateImportStepToUpdatingExistingStrings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.update!(import_step: 'updating_existing_strings')
      end
    end
  end
end
