# frozen_string_literal: true

module Api
  module Projects
    class UpdateStringsStats
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach do |breaches|
        context.fail!(errors: breaches.to_h.values.to_sentence)
      end

      def call
        context.project.update(
          strings_stats: {
            conflicts: conflict_count,
            duplicates: duplicated_count,
            missing_locale_strings: missing_locale_strings_count,
            translated: translated_count,
            untranslated: untranslated_count
          }
        )
      end

      private

      def conflict_count
        context.project.project_strings.where(conflict: true).count
      end

      def duplicated_count
        project_strings = context.project.project_strings.where(
          duplicated: true
        ).without_ordering.pluck(:key, :locale).raw

        project_strings.each_with_object({}) do |hash, acc|
          acc["#{hash['key']}-#{hash['locale']}"] ||= {}
        end.keys.count
      end

      def translated_count
        context.project
               .project_strings
               .where(conflict: false, :value.not => '').count
      end

      def untranslated_count
        context.project.project_strings.where(
          conflict: false,
          value: ''
        ).count
      end

      def missing_locale_strings_count
        context.project.project_strings.where(locale: '').count
      end
    end
  end
end
