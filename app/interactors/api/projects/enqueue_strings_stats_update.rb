# frozen_string_literal: true

module Api
  module Projects
    class EnqueueStringsStatsUpdate
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        Api::ProjectStringsStatsUpdateJob.perform_later(context.project.id)
      end
    end
  end
end
