# frozen_string_literal: true

module Api
  module Projects
    class Find
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:id) { none? | str? }
        optional(:project_id) { none? | str? }
      end

      promises do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project = Project.where(id: project_id_or_id).first

        return if context.project

        context.fail!(
          errors: I18n.t('errors.resource_not_found',
                         resource_name: I18n.t('resources.project'))
        )
      end

      private

      def project_id_or_id
        context.project_id || context.id
      end
    end
  end
end
