# frozen_string_literal: true

module Api
  module Projects
    class BroadcastDestroyedProjectToTheUi
      include Interactor
      include Interactor::Contracts

      expects do
        required(:destroyed_project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ActionCable.server.broadcast(
          'ProjectsChannel',
          { destroyed_id: context.destroyed_project.id }
        )
      end
    end
  end
end
