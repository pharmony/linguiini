# frozen_string_literal: true

module Api
  module Projects
    class FetchAll
      include Interactor
      include Interactor::Contracts

      promises do
        required(:projects).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.projects = Project.order_by(name: :asc).all
      end
    end
  end
end
