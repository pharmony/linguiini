# frozen_string_literal: true

module Api
  module Projects
    #
    # MD5 sums are used to skip files which have not changed since the last
    # import, making the synchro faster.
    #
    class UpdateFilesMd5s
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:translation_filepaths).filled(:array)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if context.project.update(files_md5s: translation_files_md5s)

        context.fail!(errors: context.project.errors)
      end

      private

      def translation_files_md5s
        context.translation_filepaths.index_with do |path|
          Digest::MD5.hexdigest(File.read(path))
        end
      end
    end
  end
end
