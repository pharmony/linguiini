# frozen_string_literal: true

module Api
  module Projects
    class Create
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:git_options)
        required(:integration).filled
        required(:locales).filled(:array)
        required(:name).filled(:string)
        required(:paths).filled(:string)
        required(:repository_id).filled(:integer)
      end

      promises do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project = Project.new(new_params)

        return if context.project.save

        context.fail!(errors: context.project.errors)
      end

      private

      def new_params
        {
          integration: context.integration,
          name: context.name,
          paths: context.paths,
          reference_languages: context.locales,
          repository_branch_name: context.git_options[:branch_name],
          repository_id: context.repository_id
        }
      end
    end
  end
end
