# frozen_string_literal: true

module Api
  module Projects
    class EnqueueForceImport
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:all_files)
        required(:project).filled
        optional(:git_options)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        Api::ProjectForceImport.perform_later(context.project.id,
                                              context.all_files,
                                              context.git_options)
      end
    end
  end
end
