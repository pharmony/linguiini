# frozen_string_literal: true

module Api
  module Projects
    class UpdateLocales
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_locales)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if context.project_locales.empty?

        import_locales = Array(context.project_locales).compact_blank.sort

        return if no_new_locale_or_locale_update_success?(import_locales)

        context.fail!(error: context.project.errors.full_messages.to_sentence)
      end

      private

      def no_new_locale_or_locale_update_success?(import_locales)
        project_locales == import_locales || context.project.update(
          locales: project_locales | import_locales
        )
      end

      def project_locales
        @project_locales ||= Array(context.project.locales).sort
      end
    end
  end
end
