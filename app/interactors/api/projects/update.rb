# frozen_string_literal: true

module Api
  module Projects
    class Update
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:params).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.update(context.params)
        return if context.project.save

        context.fail!(errors: context.project.errors)
      end
    end
  end
end
