# frozen_string_literal: true

module Api
  class OrganizeProjectStatsUpdate
    include Interactor::Organizer

    organize Projects::Find,
             Projects::EnqueueStringsStatsUpdate
  end
end
