# frozen_string_literal: true

module Api
  class OrganizeFetchingRepositories
    include Interactor::Organizer

    organize Integrations::Find,
             Integrations::FetchAllRepositories
  end
end
