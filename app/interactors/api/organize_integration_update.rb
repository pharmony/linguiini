# frozen_string_literal: true

module Api
  class OrganizeIntegrationUpdate
    include Interactor::Organizer

    organize Integrations::Find,
             Integrations::Update,
             Integrations::Enqueue
  end
end
