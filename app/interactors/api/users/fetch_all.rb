# frozen_string_literal: true

module Api
  module Users
    class FetchAll
      include Interactor
      include Interactor::Contracts

      promises do
        required(:users).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.users = User.all.to_a
      end
    end
  end
end
