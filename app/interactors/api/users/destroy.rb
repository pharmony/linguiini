# frozen_string_literal: true

module Api
  module Users
    class Destroy
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        user = User.where(id: context.id).first

        unless user
          context.fail!(errors: I18n.t('errors.resource_not_found',
                                       resource_name: I18n.t('resources.user')))
        end

        user.destroy
      end
    end
  end
end
