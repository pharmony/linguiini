# frozen_string_literal: true

module Api
  module Users
    class Find
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      promises do
        required(:user).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.user = User.where(id: context.id).first

        return if context.user

        context.fail!(errors: context.user.errors)
      end
    end
  end
end
