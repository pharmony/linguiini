# frozen_string_literal: true

module Api
  module Users
    class Update
      include Interactor
      include Interactor::Contracts

      expects do
        required(:params).filled
        required(:user).filled
      end

      promises do
        required(:user).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.user.update(context.params)

        return if context.user.save

        context.fail!(errors: context.user.errors)
      end
    end
  end
end
