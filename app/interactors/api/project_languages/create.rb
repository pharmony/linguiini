# frozen_string_literal: true

module Api
  module ProjectLanguages
    class Create
      include Interactor
      include Interactor::Contracts

      expects do
        required(:language_value).filled
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        reference_languages = context.project.reference_languages

        create_project_strings_for_new!(reference_languages)
      end

      private

      def build_new_project_string_attributes_from(key, path,
                                                   reference_language)
        {
          conflict: false,
          key: rewrite_language_from(key, reference_language),
          locale: context.language_value,
          path: rewrite_language_from(path, reference_language),
          project: context.project,
          relative_key: relative_key_from(key),
          # RethinkDB can't index null values as of writing this comment
          # therefore we have to use something else than `nil` so we're going
          # with an empty string.
          # See https://github.com/rethinkdb/rethinkdb/issues/1032
          value: ''
        }
      end

      def create_project_strings_for_new!(reference_languages)
        context.project.project_strings.where(
          :locale.in => reference_languages
        ).each do |project_string|
          next if key_existing?(project_string.key, project_string.locale)

          create_project_string_from(project_string, project_string.locale)
        end
      end

      def create_project_string_from(project_string, reference_language)
        new_project_string = ProjectString.new(
          build_new_project_string_attributes_from(project_string.key,
                                                   project_string.path,
                                                   reference_language)
        )

        return if new_project_string.save

        context.fail!(errors: new_project_string.errors)
      end

      def locale_codes
        @locale_codes ||= Locale.all.map(&:code)
      end

      def relative_key_from(key)
        root_key, *others = key.split('.')

        return key unless locale_codes.include?(root_key)

        others.join('.')
      end

      def rewrite_language_from(item, reference_language)
        item.gsub(reference_language, context.language_value)
      end

      def key_existing?(key, locale)
        ProjectString.where(
          key: rewrite_language_from(key, locale)
        ).first
      end
    end
  end
end
