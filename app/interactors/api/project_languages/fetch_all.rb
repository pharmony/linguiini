# frozen_string_literal: true

module Api
  module ProjectLanguages
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      promises do
        required(:languages)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.languages = all_project_locales.map do |locale|
          { locale: locale }
        end
      end

      private

      def all_project_locales
        context.project.reference_languages | context.project.locales
      end
    end
  end
end
