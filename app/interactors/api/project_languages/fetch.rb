# frozen_string_literal: true

module Api
  module ProjectLanguages
    class Fetch
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:language_value).filled
      end

      promises do
        required(:language)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.language = {
          locale: context.language_value,
          number_translations: number_keys_for_language_ref,
          number_translations_translated: number_keys_of_language
        }
      end

      private

      def number_keys_for_language_ref
        ProjectString.where(
          project_id: context.project.id,
          locale: context.project.reference_languages.first
        ).count
      end

      def number_keys_of_language
        ProjectString.where(
          project_id: context.project.id,
          locale: context.language_value,
          :value.not => nil
        ).count
      end
    end
  end
end
