# frozen_string_literal: true

module Api
  class OrganizeProjectStringDestroy
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStrings::Destroy,
             # Refresh pagination after destroy
             ProjectUnusedStrings::FetchAll
  end
end
