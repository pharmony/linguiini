# frozen_string_literal: true

module Api
  class OrganizeProjectUpdate
    include Interactor::Organizer

    organize Projects::Find,
             Projects::Update
  end
end
