# frozen_string_literal: true

module Api
  class OrganizeIntegrationCreation
    include Interactor::Organizer

    organize Integrations::Create,
             Integrations::Enqueue
  end
end
