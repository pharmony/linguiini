# frozen_string_literal: true

module Api
  module StringsImports
    module Importers
      class Yaml
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::HashToStrings

        expects do
          required(:path).filled(:string)
        end

        promises do
          required(:strings)
        end

        on_breach do |breaches|
          context.fail!(errors: breaches.to_h.values.to_sentence)
        end

        def call
          context.strings = reduce_to_strings(YAML.load_file(context.path))
        end
      end
    end
  end
end
