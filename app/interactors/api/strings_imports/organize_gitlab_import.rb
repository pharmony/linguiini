# frozen_string_literal: true

module Api
  module StringsImports
    #
    # Push our SSH key to the project if needed, clone it and import translation
    # files.
    #
    class OrganizeGitlabImport
      include Interactor::Organizer

      organize Projects::UpdateImportStepToRefreshingSourceCode,
               Integrations::GenerateSshKeyIfNeeded,
               Ssh::WritePrivateKeyToTheDisk,
               Integrations::Gitlab::PushSshKeyOnProject,
               Integrations::Gitlab::RetrieveRepositoryMetadata,
               Integrations::Gitlab::PrepareContextForGitClone,
               Git::PrepareGitWorkingDirectory,
               Git::CloneOrPullProjectRepository,
               Git::CheckoutCommitIfNeeded,
               Projects::UpdateImportStepToParsingTranslationFiles,
               StringsImports::RetrieveTranslationFilesFromRepository,
               StringsImports::ParseTranslationFiles,
               Projects::UpdateImportStepToDetectingLocales,
               Locales::BuildLocaleCodes,
               Projects::ExtendSourceFiles,
               Projects::BuildProjectLocales,
               Projects::UpdateImportStepToUpdatingExistingStrings,
               # Counting existing ProjectStrings allows us to know if it's the
               # first import, or if it's a "refresh"
               ProjectStrings::CountExitings,
               ProjectStrings::TryToDetectAndUpdateLocale,
               # | Skipped on the first import
               # v
               ProjectStrings::UpdateValueForReferenceLanguages,
               ProjectStrings::UpdateValueForOtherLangauges,
               Projects::UpdateImportStepToDestroyingRemovedStrings,
               ProjectStrings::DestroyRemovedStrings,
               # ^
               # | Skipped on the first import
               Projects::UpdateImportStepToCreatingStrings,
               ProjectStrings::CreateFromReferenceLanguagesFiles,
               Projects::UpdateImportStepToCreatingStringsWithUnknownLocale,
               ProjectStrings::CreateFromFileWithUnknownLocale,
               # | Only on the first import, fetch the values from the other
               # v locales files
               Projects::UpdateImportStepToImportingValuesFromOtherLocales,
               ProjectStrings::UpdateValuesFromOtherLocales,
               # ^
               # | Only on the first import
               Projects::UpdateLocales,
               Projects::UpdateFilesMd5s,
               Projects::UpdateImportStepToDetectingUnusedStrings,
               ProjectStrings::MarkUnusedStrings,
               Projects::UpdateImportStepToDetectingDuplicatedStrings,
               ProjectStrings::MarkDuplicatedStrings,
               Projects::EnqueueStringsStatsUpdate
    end
  end
end
