# frozen_string_literal: true

module Api
  module StringsImports
    #
    # Tries to detect the locale of the given file path from the filname,
    # dirname or from the infile root node name.
    #
    # The interactor promises a `locale` attribute begin a hash of the following
    # shape :
    #
    # ```
    # { code: 'fr_FR', source: 'filename' }
    # ```
    #
    # In the case of a locale which matches on the filename or the path,
    # an additional attribute is added with a RegEx matching that path :
    #
    # ```
    # { code: 'fr_FR', regex_path: '/path/to/a/\\w+/file.yml',
    #   source: 'path' }
    # ```
    #
    # rubocop:disable Metrics/ClassLength
    class DetectTranslationFilesLocales
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:locale_codes)
        required(:path).filled(:string)
        required(:project_local_path).filled(:string)
        required(:strings)
      end

      promises do
        required(:locale)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Detection stack : the first which detects the locale breaks the stack
        context.locale = try_detect_locale_from_filename ||
                         try_detect_locale_from_path ||
                         try_detect_locale_from_root_node ||
                         nil
      end

      private

      def append_filename_regexp_to(regex, splitted, position)
        splitted.each.with_index do |part, index|
          regex = regexp_merge(regex, index == position ? /\w+/ : /#{part}/)

          # `file_extname` contains the extension name preceeded by a dot so
          # this code adds dots between parts and skips the last dot
          index == (splitted.size - 1) || regex = regexp_merge(regex, /\./)
        end

        regex
      end

      def build_regex_filename_with(position)
        regex = %r{#{Regexp.escape(File.dirname(context.path))}/}

        splitted = File.basename(context.path).gsub(file_extname, '').split('.')

        regex = append_filename_regexp_to(regex, splitted, position)

        regexp_merge(regex, /#{Regexp.escape(file_extname)}/)
      end

      def build_regex_path_with(position)
        regex = //

        splitted = File.dirname(context.path).split(File::SEPARATOR)

        splitted.each.with_index do |part, index|
          regex = if index == position
                    regexp_merge(regex, %r{\w+/})
                  else
                    regexp_merge(regex, %r{#{part}/})
                  end
        end

        regexp_merge(regex, /#{Regexp.escape(File.basename(context.path))}/)
      end

      def ensure_underscore(string)
        string.gsub('-', '_')
      end

      def file_extname
        @file_extname ||= File.extname(context.path)
      end

      def locale_codes
        @locale_codes ||= context.locale_codes ||
                          "(#{Locale.all.map(&:code).join('|')})"
      end

      def regexp_merge(source, content)
        Regexp.new(source.source + content.source)
      end

      def search_locale_from_filename(filename)
        updated_filename = ensure_underscore(filename)

        # en.yml or en-US.yml
        # rubocop:disable Style/GuardClause
        if updated_filename =~ /^#{locale_codes}#{file_extname}$/
          return [updated_filename.gsub(file_extname, ''), 0]
        # devise.en.yml
        elsif updated_filename.gsub(file_extname, '').include?('.')
          splitted = updated_filename.gsub(file_extname, '').split('.')
          splitted.each.with_index do |part, index|
            return [part, index] if locale_codes.include?(part)
          end
        end
        # rubocop:enable Style/GuardClause

        nil
      end

      def search_locale_from_infile
        root_node = context.strings.first&.first
        root_node = root_node.gsub('-', '_') if root_node.to_s.include?('-')

        root_node =~ /^#{locale_codes}$/ ? root_node : nil
      end

      def search_locale_from_path(dirname)
        regex = /^#{locale_codes}$/

        splitted_and_reversed = dirname.split(File::SEPARATOR).reverse

        splitted_and_reversed.each.with_index do |folder_name, index|
          updated_folder_name = ensure_underscore(folder_name)

          next unless updated_folder_name =~ regex

          return [updated_folder_name, splitted_and_reversed.size - index - 1]
        end

        nil
      end

      def try_detect_locale_from_filename
        filename = File.basename(context.path)

        detected, index = search_locale_from_filename(filename)

        return false unless detected

        { code: detected, regex_filename: build_regex_filename_with(index),
          source: 'filename' }
      end

      def try_detect_locale_from_path
        dirname = File.dirname(context.path)

        detected, index = search_locale_from_path(dirname)

        return false unless detected

        { code: detected, regex_path: build_regex_path_with(index),
          source: 'path' }
      end

      def try_detect_locale_from_root_node
        # The first `.first` takes the first Hash entry, wich is converted into
        # an Array of Strings with the first item is the key and the last one
        # the value.
        # The second `.first` takes the key
        detected = search_locale_from_infile

        detected ? { code: detected, source: 'infile' } : false
      end
    end
    # rubocop:enable Metrics/ClassLength
  end
end
