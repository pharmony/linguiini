# frozen_string_literal: true

module Api
  module StringsImports
    #
    # This interactor parse each files from the source code and for every files
    # that has changed since the last import (comparing MD5 fingerprints), it
    # builds the `strings_per_source_files` Hash.
    #
    class ParseTranslationFiles
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:all_files)
        required(:project).filled
        required(:translation_filepaths).filled(:array)
      end

      promises do
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.strings_per_source_files = {}

        context.translation_filepaths.each do |path|
          next unless file_has_been_updated_since_last_import?(path)

          strings = extract_strings_from(path)

          context.strings_per_source_files[path] ||= {}
          context.strings_per_source_files[path][:strings] = strings
        end
      end

      private

      def build_interactor_class_from(path)
        file_extension = File.extname(path)
        case file_extension
        when '.json' then Importers::Json
        when '.yml' then Importers::Yaml
        else
          Rails.logger.error "[#{self.class}] ERROR: Unsupported file " \
                             "extension #{file_extension}."
        end
      end

      def extract_strings_from(path)
        interactor_class = build_interactor_class_from(path)

        interactor = interactor_class.call(path: path)

        return interactor.strings if interactor.success?

        context.fail!(errors: interactor.errors)
      end

      def file_has_been_updated_since_last_import?(path)
        # When the admin checked the all files checkbox from the force import
        # modal, all the files, no matter if they've changed or not, will be
        # proceeded.
        return true if context.all_files

        return true unless file_is_known?(path)

        project_files_md5s[path] != Digest::MD5.hexdigest(File.read(path))
      end

      def file_is_known?(path)
        (project_files_md5s || {}).keys.include?(path)
      end

      def project_files_md5s
        @project_files_md5s ||= context.project.files_md5s
      end
    end
  end
end
