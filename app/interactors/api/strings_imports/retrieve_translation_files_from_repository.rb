# frozen_string_literal: true

module Api
  module StringsImports
    class RetrieveTranslationFilesFromRepository
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:repository_path).filled(:string)
      end

      promises do
        required(:translation_filepaths)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.translation_filepaths = []
        @found_files = nil

        context.project.paths.split(',').each do |translation_path|
          lookup_files_and_fill_translation_filepaths_from(translation_path)
        end

        fail_context_when_needed!

        context.translation_filepaths.sort!
      end

      private

      def fail_context_when_needed!
        return if context.translation_filepaths.present?

        if @found_files
          context.fail!(errors: I18n.t(
            'integrations.no_supported_translation_file_found_in_tree'
          ))
        else
          context.fail!(
            errors: I18n.t('integrations.no_translation_file_found_in_tree')
          )
        end
      end

      def lookup_files_and_fill_translation_filepaths_from(translation_path)
        matching_pattern = matching_pattern(translation_path&.squish!)

        files_found = Dir.glob(matching_pattern)

        @found_files = files_found.size.positive? && @found_files.nil?

        files_found.each do |translation_filepath|
          context.translation_filepaths << translation_filepath
        end
      end

      def matching_pattern(translation_path)
        File.join(
          context.repository_path,
          translation_path,
          '**',
          "*.{#{%w[yaml yml json].join(',')}}"
        )
      end
    end
  end
end
