# frozen_string_literal: true

module Api
  module Roles
    class FetchAll
      include Interactor
      include Interactor::Contracts

      promises do
        required(:roles).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.roles = %w[admin translator]
      end
    end
  end
end
