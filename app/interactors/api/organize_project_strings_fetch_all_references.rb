# frozen_string_literal: true

module Api
  class OrganizeProjectStringsFetchAllReferences
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStrings::FetchAllFromLocale,
             ProjectStrings::FetchMatchingWithReferenceLanguagesIfNeeded
  end
end
