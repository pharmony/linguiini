# frozen_string_literal: true

module Api
  module ProjectExports
    class BuildTranslationsFile
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale_codes).filled
        required(:project).filled
      end

      promises do
        required(:project_strings_result)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        update_project_export_state_to_generating!

        context.project_strings_result = transform_project_strings
      end

      private

      def append_project_strings_to(project_strings, acc)
        project_strings.each do |string|
          acc[string.path] ||= {}
          acc[string.path].deep_merge!(string_as_a_hash(string))
        end
      end

      def autocomplete_value_if_empty(string)
        interactor = autocomplete_value_interactor_klass.call(
          # First time build the cache
          cache: cache_reference_languages_files,
          locale: string.locale,
          project: context.project,
          relative_key: string.relative_key,
          value: string.value
        )

        return interactor.value if interactor.success?

        context.fail!(errors: interactor.errors, string: string)
      end

      def autocomplete_value_interactor_klass
        Api::ProjectStrings::AutcompleteValueFromReferenceLanguageIfNeeded
      end

      def cache_reference_languages_files
        @cache_reference_languages_files ||= begin
          interactor = Api::ProjectStrings::CacheReferenceLanguagesFiles.call(
            project: context.project
          )

          if interactor.success?
            interactor.cache
          else
            context.fail!(errors: interactor.errors)
          end
        end
      end

      # Convert Hash key into nested hashes
      # Like : {"a.b.c"=>"v"} ---> {:a=>{:b=>{:c=>"v"}}}
      # Source: https://stackoverflow.com/a/32344465/1996540
      def flat_keys_to_nested(hash)
        hash.each_with_object({}) do |(key, value), all|
          key_parts = key.split('.').map!(&:to_s)
          leaf = key_parts[0...-1].inject(all) { |h, k| h[k] ||= {} }
          last_part = key_parts.last
          leaf[last_part.number? ? last_part.to_i : last_part] = value
        end
      end

      def string_as_a_hash(string)
        value = autocomplete_value_if_empty(string)

        return { string.key => value } if string.nested_keys

        flat_keys_to_nested(string.key => value)
      end

      def transform_project_strings
        acc = {}

        # 100 000 is the default limit of RethinkDB Array size.
        criteria = context.project.project_strings.without_ordering

        project_files = context.project.project_strings.without_ordering
                               .group_by(&:path).keys

        project_files.each do |path|
          append_project_strings_to(criteria.where(path: path), acc)
        end

        acc
      end

      def update_project_export_state_to_generating!
        context.project_export.generate_files!

        ProjectExportsChannel.broadcast_to(
          context.project_export.id,
          ActiveModelSerializers::SerializableResource.new(
            context.project_export
          )
        )
      end
    end
  end
end
