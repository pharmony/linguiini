# frozen_string_literal: true

module Api
  module ProjectExports
    class FetchPagninatedDiff
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:page)
        required(:project_export).filled
      end

      promises do
        required(:project_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_strings = retrieve_paginated_diff
      end

      private

      def retrieve_paginated_diff
        updates = Kaminari.paginate_array(context.project_export.updates.to_a)

        result = updates.page(context.page || 1)
        result = updates.page(result.total_pages) if result.to_a.empty?
        result
      end
    end
  end
end
