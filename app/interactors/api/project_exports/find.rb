# frozen_string_literal: true

module Api
  module ProjectExports
    class Find
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      promises do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_export = ProjectExport.where(id: context.id).first

        return if context.project_export

        context.fail!(
          errors: I18n.t('errors.resource_not_found',
                         resource_name: I18n.t('resources.project_export'))
        )
      end
    end
  end
end
