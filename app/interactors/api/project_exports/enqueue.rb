# frozen_string_literal: true

module Api
  module ProjectExports
    class Enqueue
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        Api::ProjectExportJob.perform_later(context.project_export.id)
      end
    end
  end
end
