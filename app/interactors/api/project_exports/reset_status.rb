# frozen_string_literal: true

module Api
  module ProjectExports
    class ResetStatus
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_export.state = 'pending'

        return if context.project_export.save

        context.fail!(error: context.project_export.errors)
      end
    end
  end
end
