# frozen_string_literal: true

module Api
  module ProjectExports
    class Create
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:current_user_id).filled(:string)
      end

      promises do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_export = create_project_export

        return if context.project_export.errors.empty?

        context.fail!(errors: context.project_export.errors)
      end

      private

      def project_path
        @project_path ||= begin
          project_path = context.project.local_path
          project_path += '/' unless project_path.ends_with?('/')
          project_path
        end
      end

      def build_updates_array
        updated_project_strings.map do |string|
          {
            key: string['key'],
            locale: string['locale'],
            old_value: string['old_value'],
            path: string['path'].gsub(project_path, ''),
            value: string['value']
          }
        end
      end

      def create_project_export
        ProjectExport.create(
          project: context.project,
          requester_id: context.current_user_id,
          updated_project_string_ids: updated_project_string_ids,
          updates: build_updates_array
        )
      end

      def updated_project_strings
        @updated_project_strings ||= context.project.project_strings.where(
          updated: true
        ).raw.to_a
      end

      def updated_project_string_ids
        updated_project_strings.map { |project_string| project_string['id'] }
      end
    end
  end
end
