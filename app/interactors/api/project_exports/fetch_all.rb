# frozen_string_literal: true

module Api
  module ProjectExports
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:page)
        required(:project).filled
      end

      promises do
        required(:project_exports)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        criteria = context.project.project_exports.order_by(created_at: :desc)

        result = criteria.page(context.page || 1)
        result = criteria.page(result.total_pages) if result.to_a.empty?

        context.project_exports = result
      end
    end
  end
end
