# frozen_string_literal: true

module Api
  module ProjectExports
    class BroadcastToTheUi
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ProjectExportsChannel.broadcast_to(
          context.project_export.id,
          ActiveModelSerializers::SerializableResource.new(
            context.project_export
          )
        )
      end
    end
  end
end
