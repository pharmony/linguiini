# frozen_string_literal: true

module Api
  module ProjectExports
    class UpdateState
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_id).filled
        required(:action).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        find_project_export

        update_project_export

        return if project_export.save

        context.fail!(errors: project_export.errors)
      end

      private

      def find_project_export
        context.project_export = ProjectExport.where(
          project_id: context.project_id
        ).last
      end

      def update_project_export
        if context.errors
          context.project_export.send("#{context.action}!", context.errors)
        else
          context.project_export.send("#{context.action}!")
        end
      end
    end
  end
end
