# frozen_string_literal: true

module Api
  module ProjectExports
    class WriteExportFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_strings_result)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_strings_result.each do |path, strings|
          dirname = File.dirname(path)

          FileUtils.mkdir_p(dirname) unless File.exist?(dirname)

          if File.extname(path) == '.json'
            File.write(path, JSON.pretty_generate(sort_hash(strings)))
          else
            # `deep_stringify_keys` prevents having YAML keys
            # like `:active_admin:`
            File.write(path, sort_hash(strings).to_yaml)
          end
        end
      end

      private

      def keys_classes_from(strings)
        strings.keys.each_with_object({}) do |key, acc|
          acc[key.to_s] = key.class
        end
      end

      def restore_keys_classes(strings, keys_classes)
        strings.transform_keys do |key|
          keys_classes[key] == Integer ? key.to_i : key
        end
      end

      def show_error_and_strings(error, strings)
        Rails.logger.debug do
          "[#{__FILE__.split('app/')[1]}:#{__LINE__}] ERROR: " \
            "#{error.message}\nstrings: #{strings.inspect}"
        end
      end

      # rubocop:disable Metrics/MethodLength
      def sort_hash(strings)
        if strings.is_a?(Hash)
          # Stores the keys classes in a Hash, and use that later to revert the
          # stringify_keys in order to preserve the key's class.
          keys_classes = keys_classes_from(strings)

          # stringify_keys must be used here in order to avoid the error :
          # > comparison of Array with Array failed
          # `sort` can't sort keys when they're not of the same type so that the
          # following example would raise the error :
          # { 'my.key' => 'the value of that key', 1: 'value of my key' }
          sorted = strings.stringify_keys.map { |k, v| [k, sort_hash(v)] }.sort
                          .to_h

          return restore_keys_classes(sorted, keys_classes)
        end

        return strings.map { |str| sort_hash(str) } if strings.is_a?(Array)

        strings
      rescue StandardError => error
        show_error_and_strings(error, strings)

        raise
      end
      # rubocop:enable Metrics/MethodLength
    end
  end
end
