# frozen_string_literal: true

module Api
  class OrganizeRetryingProjectExport
    include Interactor::Organizer

    organize Projects::Find,
             ProjectExports::Find,
             ProjectExports::ResetStatus,
             ProjectExports::Enqueue,
             ProjectExports::BroadcastToTheUi
  end
end
