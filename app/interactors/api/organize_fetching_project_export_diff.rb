# frozen_string_literal: true

module Api
  class OrganizeFetchingProjectExportDiff
    include Interactor::Organizer

    organize ProjectExports::Find,
             ProjectExports::FetchPagninatedDiff
  end
end
