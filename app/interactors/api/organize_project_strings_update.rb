# frozen_string_literal: true

module Api
  class OrganizeProjectStringsUpdate
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStrings::Update,
             Projects::EnqueueStringsStatsUpdate,
             Projects::EnqueueProjectLanguageStatsCacheUpdate
  end
end
