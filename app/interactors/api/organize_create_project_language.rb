# frozen_string_literal: true

module Api
  #
  # Initialise the creation of Project Language
  #
  class OrganizeCreateProjectLanguage
    include Interactor::Organizer

    organize Projects::Find,
             ProjectLanguages::Create,
             Projects::AddNewLocale,
             ProjectLanguages::Fetch
  end
end
