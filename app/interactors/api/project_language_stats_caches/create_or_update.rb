# frozen_string_literal: true

module Api
  module ProjectLanguageStatsCaches
    class CreateOrUpdate
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale).filled
        required(:project).filled
        required(:stats).filled
        required(:stats_cache)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if upsert_doc.errors.blank?

        context.fail!(errors: upsert_doc.errors)
      end

      private

      def upsert_doc
        ProjectLanguageStatsCache.where(
          locale: context.locale,
          project: context.project
        ).first_or_create(
          translateds: context.stats[:translateds],
          translations: context.stats[:translations]
        )
      end
    end
  end
end
