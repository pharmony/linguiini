# frozen_string_literal: true

module Api
  module ProjectLanguageStatsCaches
    class Fetch
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale).filled
        required(:project).filled
      end

      promises do
        required(:stats_cache)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.stats_cache = ProjectLanguageStatsCache.where(
          locale: context.locale,
          project: context.project
        ).first
      end
    end
  end
end
