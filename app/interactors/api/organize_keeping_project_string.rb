# frozen_string_literal: true

module Api
  class OrganizeKeepingProjectString
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStrings::Keep,
             Projects::EnqueueStringsStatsUpdate
  end
end
