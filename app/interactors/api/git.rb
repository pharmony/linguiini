# frozen_string_literal: true

module Api
  module Git
    GIT_REMOTE_NAME = 'origin'
  end
end
