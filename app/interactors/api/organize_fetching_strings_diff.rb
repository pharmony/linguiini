# frozen_string_literal: true

module Api
  #
  # Build a list of updated ProjectStrings used to review the translations
  # before to create a ProjectExport.
  #
  class OrganizeFetchingStringsDiff
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStrings::FetchUpdated
  end
end
