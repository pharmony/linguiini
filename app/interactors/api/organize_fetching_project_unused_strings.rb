# frozen_string_literal: true

module Api
  #
  # Retrieves the ProjectString objects not found in any source files
  #
  class OrganizeFetchingProjectUnusedStrings
    include Interactor::Organizer

    organize Projects::Find,
             ProjectUnusedStrings::FetchAll
  end
end
