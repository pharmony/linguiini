# frozen_string_literal: true

module Api
  module ProjectStrings
    class DestroyRemovedStrings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:strings_per_source_files)
        required(:project_string_count).filled(:integer)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Do nothing on the first import
        return if context.project_string_count.zero?

        return if context.strings_per_source_files.empty?

        to_be_deleted = calculate_project_strings_to_be_deleted

        strings_to_be_deleted = retrieve_project_strings_from(to_be_deleted)

        return if strings_to_be_deleted.empty?

        context.project.project_strings.where(
          :id.in => strings_to_be_deleted
        ).delete_all
      end

      private

      def calculate_project_strings_to_be_deleted
        files = reference_languages_files_from(reference_languages)

        # All the keys from all the reference languages files
        file_keys = files.values
                         .flat_map { |attributes| attributes[:strings].keys }

        # All the keys from all the Project's reference languages
        existing_keys = existing_project_strings_keys_for(files)

        existing_keys_to_be_deleted = existing_keys - file_keys

        # When a key is present in a file from a reference language, it should
        # be ignored here, and later Linguiini will create the missing key for
        # the other reference languages : A key present in a reference language
        # must be present in all languages.
        existing_keys_to_be_deleted.select do |key|
          reference_languages.include?(key.split('.').first) == false
        end
      end

      def existing_project_strings_keys_for(files)
        context.project.project_strings.where(
          :locale.in => reference_languages,
          # `files` is a Hash which has the file paths as key
          :path.in => files.keys
        ).pluck(:key).raw.collect { |hash| hash['key'] }
      end

      def reference_languages
        context.project.reference_languages
      end

      def reference_languages_files_from(locales)
        context.strings_per_source_files.select do |_, attributes|
          attributes[:reference_language] == true &&
            locales.include?(attributes[:locale][:code])
        end
      end

      def retrieve_project_strings_from(to_be_deleted)
        # `to_be_deleted` are the keys to be deleted from the reference
        # languages, we now need to retreive all the keys from all the other
        # locales
        to_be_deleted.flat_map do |key|
          root_key, *others = key.split('.')
          root_key = '\w+' if reference_languages.include?(root_key)
          key_regex = /^#{root_key}\.#{others.join('\.')}$/

          context.project.project_strings.where(
            key: key_regex
          ).without_ordering.pluck(:id).raw.to_a.collect { |hash| hash['id'] }
        end
      end
    end
  end
end
