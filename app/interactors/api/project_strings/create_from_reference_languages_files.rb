# frozen_string_literal: true

module Api
  module ProjectStrings
    # rubocop:disable Metrics/ClassLength
    class CreateFromReferenceLanguagesFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_locales)
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        reference_languages_files.map do |path, attributes|
          next if attributes[:strings].nil?

          strings_te_be_created_from(
            attributes[:locale],
            attributes[:strings]
          ).each_slice(200).each do |slice|
            mass_insert!(slice, path, attributes[:locale],
                         attributes[:nested_keys])
          end
        end
      end

      private

      def a_reference_language?(key)
        context.project.reference_languages.include?(key)
      end

      def build_doc_path_from_filename(path, locale_code, locale)
        *path_others, filename = path.split(File::SEPARATOR)
        fileext = File.extname(path)
        name = filename.gsub(fileext, '')
        updated_name = name.split('.').map do |part|
          part == locale_code ? locale : part
        end
        updated_name = updated_name.join('.') + File.extname(path)
        (path_others << updated_name).join('/')
      end

      def build_doc_path_from(current_locale, path, locale)
        case current_locale[:source]
        when 'filename'
          build_doc_path_from_filename(path, current_locale[:code], locale)
        when 'path'
          path.split(File::SEPARATOR).map do |part|
            part.gsub('-', '_') == current_locale[:code] ? locale : part
          end.join('/')
        else
          unknown_locale_source_at!(__FILE__.split('app/')[1], __LINE__)
        end
      end

      def build_path_cache_for(locale)
        query_path = case locale[:source]
                     when 'filename' then locale[:regex_filename]
                     when 'path' then locale[:regex_path]
                     end

        # As we are on a reference language, we should search by `key` instead
        # of `path`, but it's very slow, so a tradeoff is to use path with
        # a RegEx
        @path_cache = context.project.project_strings.where(
          path: query_path
        ).without_ordering.pluck(:key, :locale).raw.to_a
      end

      def build_project_string_defaults_with(nested_keys)
        project_string_defaults.merge(
          nested_keys: nested_keys, new_value: '', old_value: '',
          project_id: context.project.id
        )
      end

      def build_source_files_strings_from(source_files)
        source_files.values.each_with_object({}) do |hash, acc|
          acc.merge!(hash[:strings])
        end
      end

      def build_strings_to_be_created_from(key_strings)
        # Builds an Array of locales for which the given ProjectString hash are
        # missing
        if key_strings.empty?
          # Create a ProjectString for each of the project's locales
          context.project_locales
        else
          key_locales = key_strings.collect { |hash| hash['locale'] }
          context.project_locales - key_locales
        end
      end

      def find_missing_project_strings_for(key, locale)
        if root_key_is_the_locale?(key, locale)
          _, *others = key.split('.')
          escaped_others = others.collect { |other| Regexp.escape(other) }
          key_regex = /^\w+\.#{escaped_others.join('\.')}$/

          @path_cache.select { |hash| hash['key'] =~ key_regex }
        else
          @path_cache.select { |hash| hash['key'] == key }
        end
      end

      def should_root_key_be_replaced?(key)
        # When the key has no dots, then it doesn't have a 'root' that could be
        # replaced, so just return `replace_key_root` as `false`, and the key
        # itself.
        return [false, key] unless key.include?('.') || key.include?('-')

        root_key, *others = key.split('.')
        root_key.gsub!('-', '_')
        replace_key_root = a_reference_language?(root_key)

        [replace_key_root, others]
      end

      # rubocop:disable Metrics/MethodLength
      def build_project_string_attributes_with(key, locale, current_locale,
                                               path, nested_keys)
        replace_key_root, others = should_root_key_be_replaced?(key)

        now = Time.zone.now

        build_project_string_defaults_with(nested_keys).merge(
          created_at: now,
          key: replace_key_root ? [locale, *others].join('.') : key,
          locale: locale,
          path: build_doc_path_from(current_locale, path, locale),
          relative_key: replace_key_root ? others.join('.') : key,
          updated_at: now,
          value: retrieve_string_value_for(key, locale, replace_key_root)
        )
      end
      # rubocop:enable Metrics/MethodLength

      def mass_insert!(new_strings, path, current_locale, nested_keys)
        docs = new_strings.flat_map do |key, locales|
          locales.map do |locale|
            build_project_string_attributes_with(key, locale, current_locale,
                                                 path, nested_keys)
          end
        end

        # rubocop:disable Rails/SkipsModelValidations
        ProjectString.insert_all(docs)
        # rubocop:enable Rails/SkipsModelValidations
      end

      #
      # Since we're using ProjectString.insert_all, we're not using the model,
      # and therefore we don't have automatic default values so we made this
      # method which returns a Hash of the fields with their default values.
      #
      def project_string_defaults
        @project_string_defaults ||= begin
          # Takes all the ProjectString fields excluding the :id field
          fields = ProjectString.fields.reject { |k| k == :id }
          # Gets their default value
          fields.transform_values { |value| value[:default] }
        end
      end

      def reference_languages_files
        @reference_languages_files ||=
          context.strings_per_source_files.select do |_, attributes|
            attributes[:reference_language] == true
          end
      end

      def retrieve_string_value_for(key, locale, replace_key_root)
        source_files = strings_per_source_files_for_locale(locale)

        return '' unless source_files

        source_files_strings = build_source_files_strings_from(source_files)

        string_value_from(source_files_strings, key, replace_key_root)
      end

      def root_key_is_the_locale?(key, locale)
        # There could be two cases when looking up for the key :
        # * The root of the key (the first part before the first dot) is
        #   the current key's locale
        # * The key doesn't includes the locale at all
        #
        # In the first case, as we're working with a key from a file of
        # a reference language, we must create the key for all
        # the other locales. Doing so will require to change the key root in
        # order to match the locale so that a key like `fr.hello` would become
        # `en.hello`.
        root_key, = key.split('.')
        root_key == locale[:code]
      end

      def strings_per_source_files_for_locale(locale)
        context.strings_per_source_files.select do |_, attributes|
          next unless attributes[:locale]

          attributes[:locale][:code] == locale
        end
      end

      def strings_te_be_created_from(locale, strings)
        build_path_cache_for(locale)

        strings.keys.each_with_object({}) do |key, acc|
          # Makes it compatible with locale codes using dashes instead of
          # underscores like `en-US`.
          underscore_key = underscore_key(key)

          # Skip files with only the root key being the locale key like with a
          # YAML file where all the keys have been deleted.
          next if underscore_key == locale[:code]

          # `key_strings` will be empty when `key` is new, otherwise it will
          # contain a Hash of ProjectString's key and locale which exist already
          # for the `key`.
          key_strings = find_missing_project_strings_for(underscore_key, locale)

          # `string_locales` will then contain the list of locales for which the
          # `key` needs to be created in order to have a ProjectString for each
          # of the Project's locales.
          string_locales = build_strings_to_be_created_from(key_strings)

          # Avoids inserting blank pair when the key exist for each of the
          # Project's locales.
          acc[key] = string_locales if string_locales.present?
        end
      end

      def string_value_from(source_files_strings, key, replace_key_root)
        if replace_key_root
          _, *others = key.split('.')

          string = source_files_strings.detect do |string_key, _|
            _, *string_others = string_key.split('.')

            others == string_others
          end

          string ? string.last : ''
        else
          source_files_strings[key] || ''
        end
      end

      def underscore_key(key)
        return key unless key.include?('.')

        root, *others = key.split('.')

        # Adds a `nil` so that `.join` will re-create the key ending with a
        # dot.
        others << nil if key.ends_with?('.')

        [root.gsub('-', '_'), *others].join('.')
      end

      def unknown_locale_source_at!(path, line)
        context.fail!(
          error: 'Unknown locale source ' \
                 "#{current_locale[:source].inspect}. Please update the " \
                 "code at #{path}:#{line}."
        )
      end
    end
    # rubocop:enable Metrics/ClassLength
  end
end
