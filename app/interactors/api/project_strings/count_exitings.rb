# frozen_string_literal: true

module Api
  module ProjectStrings
    #
    # Runs the `.count` on project's ProjectString only once and skip all the
    # possible Interactors.
    #
    class CountExitings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      promises do
        required(:project_string_count).filled(:integer)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_string_count = context.project.project_strings.count
      end
    end
  end
end
