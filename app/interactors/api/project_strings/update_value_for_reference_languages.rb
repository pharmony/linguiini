# frozen_string_literal: true

module Api
  module ProjectStrings
    class UpdateValueForReferenceLanguages # rubocop:disable Metrics/ClassLength
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_string_count).filled(:integer)
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Do nothing on the first import
        return if context.project_string_count.zero?

        paths, strings = strings_from_reference_languages_files

        update_project_strings_values!(paths, strings) if paths.present?
      rescue NoBrainer::Error::DocumentInvalid => error
        context.fail!(errors: error.message)
      end

      private

      def autocomplete_value_interactor_klass
        Api::ProjectStrings::AutcompleteValueFromReferenceLanguageIfNeeded
      end

      def cache_reference_languages_files
        @cache_reference_languages_files ||= begin
          interactor = Api::ProjectStrings::CacheReferenceLanguagesFiles.call(
            project: context.project
          )

          if interactor.success?
            interactor.cache
          else
            context.fail!(errors: interactor.errors)
          end
        end
      end

      def compare_values(value, next_value)
        return value == next_value.unicode_normalize if next_value.is_a?(String)

        value == next_value
      end

      def log_project_string_update_for(project_string, next_value)
        key = project_string.key

        Rails.logger.debug do
          "[debug(#{__FILE__.split('app/')[1]}:#{__LINE__})] Updating " \
            "#{'un' unless project_string.translated}translated " \
            "ProjectString with ID #{project_string.id} and key " \
            "#{key.inspect} from #{project_string.value.inspect} " \
            "(#{project_string.value.class.name}) to " \
            "#{next_value.inspect} (#{next_value.class.name}) ..."
        end
      end

      def no_value_update_available?(current_value, new_value)
        case current_value
        when FalseClass then new_value == false
        when TrueClass then new_value == true
        when String
          # When new_value is nil, compare with an empty string
          return current_value == '' unless new_value

          current_value == new_value.unicode_normalize
        else
          current_value == new_value
        end
      end

      def project_string_update_is_autocomplete?(project_string, next_value)
        return false unless next_value

        interactor = autocomplete_value_interactor_klass.call(
          # First time build the cache
          cache: cache_reference_languages_files,
          locale: project_string.locale,
          project: context.project,
          relative_key: project_string.relative_key,
          value: project_string.value
        )

        context.fail!(errors: interactor.errors) if interactor.failure?

        compare_values(interactor.value, next_value)
      end

      def project_strings_from(paths)
        context.project.project_strings.where(
          :path.in => paths,
          :locale.in => context.project.reference_languages
        ).without_ordering
      end

      def reference_languages_files
        context.strings_per_source_files.select do |_, attributes|
          attributes[:reference_language] == true
        end
      end

      def strings_from_reference_languages_files
        paths = []

        files = reference_languages_files

        strings = files.each_with_object({}) do |(path, attr), acc|
          paths |= [path]
          acc[attr[:locale][:code]] ||= {}
          acc[attr[:locale][:code]].merge!(attr[:strings])
        end

        [paths, strings]
      end

      def update_project_strings_values!(paths, strings)
        project_strings_from(paths).each do |project_string|
          next_value = strings[project_string.locale][
                         project_string.key
                       ]

          next if no_value_update_available?(project_string.value, next_value)

          next if project_string_update_is_autocomplete?(project_string,
                                                         next_value)

          log_project_string_update_for(project_string, next_value)

          project_string.update!(conflict: true, new_value: next_value)
        end
      end
    end
  end
end
