# frozen_string_literal: true

module Api
  module ProjectStrings
    class TryToDetectAndUpdateLocale
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale_codes).filled
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project.project_strings.where(
          locale: ''
        ).without_ordering.each do |item|
          locale = try_to_detect_locale_from(item.path, { item.key => '' })

          next unless locale

          item.update(locale: locale[:code])
        end
      end

      private

      def try_to_detect_locale_from(path, strings)
        interactor = Api::StringsImports::DetectTranslationFilesLocales.call(
          locale_codes: context.locale_codes,
          path: path,
          project_local_path: context.project.local_path,
          strings: strings
        )

        # RethinkDB can't index null values as of writing this comment therefore
        # we have to use something else than `nil` so we're going with an empty
        # string.
        # See https://github.com/rethinkdb/rethinkdb/issues/1032
        return interactor.locale if interactor.success?

        # Should never go here, only in case of a technical issue
        context.fail!(errors: interactor.errors)
      end
    end
  end
end
