# frozen_string_literal: true

module Api
  module ProjectStrings
    #
    # This interactor build a cache of the given project strings for its
    # reference languages.
    # That cache is used by the "autocomplete" feature on the import and the
    # export of a project.
    #
    # See app/interactors/api/project_exports/build_translations_file.rb and
    # app/interactors/api/project_strings/update_value_for_other_langauges.rb
    class CacheReferenceLanguagesFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      promises do
        required(:cache).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.cache = reference_languages_files(
          reference_languages_paths
        ).each_with_object({}) do |file, acc|
          acc[file['locale']] ||= {}
          acc[file['locale']].merge!(file['relative_key'] => file['value'])
        end
      end

      private

      def reference_languages_files(paths)
        context.project.project_strings.where(
          :locale.in => context.project.reference_languages,
          :path.in => paths
        ).without_ordering.pluck(:relative_key, :locale, :value).raw.to_a
      end

      def reference_languages_paths
        @reference_languages_paths ||= context.project.project_strings.where(
          :locale.in => context.project.reference_languages
        ).without_ordering.pluck(:path).raw.collect { |hash| hash['path'] }
      end
    end
  end
end
