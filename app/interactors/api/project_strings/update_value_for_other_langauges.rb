# frozen_string_literal: true

module Api
  module ProjectStrings
    #
    # This interactor is responsible for updating ProjectString from other
    # locales than project's reference languages.
    #
    class UpdateValueForOtherLangauges # rubocop:disable Metrics/ClassLength
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_string_count).filled(:integer)
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Skip this interactor on the first import
        return if context.project_string_count.zero?

        other_locales_files.each do |_, attributes|
          # Skips strings with an unknown locale
          next unless attributes[:locale]

          build_path_cache_for(attributes[:locale])

          update_project_strings_values!(attributes[:locale][:code],
                                         attributes[:strings])
        end
      rescue NoBrainer::Error::DocumentInvalid => error
        context.fail!(errors: error.message)
      end

      private

      def autocomplete_value_interactor_klass
        Api::ProjectStrings::AutcompleteValueFromReferenceLanguageIfNeeded
      end

      def build_path_cache_for(locale)
        query_path = case locale[:source]
                     when 'filename' then locale[:regex_filename]
                     when 'path' then locale[:regex_path]
                     end

        # As we are on a reference language, we should search by `key` instead
        # of `path`, but it's very slow, so a tradeoff is to use path with
        # a RegEx
        @path_cache = context.project.project_strings.where(
          path: query_path
        ).pluck(
          :id, :key, :locale, :new_value, :relative_key, :translated, :value
        ).without_ordering.raw.to_a
      end

      def cache_reference_languages_files
        @cache_reference_languages_files ||= begin
          interactor = Api::ProjectStrings::CacheReferenceLanguagesFiles.call(
            project: context.project
          )

          if interactor.success?
            interactor.cache
          else
            context.fail!(errors: interactor.errors)
          end
        end
      end

      def compare_values(value, next_value)
        return value == next_value.unicode_normalize if next_value.is_a?(String)

        value == next_value
      end

      def log_project_string_update_for(hash, next_value)
        key = hash['key']

        Rails.logger.debug do
          "[debug(#{__FILE__.split('app/')[1]}:#{__LINE__})] Updating " \
            "#{'un' unless hash['translated']}translated ProjectString with " \
            "ID #{hash['id']} and key #{key.inspect} from " \
            "#{hash['value'].inspect} (#{hash['value'].class.name}) to " \
            "#{next_value.inspect} (#{next_value.class.name}) ..."
        end
      end

      def other_locales_files
        context.strings_per_source_files.select do |_, attributes|
          attributes[:reference_language] == false
        end
      end

      def project_string_update_is_autocomplete?(hash, next_value)
        return false unless next_value

        interactor = autocomplete_value_interactor_klass.call(
          # First time build the cache
          cache: cache_reference_languages_files,
          locale: hash['locale'],
          project: context.project,
          relative_key: hash['relative_key'],
          value: hash['value']
        )

        context.fail!(errors: interactor.errors) if interactor.failure?

        compare_values(interactor.value, next_value)
      end

      def translated_project_string_need_update?(hash, strings)
        value = strings[hash['key']]
        value = value.unicode_normalize if value.is_a?(String)

        return false if hash['value'] == value
        return false if hash['translated'] == false

        new_value = strings[hash['key']]
        new_value = new_value.unicode_normalize if new_value.is_a?(String)

        return false if hash['new_value'] == new_value

        true
      end

      def untranslated_project_string_need_update?(hash, strings)
        value = strings[hash['key']]
        value = value.unicode_normalize if value.is_a?(String)

        return false if hash['value'] == value
        return false if hash['translated'] == true

        true
      end

      def update_project_string!(hash, strings)
        # When there's an update available for a string which has never been
        # touched in Linguiini, then we can update the ProjectString value
        update_untranslated_project_string_if_needed!(hash, strings)

        # When there's an update available for the string which has been touched
        # in Linguiini, and that update has not been treated before,
        # then proposes the ProjectString update by marking it in conflit and
        # storing the update in the `new_value` field.
        update_translated_project_string_if_needed!(hash, strings)
      end

      def update_project_strings_values!(locale, strings)
        keys = strings.keys

        existings = @path_cache.select do |hash|
          keys.include?(hash['key']) && hash['locale'] == locale
        end

        existings.each do |existing|
          update_project_string!(existing, strings)
        end
      end

      def update_translated_project_string_if_needed!(hash, strings)
        return unless translated_project_string_need_update?(hash, strings)

        log_project_string_update_for(hash, strings)

        ProjectString.find(hash['id']).update!(
          conflict: true,
          new_value: strings[hash['key']]
        )
      end

      def update_untranslated_project_string_if_needed!(hash, strings)
        return unless untranslated_project_string_need_update?(hash, strings)

        next_value = strings[hash['key']]

        return if project_string_update_is_autocomplete?(hash, next_value)

        log_project_string_update_for(hash, next_value)

        ProjectString.find(hash['id']).update!(value: next_value)
      end
    end
  end
end
