# frozen_string_literal: true

module Api
  module ProjectStrings
    class MarkDuplicatedStrings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        duplicated_key_ids = detect_duplicated_keys.flat_map do |_, value|
          value[:ids]
        end

        return if duplicated_key_ids.empty?

        # rubocop:disable Rails/SkipsModelValidations
        context.project.project_strings.where(
          :id.in => duplicated_key_ids
        ).update_all(duplicated: true, with_issue: true)
        # rubocop:enable Rails/SkipsModelValidations
      end

      private

      def all_duplicates
        project_project_strings.each_with_object({}) do |ps, acc|
          acc["#{ps['key']}-#{ps['locale']}"] ||= { count: 0, ids: [] }

          acc["#{ps['key']}-#{ps['locale']}"][:count] += 1
          acc["#{ps['key']}-#{ps['locale']}"][:ids] << ps['id']
        end
      end

      def detect_duplicated_keys
        all_duplicates.select { |_, value| value[:count] > 1 }
      end

      def project_project_strings
        context.project.project_strings.pluck(
          :id,
          :key,
          :locale
        ).without_ordering.raw
      end
    end
  end
end
