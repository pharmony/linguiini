# frozen_string_literal: true

module Api
  module ProjectStrings
    class FetchAllFromLocale
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:empty_only)
        optional(:filter)
        required(:locale).filled(:string)
        optional(:page)
        required(:project).filled
        required(:sort)
      end

      promises do
        required(:project_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        criteria = build_base_criteria

        criteria = filter_when_needed(criteria)

        criteria = sort(criteria)

        result = criteria.page(context.page || 1)
        result = criteria.page(result.total_pages) if result.to_a.empty?

        context.project_strings = result
      end

      private

      def build_base_criteria
        context.project.project_strings.where(locale: context.locale)
      end

      def criteria_column
        :"#{context.filter['column']}".to_sym
      end

      def criteria_value
        Regexp.escape(context.filter['value'])
      end

      def filter_docs(criteria)
        return filter_on_docs_value(criteria) if criteria_column == :value

        # Default / simple filtering
        criteria.where(criteria_column => /^.*#{criteria_value}.*$/)
      end

      def filter_on_docs_value(criteria)
        criteria.where do |doc|
          RethinkDB::RQL.new.branch( # if-else-end
            # Only filter on String values
            doc[:value].type_of.eq('STRING'),
            # True branch
            doc[:value].match("(?m)^.*#{criteria_value}.*$"),
            # False branch
            nil
          )
        end
      end

      def filter_present?
        return false if context.filter.blank?
        return false unless context.filter.keys.sort == %w[column value]

        context.filter['value'].present?
      end

      def filter_when_needed(criteria)
        criteria = filter_docs(criteria) if filter_present?

        criteria = criteria.where(value: '') if context.empty_only == 'true'

        criteria
      end

      def sort(criteria)
        case context.sort
        when 'alpha'
          criteria.order_by(:key)
        when 'newests'
          criteria.order_by(created_at: :desc)
        else
          criteria
        end
      end
    end
  end
end
