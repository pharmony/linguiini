# frozen_string_literal: true

module Api
  module ProjectStrings
    #
    # This interactor is used ONLY on the first project import to run the
    # value update from other locales so that the imported project is complete.
    #
    # This is necessary because of the way the
    # Api::StringsImports::OrganizeGitlabImport organizer has been built.
    #
    # See app/interactors/api/strings_imports/organize_gitlab_import.rb.
    #
    class UpdateValuesFromOtherLocales
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:project_locales)
        required(:project_string_count).filled(:integer)
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        # Only execute on the first import
        return unless context.project_string_count.zero?

        return if context.strings_per_source_files.empty?

        call_update_value_for_reference_languages

        call_update_value_for_other_languages
      end

      private

      def call_update_value_for_other_languages
        interactor = ProjectStrings::UpdateValueForOtherLangauges.call(
          update_value_for_other_languages_params
        )

        return if interactor.success?

        context.fail!(errors: interactor.errors)
      end

      def call_update_value_for_reference_languages
        interactor = ProjectStrings::UpdateValueForReferenceLanguages.call(
          update_value_for_reference_languages_params
        )

        return if interactor.success?

        context.fail!(errors: interactor.errors)
      end

      def update_value_for_other_languages_params
        {
          project: context.project,
          project_locales: context.project_locales,
          # Fakes the count in order to allow the interactor to run
          project_string_count: 1,
          strings_per_source_files: context.strings_per_source_files
        }
      end

      def update_value_for_reference_languages_params
        {
          project: context.project,
          # Fakes the count in order to allow the interactor to run
          project_string_count: 1,
          strings_per_source_files: context.strings_per_source_files
        }
      end
    end
  end
end
