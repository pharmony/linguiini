# frozen_string_literal: true

module Api
  module ProjectStrings
    class CreateFromFileWithUnknownLocale
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:strings_per_source_files)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        files_with_unknown_locale.each do |path, attributes|
          next if attributes[:strings].nil?

          strings_te_be_created_from(
            path,
            attributes[:strings]
          ).each_slice(200).each do |slice|
            mass_insert!(slice, path, attributes[:nested_keys])
          end
        end
      end

      private

      def build_path_cache_for(path)
        @path_cache = context.project.project_strings.where(
          path: path
        ).pluck(:key, :locale).raw.to_a
      end

      def build_project_string_defaults_with(nested_keys)
        project_string_defaults.merge(
          nested_keys: nested_keys, new_value: '', old_value: '',
          project_id: context.project.id
        )
      end

      def build_project_string_attributes_with(key, path, nested_keys, value)
        now = Time.zone.now

        build_project_string_defaults_with(nested_keys).merge(
          created_at: now,
          key: key,
          # RethinkDB can't index null values as of writing this comment
          # therefore we have to use something else than `nil` so we're going
          # with an empty string.
          # See https://github.com/rethinkdb/rethinkdb/issues/1032
          locale: '',
          path: path,
          relative_key: key,
          updated_at: now,
          value: value
        )
      end

      #
      # Since we're using ProjectString.insert_all, we're not using the model,
      # and therefore we don't have automatic default values so we made this
      # method which returns a Hash of the fields with their default values.
      #
      def project_string_defaults
        @project_string_defaults ||= begin
          # Takes all the ProjectString fields excluding the :id field
          fields = ProjectString.fields.reject { |k| k == :id }
          # Gets their default value
          fields.transform_values { |value| value[:default] }
        end
      end

      def files_with_unknown_locale
        context.strings_per_source_files.select do |_, attributes|
          attributes[:locale].nil?
        end
      end

      def find_missing_project_strings_for(key)
        @path_cache.select { |hash| hash['key'] == key }
      end

      def mass_insert!(new_strings, path, nested_keys)
        docs = new_strings.flat_map do |key, value|
          build_project_string_attributes_with(key, path, nested_keys, value)
        end

        # rubocop:disable Rails/SkipsModelValidations
        ProjectString.insert_all(docs)
        # rubocop:enable Rails/SkipsModelValidations
      end

      def strings_te_be_created_from(path, strings)
        build_path_cache_for(path)

        existing_keys = @path_cache.collect do |hash|
          hash['key'] if strings.keys.include?(hash['key'])
        end.compact

        keys_to_be_created = strings.keys - existing_keys

        strings.select { |key, _| keys_to_be_created.include?(key) }
      end
    end
  end
end
