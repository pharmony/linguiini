# frozen_string_literal: true

module Api
  module ProjectStrings
    class ClearUpdated
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_export.project.project_strings.where(
          :id.in => context.project_export.updated_project_string_ids
        ).each do |project_string|
          project_string.update(updated: false)
        end
      end
    end
  end
end
