# frozen_string_literal: true

module Api
  module ProjectStrings
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      promises do
        required(:project_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_strings = ProjectString.where(
          project_id: context.project.id
        )
      end
    end
  end
end
