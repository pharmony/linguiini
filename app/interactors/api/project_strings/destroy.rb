# frozen_string_literal: true

module Api
  module ProjectStrings
    class Destroy
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ensure_project_string_exists!

        context.project.project_strings.where(id: context.id).first.delete
      end

      private

      def ensure_project_string_exists!
        return if context.project.project_strings.where(id: context.id).present?

        context.fail!(
          errors: I18n.t('errors.resource_not_found',
                         resource_name: I18n.t('resources.project_string'))
        )
      end
    end
  end
end
