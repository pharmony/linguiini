# frozen_string_literal: true

module Api
  module ProjectStrings
    class FetchUpdated
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:page)
        required(:project).filled
      end

      promises do
        required(:project_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.project_strings = if context.check_only
                                    check_if_there_any_updated_strings?
                                  else
                                    retrieve_updated_strings
                                  end
      end

      private

      def build_query_criteria
        context.project.project_strings.where(updated: true)
      end

      def check_if_there_any_updated_strings?
        {
          has_any: build_query_criteria.without_ordering.count.positive?
        }
      end

      def retrieve_updated_strings
        criteria = build_query_criteria.order_by(:key)

        result = criteria.page(context.page || 1)
        result = criteria.page(result.total_pages) if result.to_a.empty?
        result
      end
    end
  end
end
