# frozen_string_literal: true

module Api
  module ProjectStrings
    class Keep
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
        required(:id).filled(:string)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        to_be_kept = find_project_string

        to_be_destroyed = find_other_project_strings_from(to_be_kept)
        context.destroyed_ids = to_be_destroyed.collect(&:id)

        destroy_duplicates_from(to_be_destroyed)

        keep_project_string(to_be_kept)
        context.kept_id = to_be_kept.id
      end

      private

      def destroy_duplicates_from(project_strings)
        project_strings.each(&:delete)
      end

      def find_other_project_strings_from(project_string)
        context.project.project_strings.where(key: project_string.key,
                                              locale: project_string.locale,
                                              :id.not => context.id)
      end

      def find_project_string
        context.project.project_strings.where(id: context.id).first
      end

      def keep_project_string(project_string)
        return if project_string.update(duplicated: false)
      end
    end
  end
end
