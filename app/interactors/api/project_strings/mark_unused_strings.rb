# frozen_string_literal: true

module Api
  module ProjectStrings
    class MarkUnusedStrings
      include Interactor
      include Interactor::Contracts

      expects do
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        unused_key_ids = detect_unused_keys.collect { |unused| unused['id'] }

        return if unused_key_ids.empty?

        # rubocop:disable Rails/SkipsModelValidations
        context.project.project_strings.where(
          :id.in => unused_key_ids
        ).update_all(unused: true)
        # rubocop:enable Rails/SkipsModelValidations
      end

      private

      def cleaned_key_from(object)
        return object['key'] unless object['key'].include?('.')

        first, *others = object['key'].split('.')

        first == object['locale'] ? others.join('.') : object['key']
      end

      def detect_unused_keys
        reference_keys = fetch_project_reference_keys.collect do |object|
          cleaned_key_from(object)
        end.sort

        fetch_project_translations_keys.select do |object|
          cleaned_key = cleaned_key_from(object)
          reference_keys.include?(cleaned_key) == false
        end
      end

      def fetch_project_reference_keys
        uniq_hash_on('key') do
          context.project.project_strings.where(
            :locale.in => context.project.reference_languages
          ).pluck(:id, :key, :locale).without_ordering.raw.to_a
        end
      end

      def fetch_project_translations_keys
        uniq_hash_on('key') do
          context.project.project_strings.where(
            :locale.in => context.project.locales
          ).pluck(:id, :key, :locale).without_ordering.raw.to_a
        end
      end

      def uniq_hash_on(key, &block)
        block.call.uniq { |project_string| project_string[key] }
      end
    end
  end
end
