# frozen_string_literal: true

module Api
  module ProjectStrings
    class Update
      include Interactor
      include Interactor::Contracts

      expects do
        required(:translations).filled
        required(:project).filled
        required(:language).filled
      end

      promises do
        required(:updated_strings).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.updated_strings = []

        to_be_updated.each do |project_string|
          value = context.translations.detect do |id, _|
            id == project_string.id
          end.last

          update_translation(project_string, value)

          context.updated_strings << project_string
        end
      end

      private

      def to_be_updated
        # context.translations is an Array of Hash, with the key being the
        # ProjectString ID, and the value the translating update
        project_string_ids = context.translations.collect(&:first)

        context.project.project_strings.where(:id.in => project_string_ids,
                                              locale: context.language).all
      end

      def update_translation(project_string, value)
        return if project_string.update(
          old_value: project_string.value,
          translated: true,
          updated: true,
          value: value
        )

        context.fail!(errors: project_string.errors)
      end
    end
  end
end
