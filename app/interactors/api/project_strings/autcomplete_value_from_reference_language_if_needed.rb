# frozen_string_literal: true

module Api
  module ProjectStrings
    class AutcompleteValueFromReferenceLanguageIfNeeded
      include Interactor
      include Interactor::Contracts

      expects do
        required(:cache).filled
        required(:locale).filled(:string)
        required(:project).filled
        required(:relative_key).filled(:string)
        required(:value)
      end

      promises do
        required(:value)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if string_value_is_present?

        autocomplete_value_from_reference_languages

        return unless autocomplete_success?

        update_context_value_with_autocompleted
      end

      private

      # When none of the reference languages have a value
      def autocomplete_success?
        return true if @detected_reference_language.present?

        context.value ||= ''

        false
      end

      def autocomplete_value_from_reference_languages
        @detected_reference_language = ordered_reference_languages_for(
          context.locale
        ).detect do |language|
          cache_has_key_with_value?(language, context.relative_key)
        end
      end

      def cache_has_key_with_value?(language, relative_key)
        reference_language_values = context.cache[language]

        has_key = reference_language_values.keys.include?(relative_key)
        has_value = reference_language_values[relative_key].to_s.present?

        has_key && has_value
      end

      # String containing spaces only repond `true` to `blank?` and therefore
      # deletes the spaces which we don't want, so we try to use `empty?`
      # instead when possible and fallback to `blank?` otherwise.
      def empty_value?
        (context.value.respond_to?(:empty?) && context.value.empty?) ||
          context.value.blank?
      end

      #
      # Return an Array with the prefered reference language code, based on the
      # given locale country code, if any, and then the other project
      # reference languages.
      #
      def ordered_reference_languages_for(locale)
        # Selects the reference language used to fill in the empty string value
        prefered_reference_language = select_reference_language_for(locale)

        [
          prefered_reference_language,
          *context.project.reference_languages - [prefered_reference_language]
        ]
      end

      def reference_language_from_country_code_or_sorted_first(locale)
        # When the ProjectString has a locale like `en_BE`, we use the country
        # code to select a reference language
        candidates = context.project.reference_languages
                            .grep(/_#{locale.split('_').last}$/)

        return candidates.min if candidates.present?

        context.project.reference_languages.min
      end

      def select_reference_language_for(locale)
        # When we can't rely on the country code, we take the
        # first sorted reference language
        unless locale.include?('_')
          return context.project.reference_languages.min
        end

        reference_language_from_country_code_or_sorted_first(locale)
      end

      def string_value_is_present?
        return if empty_value?

        context.value = context.value

        true
      end

      def update_context_value_with_autocompleted
        context.value = context.cache[@detected_reference_language][
          context.relative_key
        ]
      end
    end
  end
end
