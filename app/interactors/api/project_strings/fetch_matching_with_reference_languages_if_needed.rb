# frozen_string_literal: true

module Api
  module ProjectStrings
    class FetchMatchingWithReferenceLanguagesIfNeeded
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale).filled(:string)
        required(:project).filled
        required(:project_strings)
      end

      promises do
        required(:reference_languages_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.reference_languages_strings = begin
          context.project.project_strings.where(
            :locale.in => context.project.reference_languages,
            :relative_key.in => context.project_strings.collect(&:relative_key)
          )
        end
      end
    end
  end
end
