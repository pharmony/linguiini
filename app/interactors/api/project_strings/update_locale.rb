# frozen_string_literal: true

module Api
  module ProjectStrings
    class UpdateLocale
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale).filled
        required(:ids).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        project_string = ProjectString.where(:id.in => context.ids)

        # rubocop:disable Rails/SkipsModelValidations
        return if project_string.update_all(locale: context.locale.code)
        # rubocop:enable Rails/SkipsModelValidations

        context.fail!(errors: project_string.errors)
      end
    end
  end
end
