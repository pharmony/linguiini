# frozen_string_literal: true

module Api
  module ProjectUnusedStrings
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:check_only).filled(:bool)
        optional(:page)
        required(:project).filled
      end

      promises do
        required(:unused_strings)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.unused_strings = if context.check_only
                                   check_if_there_any_unused_strings?
                                 else
                                   retrieve_unused_strings
                                 end
      end

      private

      def build_query_criteria
        context.project.project_strings.where(unused: true).order_by(:key)
      end

      def check_if_there_any_unused_strings?
        count = build_query_criteria.count

        {
          count: count,
          has_any: count.positive?
        }
      end

      def retrieve_unused_strings
        criteria = build_query_criteria

        result = criteria.page(context.page || 1)
        result = criteria.page(result.total_pages) if result.to_a.empty?
        result
      end
    end
  end
end
