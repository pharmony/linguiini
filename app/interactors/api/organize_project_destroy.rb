# frozen_string_literal: true

module Api
  class OrganizeProjectDestroy
    include Interactor::Organizer

    organize Projects::Find,
             Projects::Destroy,
             Projects::BroadcastDestroyedProjectToTheUi
  end
end
