# frozen_string_literal: true

module Api
  class OrganizeApplyingProjectStringsAlert
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStringsAlerts::Apply,
             Projects::EnqueueStringsStatsUpdate
  end
end
