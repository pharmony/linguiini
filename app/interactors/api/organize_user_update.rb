# frozen_string_literal: true

module Api
  class OrganizeUserUpdate
    include Interactor::Organizer

    organize Users::Find,
             Users::Update
  end
end
