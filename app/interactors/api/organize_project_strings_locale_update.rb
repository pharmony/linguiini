# frozen_string_literal: true

module Api
  #
  # Updates ProjectString from given ID list with the given locale
  #
  class OrganizeProjectStringsLocaleUpdate
    include Interactor::Organizer

    organize Locales::Find,
             Projects::Find,
             ProjectStrings::UpdateLocale,
             Projects::UpdateStringsStats
  end
end
