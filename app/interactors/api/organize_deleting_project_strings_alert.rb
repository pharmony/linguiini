# frozen_string_literal: true

module Api
  class OrganizeDeletingProjectStringsAlert
    include Interactor::Organizer

    organize Projects::Find,
             ProjectStringsAlerts::Reject,
             Projects::EnqueueStringsStatsUpdate
  end
end
