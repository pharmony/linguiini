# frozen_string_literal: true

module Api
  module Git
    class CommitAndPushExportFiles
      include Interactor
      include Interactor::Contracts

      expects do
        required(:git).filled
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        update_project_export_state_to_pushing!

        context.git.add(context.project.local_path)
        context.git.commit('Updates translation files')
        context.git.push(
          Git::GIT_REMOTE_NAME,
          branch_name,
          force: must_force?
        )
      end

      private

      def branch_name
        Rails.configuration.x.linguiini.branch_name
      end

      def must_force?
        # When linguiini/export exist, push force, otherwise push "normally"
        context.git.branches[branch_name] || false
      end

      def update_project_export_state_to_pushing!
        context.project_export.push_files!

        ProjectExportsChannel.broadcast_to(
          context.project_export.id,
          ActiveModelSerializers::SerializableResource.new(
            context.project_export
          )
        )
      end
    end
  end
end
