# frozen_string_literal: true

module Api
  module Git
    class InitializeGit
      include Interactor
      include Interactor::Contracts

      expects do
        required(:git_working_directory).filled(:string)
        required(:path_with_namespace).filled(:string)
        required(:project).filled
      end

      promises do
        required(:git).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.git = ::Git.open(context.project.local_path, log: Rails.logger)

        configure_git_user_name_and_email
      rescue StandardError => error
        context.fail!(errors: error.message)
      end

      private

      def configure_git_user_name_and_email
        gitlab_user = ::Gitlab.user

        context.git.config('user.name', gitlab_user.name)
        context.git.config('user.email', gitlab_user.email)
      end
    end
  end
end
