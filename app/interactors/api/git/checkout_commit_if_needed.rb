# frozen_string_literal: true

module Api
  module Git
    class CheckoutCommitIfNeeded
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:git_options)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if context.git_options['commit_sha'].blank?

        checkout_commit_sha
      rescue StandardError => error
        context.fail!(errors: error.message)
      end

      private

      def commit_sha
        context.git_options['commit_sha']
      end

      def checkout_commit_sha
        Rails.logger.debug do
          "[#{self.class}] Checking out commit SHA #{commit_sha} in the " \
            "repository at #{context.repository_path} ..."
        end

        git = ::Git.open(context.repository_path, log: Rails.logger)
        # git fetch origin
        git.remote(Git::GIT_REMOTE_NAME).fetch
        # git checkout abcd123
        git.checkout(commit_sha)
      end
    end
  end
end
