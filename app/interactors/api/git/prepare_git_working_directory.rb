# frozen_string_literal: true

module Api
  module Git
    class PrepareGitWorkingDirectory
      include Interactor
      include Interactor::Contracts

      promises do
        required(:git_working_directory)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.git_working_directory = File.join(base_dir, 'git')

        FileUtils.mkdir_p(context.git_working_directory)
      end

      private

      def base_dir
        Rails.env.test? ? '/tmp' : ENV['HOME']
      end
    end
  end
end
