# frozen_string_literal: true

module Api
  module Git
    class CloneOrPullProjectRepository
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:git_options)
        required(:git_working_directory).filled(:string)
        required(:integration_ssh_private_key_path).filled(:string)
        required(:path_with_namespace).filled(:string)
        required(:project).filled
        required(:ssh_url_to_repo).filled(:string)
      end

      promises do
        required(:repository_path).filled(:string)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        build_ssh_command

        build_repository_path

        clone_repo unless File.directory?(context.repository_path)

        reset_repo

        update_project_local_path_if_needed!
      rescue StandardError => error
        context.fail!(errors: error.message)
      end

      private

      def build_repository_path
        context.repository_path = File.join(context.git_working_directory,
                                            context.path_with_namespace)
      end

      def build_ssh_command
        ssh_known_hosts_path = File.join(ENV.fetch('HOME'), '.ssh',
                                         'known_hosts')

        ENV['GIT_SSH_COMMAND'] = [
          'ssh',
          "-o IdentityFile=\"#{context.integration_ssh_private_key_path}\"",
          "-o UserKnownHostsFile=#{ssh_known_hosts_path}",
          '-o StrictHostKeyChecking=no'
        ].join(' ')
      end

      def checkout_git_branch(git)
        Rails.logger.info "[#{self.class}] Checking out the branch " \
                          "#{git_branch} ..."

        git.branch(git_branch).checkout
      end

      def clone_repo
        Rails.logger.debug { "[#{self.class}] Cloning repository ..." }

        #
        # We are not selecting a branch on the clone in order to get the default
        # one and configuring git refs to all branches.
        #
        # If a different branch than the default one has been selected to import
        # the project, it will be checked out from the `reset_repo` method.
        #
        #
        # Also we can't use the depth option here since it makes git limiting
        # repo's refs to the cloned branch, and the git gem doesn't support the
        # `git remote set-branches` command.
        # See https://github.com/ruby-git/ruby-git/issues/664
        #
        ::Git.clone(
          context.ssh_url_to_repo,
          context.path_with_namespace,
          log: Rails.logger,
          path: context.git_working_directory
        )
      end

      def git_branch
        # Selected branch from GitOptions component
        @git_branch ||= context.git_options['branch_name'] ||
                        # Selected branch when creating new project
                        context.project.repository_branch_name ||
                        # Otherwise the default branch from the remote git
                        # server
                        project_default_branch_name
      end

      def open_and_fetch_repository
        git = ::Git.open(context.repository_path, log: Rails.logger)

        # git fetch origin
        git.remote(Git::GIT_REMOTE_NAME).fetch

        git
      end

      def project_default_branch_name
        @project_default_branch_name ||= ::Gitlab.project(
          context.project.repository_id
        ).default_branch
      end

      def reset_repo
        Rails.logger.debug do
          "[#{self.class}] Reseting repository at #{context.repository_path} " \
            "from #{Git::GIT_REMOTE_NAME}/#{git_branch} ..."
        end

        git = open_and_fetch_repository

        checkout_git_branch(git) if git.current_branch != git_branch

        # git reset --hard origin/develop
        git.reset_hard("#{Git::GIT_REMOTE_NAME}/#{git_branch}")
      end

      def update_project_local_path_if_needed!
        return true if context.project.local_path

        context.project.update(local_path: context.repository_path)
      end
    end
  end
end
