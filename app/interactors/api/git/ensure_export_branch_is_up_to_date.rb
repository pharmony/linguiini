# frozen_string_literal: true

module Api
  module Git
    class EnsureExportBranchIsUpToDate
      include Interactor
      include Interactor::Contracts

      expects do
        required(:git).filled
        required(:integration_ssh_private_key_path).filled(:string)
        required(:project).filled
        required(:project_export).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        build_ssh_command

        ensure_branch_name_is_defined!

        update_project_export_state_to_branch_cleaning!

        # Create or switch to the export branch
        checkout_branch

        reset_hard_from_origin if linguiini_export_branch_exists?
      end

      private

      def branch_name
        Rails.configuration.x.linguiini.branch_name
      end

      def build_ssh_command
        ssh_known_hosts_path = File.join(ENV.fetch('HOME'), '.ssh',
                                         'known_hosts')

        ENV['GIT_SSH_COMMAND'] = [
          'ssh',
          "-o IdentityFile=\"#{context.integration_ssh_private_key_path}\"",
          "-o UserKnownHostsFile=#{ssh_known_hosts_path}",
          '-o StrictHostKeyChecking=no'
        ].join(' ')
      end

      def checkout_branch
        context.git.branch(branch_name).checkout
      end

      def ensure_branch_name_is_defined!
        return if branch_name.length.positive?

        context.fail!(errors: I18n.t('errors.branch_name_undefined'))
      end

      def linguiini_export_branch_exists?
        context.git.branches[branch_name] ||
          context.git.branches["remotes/#{origin_branch_name}"]
      end

      def origin_branch_name
        "#{Git::GIT_REMOTE_NAME}/#{branch_name}"
      end

      def project_default_branch_name
        @project_default_branch_name ||= ::Gitlab.project(
          context.project.repository_id
        ).default_branch
      end

      def reset_hard_from_origin
        # git fetch origin
        context.git.remote(Git::GIT_REMOTE_NAME).fetch
        # git reset --hard develop
        context.git.reset_hard(
          "#{Git::GIT_REMOTE_NAME}/#{project_default_branch_name}"
        )
      end

      def update_project_export_state_to_branch_cleaning!
        context.project_export.branch_clean!

        ProjectExportsChannel.broadcast_to(
          context.project_export.id,
          ActiveModelSerializers::SerializableResource.new(
            context.project_export
          )
        )
      end
    end
  end
end
