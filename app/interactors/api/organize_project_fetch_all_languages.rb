# frozen_string_literal: true

module Api
  class OrganizeProjectFetchAllLanguages
    include Interactor::Organizer

    organize Projects::Find,
             ProjectLanguages::FetchAll
  end
end
