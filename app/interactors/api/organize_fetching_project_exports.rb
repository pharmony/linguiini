# frozen_string_literal: true

module Api
  class OrganizeFetchingProjectExports
    include Interactor::Organizer

    organize Projects::Find,
             ProjectExports::FetchAll
  end
end
