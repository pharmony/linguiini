# frozen_string_literal: true

module Api
  class OrganizeQueuingProjectDestroy
    include Interactor::Organizer

    organize Projects::Find,
             Projects::MarkAsToBeDeleted,
             Projects::EnqueueDestroy
  end
end
