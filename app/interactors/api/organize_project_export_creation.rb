# frozen_string_literal: true

module Api
  class OrganizeProjectExportCreation
    include Interactor::Organizer

    organize Projects::Find,
             ProjectExports::Create,
             ProjectExports::Enqueue
  end
end
