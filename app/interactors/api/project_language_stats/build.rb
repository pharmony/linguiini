# frozen_string_literal: true

module Api
  module ProjectLanguageStats
    class Build
      include Interactor
      include Interactor::Contracts

      expects do
        required(:locale).filled
        required(:project).filled
        required(:stats_cache)
      end

      promises do
        required(:stats)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.stats = if context.stats_cache.present?
                          build_stats_from_cache
                        else
                          build_stats_from_db
                        end
      end

      private

      def build_stats_from_cache
        {
          translateds: context.stats_cache[:translateds],
          translations: context.stats_cache[:translations]
        }
      end

      def build_stats_from_db
        {
          translateds: translated_strings_count_for_locale,
          translations: highest_strings_count_from_reference_languages
        }
      end

      #
      # This method counts the number of ProjectString documents per locales and
      # returns the highest count.
      def highest_strings_count_from_reference_languages
        NoBrainer.run do |r|
          r.table(ProjectString.table_name).get_all(
            context.project.id,
            { index: :project_id }
          ).group('locale').count
        end.values.max
      end

      def translated_strings_count_for_locale
        ProjectString.where(
          locale: context.locale.code,
          project: context.project,
          # RethinkDB can't index null values as of writing this comment
          # therefore we have to use something else than `nil` so we're going
          # with an empty string.
          # See https://github.com/rethinkdb/rethinkdb/issues/1032
          :value.not => ''
        ).count
      end
    end
  end
end
