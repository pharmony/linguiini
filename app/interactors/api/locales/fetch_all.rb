# frozen_string_literal: true

module Api
  module Locales
    class FetchAll
      include Interactor
      include Interactor::Contracts

      promises do
        required(:locales).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.locales = Locale.all.to_a
      end
    end
  end
end
