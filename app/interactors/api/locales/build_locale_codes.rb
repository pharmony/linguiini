# frozen_string_literal: true

module Api
  module Locales
    #
    # locale_codes is used from a RegEx in order to detect the file's locale
    # from its filename or dirname.
    # See Api::StringsImports::DetectTranslationFilesLocales
    #
    class BuildLocaleCodes
      include Interactor
      include Interactor::Contracts

      promises do
        required(:locale_codes).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.locale_codes = "(#{Locale.all.map(&:code).join('|')})"
      end
    end
  end
end
