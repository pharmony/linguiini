# frozen_string_literal: true

module Api
  module Locales
    class CheckCodeAvailablity
      include Interactor
      include Interactor::Contracts

      expects do
        required(:code).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        query = Locale.where(code: context.code)

        return unless query.first

        context.fail!(errors: I18n.t('projects.errors.code_is_unavailable'))
      end
    end
  end
end
