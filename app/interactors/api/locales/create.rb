# frozen_string_literal: true

module Api
  module Locales
    class Create
      include Interactor
      include Interactor::Contracts

      expects do
        required(:name).filled(:string)
        required(:code).filled(:string)
      end

      promises do
        required(:locale).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.locale = Locale.new(new_params)

        return if context.locale.save

        context.fail!(errors: context.locale.errors)
      end

      private

      def new_params
        {
          code: context.code,
          english_name: context.name
        }
      end
    end
  end
end
