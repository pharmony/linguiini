# frozen_string_literal: true

module Api
  module Locales
    class Destroy
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        locale = Locale.where(id: context.id).first

        unless locale
          context.fail!(
            errors: I18n.t('errors.resource_not_found',
                           resource_name: I18n.t('resources.locale'))
          )
        end

        locale.destroy
      end
    end
  end
end
