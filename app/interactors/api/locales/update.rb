# frozen_string_literal: true

module Api
  module Locales
    class Update
      include Interactor
      include Interactor::Contracts

      expects do
        required(:code).filled(:string)
        required(:locale).filled
        required(:name).filled(:string)
      end

      promises do
        required(:locale).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if context.locale.update(code: context.code,
                                        english_name: context.name)

        context.fail!(errors: context.locale.errors)
      end
    end
  end
end
