# frozen_string_literal: true

module Api
  module Locales
    class Find
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:id) { none? | str? }
        optional(:locale_code) { none? | str? }
      end

      promises do
        required(:locale).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.locale = if context.locale_code
                           Locale.where(code: context.locale_code).first
                         else
                           Locale.where(id: context.id).first
                         end
      end
    end
  end
end
