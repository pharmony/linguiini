# frozen_string_literal: true

module Api
  module ProjectStringsAlerts
    class Reject
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled(:string)
        required(:project).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        string = context.project.project_strings.where(id: context.id).first

        return if string.update(conflict: false, new_value: '')

        context.fail!(errors: string.errors)
      end
    end
  end
end
