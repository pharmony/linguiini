# frozen_string_literal: true

module Api
  module ProjectStringsAlerts
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        required(:check_only).filled(:bool)
        optional(:filter)
        optional(:page)
        required(:project).filled
      end

      promises do
        required(:strings_alerts)
        optional(:total_count)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.strings_alerts = if context.check_only
                                   check_if_there_any_alerts?
                                 else
                                   retrieve_alerts
                                 end

        context.total_count = alerts_total_count unless context.check_only
      end

      private

      def alerts_total_count
        alerts = 0
        alerts += context.project.strings_stats[:conflicts]
        alerts += context.project.strings_stats[:duplicates]
        alerts += context.project.strings_stats[:missing_locale_strings]
        alerts
      end

      def build_query_criteria
        # RethinkDB can't index null values as of writing this comment
        # therefore we have to use something else than `nil` so we're going
        # with an empty string.
        # See https://github.com/rethinkdb/rethinkdb/issues/1032
        criteria = context.project.project_strings
        criteria = criteria.where(with_issue: true)

        if filter_present?
          # We're using `without_index` because we weren't able to
          # create the right index making NoBrainer able to return
          # the expected values.
          criteria = criteria.where(
            criteria_column => /^.*#{criteria_value}.*$/
          ).without_index
        end

        criteria.order_by(:key, :path)
      end

      def check_if_there_any_alerts?
        alerts = alerts_total_count

        {
          count: alerts,
          has_any: alerts.positive?
        }
      end

      def criteria_column
        :"#{context.filter['column']}".to_sym
      end

      def criteria_value
        Regexp.escape(context.filter['value'])
      end

      def filter_present?
        return false if context.filter.blank?
        return false unless context.filter.keys.sort == %w[column value]

        context.filter['value'].present?
      end

      def retrieve_alerts
        criteria = build_query_criteria

        result = criteria.page(context.page || 1)
        result = criteria.page(result.total_pages) if result.to_a.empty?
        result
      end
    end
  end
end
