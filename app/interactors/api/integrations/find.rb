# frozen_string_literal: true

module Api
  module Integrations
    class Find
      include Interactor
      include Interactor::Contracts

      expects do
        optional(:id) { none? | str? }
        optional(:platform) { none? | str? }
      end

      promises do
        required(:integration).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        ensure_id_or_platform_is_given

        context.integration = Integration.find(context.id) if context.id
        context.integration = find_by_platform if context.platform
      end

      private

      def ensure_id_or_platform_is_given
        return if context.id || context.platform

        context.fail!(errors: 'id or platform required')
      end

      def find_by_platform
        "#{context.platform.classify}Integration".constantize.first
      end
    end
  end
end
