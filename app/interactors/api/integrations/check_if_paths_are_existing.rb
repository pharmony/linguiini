# frozen_string_literal: true

module Api
  module Integrations
    class CheckIfPathsAreExisting
      include Interactor
      include Interactor::Contracts

      expects do
        required(:platform).filled(:string)
        required(:integration).filled
        required(:repository_id).filled(:integer)
        required(:paths).filled(:string)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.paths.split(',').each do |basedir|
          tree = fetch_repository_tree_from(basedir)

          next if tree_has_file?(tree)

          context.fail!(errors: I18n.t(
            'integrations.no_supported_translation_file_found_in_tree'
          ))
        end
      end

      private

      def build_platform_klass_name
        "Api::Integrations::#{context.platform.classify}::FetchTreeFromPath"
      end

      def ensure_tree_has_files!(tree)
        return if tree.present?

        context.fail!(
          errors: I18n.t('integrations.no_translation_file_found_in_tree')
        )
      end

      def tree_has_file?(tree)
        ensure_tree_has_files!(tree)

        return true if tree.detect { |file| file['name'] =~ /\.(ya?ml|json)$/ }

        tree.each do |file|
          # Skip files, only go through folders
          #
          # Can't trust Ruby's File.directory?/File.file? methods so the only
          # reliable way is to check if the currrent path + '/*' detects files
          # or not.
          # File.directory?/File.file? methods are failing with `.keep` files
          # for example, while Dir.glob(`my/path/.keep/*`) tells use that path
          # has no sub files.
          next if Dir.glob(File.join(file['path'], '/*')).empty?

          sub_tree = fetch_repository_tree_from(file['path'])
          tree_has_file?(sub_tree)
        end
      end

      def fetch_repository_tree_from(path)
        interactor = build_platform_klass_name.constantize.call(
          id: context.repository_id,
          integration: context.integration,
          path: path.squish!
        )

        return interactor.tree if interactor.success?

        context.fail!(errors: interactor.errors)
      end
    end
  end
end
