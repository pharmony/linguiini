# frozen_string_literal: true

module Api
  module Integrations
    class Enqueue
      include Interactor
      include Interactor::Contracts

      expects do
        required(:integration).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        Gitlab::RepositoriesFetchingJob.perform_later(context.integration.id)
      end
    end
  end
end
