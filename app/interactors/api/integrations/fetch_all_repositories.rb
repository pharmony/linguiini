# frozen_string_literal: true

module Api
  module Integrations
    class FetchAllRepositories
      include Interactor
      include Interactor::Contracts

      expects do
        required(:platform).filled
        required(:integration).filled
      end

      promises do
        required(:repositories)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.repositories = fetch_all_repositories_from_platform
      end

      private

      def build_platform_klass_name
        "Api::Integrations::#{context.platform.classify}::FetchAll"
      end

      def fetch_all_repositories_from_platform
        interactor = build_platform_klass_name.constantize.call(
          integration: context.integration
        )

        return interactor.repositories if interactor.success?

        context.fail!(errors: interactor.errors)
      end
    end
  end
end
