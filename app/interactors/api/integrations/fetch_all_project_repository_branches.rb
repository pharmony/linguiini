# frozen_string_literal: true

module Api
  module Integrations
    class FetchAllProjectRepositoryBranches
      include Interactor
      include Interactor::Contracts

      expects do
        required(:integration).filled
        required(:platform).filled
        required(:project_id).filled
      end

      promises do
        required(:branches)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.branches = fetch_all_branches_from_platform
      end

      private

      def build_platform_klass_name
        "Api::Integrations::#{context.platform.classify}::FetchBranches"
      end

      def fetch_all_branches_from_platform
        interactor = build_platform_klass_name.constantize.call(
          integration: context.integration,
          project_id: context.project_id
        )

        return interactor.branches if interactor.success?

        context.fail!(errors: interactor.errors)
      end
    end
  end
end
