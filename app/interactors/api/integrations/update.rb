# frozen_string_literal: true

module Api
  module Integrations
    class Update
      include Interactor
      include Interactor::Contracts

      expects do
        required(:integration).filled
        required(:token).filled
      end

      promises do
        required(:integration).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.integration.token = context.token

        return context.integration if context.integration.save

        context.fail!(errors: context.integration.errors)
      end
    end
  end
end
