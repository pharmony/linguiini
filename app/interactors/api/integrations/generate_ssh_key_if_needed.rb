# frozen_string_literal: true

module Api
  module Integrations
    class GenerateSshKeyIfNeeded
      include Interactor
      include Interactor::Contracts

      expects do
        required(:integration).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        return if context.integration.ssh_key_generated?

        generate_integration_ssh_key!
      end

      private

      def generate_integration_ssh_key!
        key = SSHKey.generate

        context.integration.update(
          ssh_key_fingerprint_md5: key.md5_fingerprint,
          ssh_key_fingerprint_sha256: key.sha256_fingerprint,
          ssh_private_key: key.private_key,
          ssh_public_key: key.ssh_public_key
        )
      end
    end
  end
end
