# frozen_string_literal: true

module Api
  module Integrations
    class FetchAll
      include Interactor
      include Interactor::Contracts

      promises do
        required(:integrations)
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.integrations = Integration.all.to_a
      end
    end
  end
end
