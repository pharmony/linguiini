# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class FetchBranches
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        expects do
          required(:integration).filled
          required(:project_id).filled
        end

        promises do
          required(:branches)
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          configure_gitlab_with_token(context.integration.token)

          context.branches = build_branches_list_for_select
        end

        private

        def build_branches_list_for_select
          branches.map do |branch|
            {
              default: branch.default,
              label: branch.name,
              protected: branch.protected,
              value: branch.commit.short_id
            }
          end
        end

        def branches
          ::Gitlab.branches(
            context.project_id
          ).auto_paginate # retrieve all projects as an array
        end
      end
    end
  end
end
