# frozen_string_literal: true

# require 'interactors/api/concerns/gitlab_configurator'

module Api
  module Integrations
    module Gitlab
      class PushSshKeyOnProject
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        expects do
          required(:integration).filled
          required(:project).filled
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          configure_gitlab_with_token(context.integration.token)

          if project_has_integration_key_already?
            Rails.logger.debug do
              "[#{self.class}] Deploy key already existing, skipping."
            end

            return
          end

          push_ssh_key_to_project_deploy_keys!
        end

        private

        def project_deploy_keys
          Rails.logger.debug do
            "[#{self.class}] Retrieving project's deploy keys ..."
          end

          #
          # Even do the documentation doesn't say anything about it, it looks
          # like the API returns the ENABLED project deploy keys.
          #
          ::Gitlab.deploy_keys(context.project.repository_id)
        end

        def project_has_integration_key_already?
          fingerprint_sha256 = context.integration.ssh_key_fingerprint_sha256

          project_deploy_keys.detect do |deploy_key|
            deploy_key.fingerprint_sha256 == fingerprint_sha256
          end
        end

        def push_ssh_key_to_project_deploy_keys!
          Rails.logger.debug { "[#{self.class}] Pushing deploy key ..." }

          #
          # This API will create or re-enable a disabled existing key.
          # Otherwise it raises an error if a key with the same SHA256
          # fingerprint already exist and is enabled.
          #
          ::Gitlab.create_deploy_key(
            context.project.repository_id,
            'Linguiini',
            context.integration.ssh_public_key,
            can_push: true
          )
        end
      end
    end
  end
end
