# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class FetchAll
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        expects do
          required(:integration).filled
        end

        promises do
          required(:repositories)
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          configure_gitlab_with_token(context.integration.token)

          context.repositories = build_repositories_list_for_select
        end

        private

        def build_repositories_list_for_select
          repositories.map do |repository|
            {
              label: repository.name_with_namespace,
              value: repository.id
            }
          end
        end

        def repositories
          ::Gitlab.projects(
            {
              # Projects are filtered on membershiped projects,
              # but can be "archived", "owner", and "starred".
              #
              # https://docs.gitlab.com/ee/api/projects.html#list-all-projects
              membership: true
            }
          ).auto_paginate # retrieve all projects as an array
        end
      end
    end
  end
end
