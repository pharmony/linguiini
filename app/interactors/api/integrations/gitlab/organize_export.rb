# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class OrganizeExport
        include Interactor::Organizer

        organize Api::Ssh::WritePrivateKeyToTheDisk,
                 Api::Integrations::Gitlab::PushSshKeyOnProject,
                 Api::Integrations::Gitlab::RetrieveRepositoryMetadata,
                 Api::Integrations::Gitlab::PrepareContextForGitClone,
                 Api::Git::PrepareGitWorkingDirectory,
                 Api::Git::InitializeGit,
                 Api::Git::EnsureExportBranchIsUpToDate,
                 Api::Integrations::Gitlab::EnsureMergeRequestExist,
                 Api::Locales::BuildLocaleCodes,
                 Api::ProjectExports::BuildTranslationsFile,
                 Api::ProjectExports::WriteExportFiles,
                 Api::Git::CommitAndPushExportFiles,
                 Api::ProjectStrings::ClearUpdated
      end
    end
  end
end
