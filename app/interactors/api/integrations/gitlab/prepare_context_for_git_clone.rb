# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class PrepareContextForGitClone
        include Interactor
        include Interactor::Contracts

        expects do
          required(:repository_metadata).filled
        end

        promises do
          required(:path_with_namespace).filled(:string)
          required(:ssh_url_to_repo).filled(:string)
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          Rails.logger.debug do
            "[#{self.class}] Preparing repository metadata ..."
          end

          # Gitlab repo metadata
          context.path_with_namespace = context.repository_metadata
                                               .path_with_namespace

          context.ssh_url_to_repo = context.repository_metadata
                                           .ssh_url_to_repo
        end
      end
    end
  end
end
