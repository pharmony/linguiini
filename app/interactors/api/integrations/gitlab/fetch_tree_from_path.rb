# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class FetchTreeFromPath
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        expects do
          required(:id).filled(:integer)
          required(:integration).filled
          required(:path).filled(:string)
        end

        promises do
          required(:tree)
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          configure_gitlab_with_token(context.integration.token)

          context.tree = ::Gitlab.tree(context.id, path: context.path)
        rescue StandardError => error
          context.fail!(errors: error.message)
        end
      end
    end
  end
end
