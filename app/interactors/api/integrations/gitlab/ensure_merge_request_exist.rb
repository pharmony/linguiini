# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class EnsureMergeRequestExist
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        promises do
          required(:project).filled
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          @branch = ::Gitlab.project(context.project.repository_id)
                            .default_branch

          return if linguiini_merge_request_exists?

          create_merge_request
        end

        private

        def branch_name
          Rails.configuration.x.linguiini.branch_name
        end

        def create_merge_request
          ::Gitlab.create_merge_request(
            context.project.repository_id,
            branch_name,
            {
              source_branch: branch_name,
              target_branch: @branch
            }
          )
        end

        def linguiini_merge_request_exists?
          ::Gitlab.merge_requests(
            context.project.repository_id,
            {
              source_branch: branch_name,
              state: 'opened',
              target_branch: @branch
            }
          ).size == 1
        end
      end
    end
  end
end
