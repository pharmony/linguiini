# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class RetrieveRepositoryMetadata
        include Interactor
        include Interactor::Contracts
        include Api::Concerns::GitlabConfigurator

        expects do
          required(:integration).filled
          required(:project).filled
        end

        promises do
          required(:repository_metadata)
        end

        on_breach { |breaches| context.fail!(errors: breaches.to_h) }

        def call
          configure_gitlab_with_token(context.integration.token)

          Rails.logger.debug do
            "[#{self.class}] Retrieving repository metadata ..."
          end

          context.repository_metadata = ::Gitlab.project(
            context.project.repository_id
          )
        rescue StandardError => error
          context.fail!(errors: error.message)
        end
      end
    end
  end
end
