# frozen_string_literal: true

module Api
  module Integrations
    class Create
      include Interactor
      include Interactor::Contracts

      expects do
        required(:params).schema do
          required(:platform).filled(:string)
          required(:token).filled(:string)
        end
      end

      promises do
        required(:integration).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.integration = build_integration_klass.new(
          token: context.params['token']
        )

        return if context.integration.save

        context.fail!(errors: context.integration.errors)
      end

      private

      def build_integration_klass
        "#{context.params['platform']}_integration".classify.constantize
      rescue NameError => error
        Rails.logger.error "[Api::Integrations::Create] #{error.message}"
        Rails.logger.error error.backtrace.join("\n")
        context.fail!(errors: 'Invalid platform')
      end
    end
  end
end
