# frozen_string_literal: true

module Api
  module Translations
    class FetchAll
      include Interactor
      include Interactor::Contracts

      expects do
        required(:id).filled
        required(:language).filled
      end

      promises do
        required(:translations).filled
      end

      on_breach { |breaches| context.fail!(errors: breaches.to_h) }

      def call
        context.translations = ProjectString.where(
          project_id: context.id,
          locale: context.language
        )
      end
    end
  end
end
