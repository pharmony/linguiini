# frozen_string_literal: true

#
# Allows the controller including this concern to accept Basic Auth method.
#
module BasicAuthMethod
  extend ActiveSupport::Concern

  included do
    skip_before_action :authenticate_user!, if: :basic_authorisation_request?
    before_action :basic_auth, if: :basic_authorisation_request?
  end

  def basic_authenticate_request?
    env_credentials = Rails.application.secrets
    api_basic_auth = env_credentials[:api_basic_auth]
    authenticate_with_http_basic do |username, password|
      username == api_basic_auth[:username] &&
        password == api_basic_auth[:password]
    end
  end

  def basic_authorisation_request?
    return false unless request.headers['Authorization']

    request.headers['Authorization'].start_with?('Basic ')
  end

  def basic_auth
    return if basic_authenticate_request?

    render json: { error: 'Authentication missing' }, status: :unauthorized
  end
end
