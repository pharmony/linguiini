# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include ActionController::Cookies

  protect_from_forgery with: (
    Rails.env.production? ? :null_session : :exception
  )

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!
  before_action :set_i18n_locale!

  after_action :set_csrf_cookie

  # Render the error as a JSON when request has been made in JSON format
  # otherwise render in HTML.
  rescue_from Exception do |error|
    raise unless request.format == :json

    raise if Rails.env.test?

    Rails.logger.error "\n\n#{error.class.name} (#{error.message}):"
    Rails.logger.error error.backtrace.join("\n")

    render json: {
      error: { message: error.message, backtrace: error.backtrace }
    }, status: :internal_server_error
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:invite, keys: %i[role])
  end

  private

  def log422(errors)
    Rails.logger.warn "[render_with] rendering 422: #{errors.inspect}"
  end

  def render_json(interactor, key, options = {})
    # 200 or 201?
    success_response = action_name == 'create' ? :created : :ok

    json = (key && interactor.send(key))
    json = {} if json.nil?

    # When passing the `:root` option to `render_with`, the interactor/organizer
    # data is embedded within a Hash having the given `:root` name as root node.
    root = options.delete(:root) || nil

    json = { root => json } if root

    render json: json, **options.merge(status: success_response)
  end

  def render_with(interactor, key: nil, **options)
    if interactor.success?
      render_json(interactor, key, options.dup)
    elsif key && interactor.send(key).nil?
      render json: {}, status: :not_found
    else
      log422(interactor.errors) if %w[development test].include?(Rails.env)

      render json: { errors: interactor.errors },
             status: :unprocessable_entity
    end
  end

  #
  # Since the CSRF token is not updated from the page <head> as we're using an
  # SPA, we need to pass the new CSRF token to the client, so that it has a
  # fresh one.
  #
  def set_csrf_cookie
    cookies['CSRF-TOKEN'] = {
      domain: ENV.fetch('RAILS_ENV_BASE_DOMAIN'),
      httponly: false,
      same_site: :strict,
      secure: true,
      value: form_authenticity_token
    }
  end

  def set_i18n_locale!
    return unless current_user

    I18n.locale = current_user.locale
  end
end
