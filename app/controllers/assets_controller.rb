# frozen_string_literal: true

class AssetsController < ApplicationController
  skip_before_action :authenticate_user!
end
