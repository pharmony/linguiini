# frozen_string_literal: true

module Api
  class TranslationsController < ApplicationController
    def index
      organizer = OrganizeTranslationsAndReference.call(index_params)

      if organizer.success?
        render json: { translations: organizer.translations,
                       reference: organizer.reference }, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    private

    def index_params
      {
        id: params[:project_id],
        language: params[:project_language_id]
      }
    end
  end
end
