# frozen_string_literal: true

module Api
  class RepositoriesController < ApplicationController
    def index
      organizer = OrganizeFetchingRepositories.call(
        platform: params[:platform]
      )

      if organizer.success?
        render json: organizer.repositories, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end
  end
end
