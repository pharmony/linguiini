# frozen_string_literal: true

module Api
  class ProjectsController < ApplicationController
    def check
      interactor = Projects::CheckNameAvailablity.call(
        exclude_project_id: params[:project_id],
        name: params[:name]
      )

      render_with(interactor)
    end

    def create
      organizer = OrganizeProjectCreation.call(create_params)

      render_with(organizer, key: :project)
    end

    def fixes_alert_project
      organizer = OrganizeProjectStringsLocaleUpdate.call(
        id: params[:id], ids: params[:project][:ids],
        locale_code: params[:project][:locale]
      )

      render_with(organizer, key: :project)
    end

    def force_import
      organizer = OrganizeProjectForceImport.call(force_import_params)

      render_with(organizer, key: :project)
    end

    def destroy
      organizer = OrganizeQueuingProjectDestroy.call(id: params[:id])

      render_with(organizer, key: :project)
    end

    def index
      interactor = Projects::FetchAll.call

      render_with(interactor, key: :projects)
    end

    def refresh
      organizer = OrganizeProjectStatsUpdate.call(id: params[:id])

      render_with(organizer)
    end

    def show
      interactor = Projects::Fetch.call(id: params[:id])

      render_with(interactor, key: :project)
    end

    def update
      organizer = OrganizeProjectUpdate.call(
        id: params[:id],
        params: update_params
      )

      render_with(organizer, key: :project)
    end

    private

    def create_params
      params.require(:project).permit(
        :name,
        :platform,
        :paths,
        :repository_id,
        locales: []
      ).merge(
        git_options: params.require(:project).permit(:branch_name, :commit_sha)
      )
    end

    def force_import_params
      params.require(:project).permit(
        :all_files,
        :id,
        git_options: %i[
          commit_sha
          branch_name
        ]
      )
    end

    def update_params
      params.require(:project).permit(
        :name,
        :paths,
        locales: []
      )
    end
  end
end
