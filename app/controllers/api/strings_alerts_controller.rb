# frozen_string_literal: true

module Api
  class StringsAlertsController < ApplicationController
    def create
      organizer = OrganizeApplyingProjectStringsAlert.call(action_params)

      render_with(organizer)
    end

    def destroy
      organizer = OrganizeDeletingProjectStringsAlert.call(action_params)

      render_with(organizer)
    end

    def index
      organizer = OrganizeFetchingProjectStringsAlerts.call(index_params)

      if organizer.success?
        render json: build_index_response_from(organizer), status: :ok
      else
        render json: { errors: organizer.errors }, status: :unprocessable_entity
      end
    end

    def keep
      organizer = OrganizeKeepingProjectString.call(keep_params)

      if organizer.success?
        render json: build_keep_response_from(organizer), status: :ok
      else
        render json: { errors: organizer.errors }, status: :unprocessable_entity
      end
    end

    private

    def action_params
      params.permit(:id, :project_id).to_h
    end

    def build_index_response_from(organizer)
      return organizer.strings_alerts if params[:check] == 'true'

      {
        current_page: organizer.strings_alerts.current_page,
        data: ActiveModelSerializers::SerializableResource.new(
          organizer.strings_alerts,
          each_serializer: ProjectStringSerializer
        ),
        total_count: organizer.total_count,
        total_pages: organizer.strings_alerts.total_pages
      }
    end

    def build_keep_response_from(organizer)
      {
        kept_id: organizer.kept_id,
        destroyed_ids: organizer.destroyed_ids
      }
    end

    def index_params
      {
        check_only: params[:check] == 'true',
        filter: params[:filter],
        id: params[:project_id],
        page: params[:page]
      }
    end

    def keep_params
      {
        id: params[:strings_alert_id],
        project_id: params[:project_id]
      }
    end
  end
end
