# frozen_string_literal: true

module Api
  class LocalesController < ApplicationController
    def check
      interactor = Locales::CheckCodeAvailablity.call(code: params[:code])
      if interactor.success?
        render json: {}, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def create
      interactor = Locales::Create.call(params_create)

      if interactor.success?
        render json: interactor.locale, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def index
      interactor = Locales::FetchAll.call

      if interactor.success?
        render json: interactor.locales,
               each_serializer: LocaleSerializer,
               status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def destroy
      interactor = Locales::Destroy.call(id: params[:id])

      if interactor.success?
        render json: {}, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def update
      organizer = OrganizeLocaleUpdate.call(update_params)

      render_with(organizer, key: :locale)
    end

    private

    def params_create
      params.require(:locale).permit(:name, :code)
    end

    def update_params
      params.require(:locale).permit(:name, :code).merge({ id: params[:id] })
    end
  end
end
