# frozen_string_literal: true

module Api
  class IntegrationsController < ApplicationController
    def check_paths
      organizer = OrganizeCheckingPaths.call(check_params)

      if organizer.success?
        render json: organizer.integration, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def create
      organizer = OrganizeIntegrationCreation.call(params: create_params)

      if organizer.success?
        render json: organizer.integration, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def index
      interactor = Integrations::FetchAll.call

      if interactor.success?
        render json: interactor.integrations, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def update
      organizer = OrganizeIntegrationUpdate.call(update_params)
      if organizer.success?
        render json: organizer.integration, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    private

    def check_params
      {
        platform: params[:platform],
        paths: params[:paths],
        repository_id: params[:repository_id]
      }
    end

    def create_params
      params.require(:integration).permit(:token, :platform)
    end

    def update_params
      {
        id: params[:id],
        token: params[:integration][:token]
      }
    end
  end
end
