# frozen_string_literal: true

module Api
  class ProjectLanguagesController < ApplicationController
    def create
      organizer = OrganizeCreateProjectLanguage.call(
        id: params[:project_id],
        language_value: params[:value]
      )
      if organizer.success?
        render json: organizer.language, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def index
      organizer = OrganizeProjectFetchAllLanguages.call(id: params[:project_id])

      if organizer.success?
        render json: organizer.languages, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end
  end
end
