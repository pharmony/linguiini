# frozen_string_literal: true

module Api
  class StatsController < ApplicationController
    def show
      organizer = OrganizeFetchingProjectLanguageStats.call(
        id: params[:project_id],
        locale_code: params[:project_language_id]
      )

      if organizer.success?
        render json: organizer.stats, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end
  end
end
