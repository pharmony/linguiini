# frozen_string_literal: true

module Api
  class ProjectStringsController < ApplicationController
    def index
      organizer = OrganizeProjectStringsFetchAllReferences.call(index_params)

      if organizer.success?
        render json: build_index_response_from(organizer), status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def update
      interactor = OrganizeProjectStringsUpdate.call(update_params)

      render_with(interactor, key: :updated_strings)
    end

    private

    def build_index_response_from(organizer)
      {
        current_page: organizer.project_strings.current_page,
        project_strings: serialize_project_strings(organizer.project_strings),
        reference_languages_strings: serialize_project_strings(
          organizer.reference_languages_strings
        ),
        total_pages: organizer.project_strings.total_pages
      }
    end

    def index_params
      params.permit(:empty_only, :page, :project_id, :project_language_id,
                    :sort, filter: %i[column value]).tap do |hash|
        hash[:id] = hash.delete(:project_id)
        hash[:locale] = hash.delete(:project_language_id)
      end
    end

    def serialize_project_strings(project_strings)
      ActiveModelSerializers::SerializableResource.new(
        project_strings,
        each_serializer: ProjectStringSerializer
      )
    end

    def update_params
      # project_string is a hash of hashes with the ID of the ProjectString as
      # key, and the new ProjectString value as value.
      permitted = params.permit(:project_id, :project_language_id, hash: {})

      {
        action: 'modification',
        id: permitted[:project_id],
        translations: permitted[:hash].to_h,
        language: permitted[:project_language_id]
      }
    end
  end
end
