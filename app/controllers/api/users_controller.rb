# frozen_string_literal: true

module Api
  #
  # List users. Invitations are managed by the devise_invitable gem.
  #
  class UsersController < ApplicationController
    def index
      interactor = Users::FetchAll.call

      if interactor.success?
        render json: interactor.users, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def destroy
      interactor = Users::Destroy.call(id: params[:id])

      if interactor.success?
        render json: {}, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end

    def update
      organizer = OrganizeUserUpdate.call(
        id: params[:id],
        params: update_params
      )

      if organizer.success?
        render json: organizer.user, status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    private

    def update_params
      params.require(:user).permit(:role)
    end
  end
end
