# frozen_string_literal: true

module Api
  class RolesController < ApplicationController
    def index
      interactor = Roles::FetchAll.call

      if interactor.success?
        render json: interactor.roles, status: :ok
      else
        render json: { errors: interactor.errors },
               status: :internal_server_error
      end
    end
  end
end
