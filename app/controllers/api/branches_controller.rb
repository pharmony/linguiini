# frozen_string_literal: true

module Api
  class BranchesController < ApplicationController
    def index
      organizer = OrganizeFetchingBranches.call(
        platform: params[:platform],
        project_id: params[:project_id]
      )

      render_with(organizer, key: :branches)
    end
  end
end
