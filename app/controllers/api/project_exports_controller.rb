# frozen_string_literal: true

module Api
  # rubocop:disable Metrics/ClassLength
  class ProjectExportsController < ApplicationController
    def create
      organizer = OrganizeProjectExportCreation.call(create_params)

      if organizer.success?
        render json: organizer.project_export, status: :created
      else
        update_project_export_failure(organizer.errors)

        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def diff
      organizer = diff_organizer_klass.call(diff_params)

      if organizer.success?
        render json: build_response_from(organizer.project_strings),
               status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def index
      organizer = OrganizeFetchingProjectExports.call(index_params)

      if organizer.success?
        render json: build_response_from(organizer.project_exports),
               status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def new
      organizer = OrganizeFetchingStringsDiff.call(new_params)

      if organizer.success?
        render json: build_response_from(organizer.project_strings),
               status: :ok
      else
        render json: { errors: organizer.errors },
               status: :internal_server_error
      end
    end

    def retry
      organizer = OrganizeRetryingProjectExport.call(show_params)

      render_with(organizer)
    end

    def show
      organizer = OrganizeFetchingProjectExport.call(show_params)

      render_with(organizer, key: :project_export)
    end

    private

    def build_paginated_response_from(organizer_data, serialized)
      {
        current_page: organizer_data.current_page,
        count: organizer_data.total_count,
        data: ActiveModelSerializers::SerializableResource.new(
          organizer_data,
          each_serializer: serialized
        ),
        total_pages: organizer_data.total_pages
      }
    end

    def build_response_from(organizer_data)
      return organizer_data if params[:check] == 'true'

      serialized = if organizer_data.first.is_a?(ProjectString)
                     UpdatedProjectStringSerializer
                   end

      build_paginated_response_from(organizer_data, serialized)
    end

    def create_params
      {
        current_user_id: current_user.id,
        project_id: params[:project_id]
      }
    end

    #
    # The same controller action can be used to :
    #   * check if there are strings pending for export
    #   * retrieving an export diff (from its show page)
    #
    def diff_organizer_klass
      type = params[:id] ? 'ProjectExport' : 'Strings'

      "Api::OrganizeFetching#{type}Diff".constantize
    end

    def diff_params
      {
        check_only: params[:check] == 'true',
        id: params[:id], # Present only from the ProjectExport show page
        project_id: params[:project_id]
      }
    end

    def index_params
      {
        page: params[:page],
        project_id: params[:project_id]
      }
    end

    def new_params
      {
        page: params[:page],
        project_id: params[:project_id]
      }
    end

    def show_params
      {
        id: params[:id],
        project_id: params[:project_id]
      }
    end

    def update_project_export_failure(errors)
      ProjectExports::UpdateState.call(
        project_id: params[:project_id],
        action: 'export_failure',
        errors: errors
      )
    end
  end
  # rubocop:enable Metrics/ClassLength
end
