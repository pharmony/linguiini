# frozen_string_literal: true

module Api
  class UnusedStringsController < ApplicationController
    def destroy
      organizer = OrganizeProjectStringDestroy.call(
        project_id: params[:project_id],
        id: params[:id]
      )

      if organizer.success?
        render json: build_response_from(organizer), status: :ok
      else
        render json: { errors: organizer.errors },
               status: :unprocessable_entity
      end
    end

    def index
      organizer = OrganizeFetchingProjectUnusedStrings.call(index_params)

      if organizer.success?
        render json: build_response_from(organizer), status: :ok
      else
        render json: { errors: organizer.errors },
               status: :unprocessable_entity
      end
    end

    private

    def build_response_from(organizer)
      return organizer.unused_strings if params[:check] == 'true'

      {
        current_page: organizer.unused_strings.current_page,
        count: organizer.unused_strings.total_count,
        data: ActiveModelSerializers::SerializableResource.new(
          organizer.unused_strings,
          each_serializer: ProjectStringSerializer
        ),
        total_pages: organizer.unused_strings.total_pages
      }
    end

    def index_params
      {
        check_only: params[:check] == 'true',
        id: params[:project_id],
        page: params[:page]
      }
    end
  end
end
