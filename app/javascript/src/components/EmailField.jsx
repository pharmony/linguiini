import React from 'react'
import PropTypes from 'prop-types'

import FormInputField from './FormInputField'
import i18n from '../config/i18n'

class EmailField extends React.Component {
  showAsInvalid(message = null) {
    this.componentField.showAsInvalid(message)
  }

  render() {
    const {
      inputId,
      inputName,
      onChange,
      value
    } = this.props

    return (
      <FormInputField
        inputId={inputId}
        inputName={inputName}
        inputType="email"
        labelText={i18n.t('devise.fields.email')}
        onChange={(event) => onChange(event)}
        ref={(component) => { this.componentField = component }}
        value={value}
      />
    )
  }
}

EmailField.propTypes = {
  inputId: PropTypes.string.isRequired,
  inputName: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
}

export default EmailField
