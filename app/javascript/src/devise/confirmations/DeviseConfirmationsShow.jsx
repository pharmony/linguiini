import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import AlertMessage from '../../components/AlertMessage'
import confirmationActions from './actions'
import DeviseLayout from '../DeviseLayout'
import history from '../../helpers/History'
import i18n from '../../config/i18n'
import PageLoader from '../../components/PageLoader'

class DeviseConfirmationsShow extends React.Component {
  componentDidMount() {
    this.confirmAccountTokenFromParams()
  }

  componentDidUpdate(prevProps) {
    const { confirmingStatus } = this.props

    if (prevProps.confirmingStatus === 'confirming'
        && confirmingStatus === 'succeeded'
    ) {
      history.push('/users/sign_in')
    }
  }

  confirmAccountTokenFromParams() {
    const { dispatch, location: { search } } = this.props

    const urlParams = new URLSearchParams(search)
    const token = urlParams.get('confirmation_token')

    dispatch(confirmationActions.confirm(token))
  }

  render() {
    const { confirmingStatus } = this.props

    return (
      <DeviseLayout
        title={i18n.t('devise.DeviseConfirmationsShow.title')}
      >
        <div className="d-flex flex-column align-items-center mt-5">
          {confirmingStatus === 'failed' ? (
            <AlertMessage>
              {i18n.t('errors.somethingWentWrong')}
            </AlertMessage>
          ) : (
            <PageLoader
              message={i18n.t('devise.DeviseConfirmationsShow.confirming')}
            />
          )}

          <div className="d-flex flex-column w-50 mt-5">
            <Link
              className="mb-3 btn btn-secondary"
              to="/users/sign_in"
            >
              {i18n.t('buttons.connect')}
            </Link>
            <Link
              className="mb-3 btn btn-secondary"
              to="/users/sign_in"
            >
              {i18n.t('buttons.resend_confirmation_email')}
            </Link>
            <Link
              className="mb-3 btn btn-secondary"
              to="/users/confirmation/new"
            >
              {i18n.t('buttons.resend_confirmation_email')}
            </Link>
          </div>
        </div>

      </DeviseLayout>
    )
  }
}

DeviseConfirmationsShow.propTypes = {
  confirmingStatus: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired
  }).isRequired
}

const mapStateToProps = ({
  applicationLayout: {
    location
  },
  confirmation: {
    confirmingStatus
  }
}) => ({
  confirmingStatus,
  location
})

export default connect(mapStateToProps)(DeviseConfirmationsShow)
