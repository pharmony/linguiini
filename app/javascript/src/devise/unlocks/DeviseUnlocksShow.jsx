import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import unlockActions from './actions'

// The unlock feature is not working with NoBrainer I guess, I fixed the eslinter
// offences but can't test this code ...
class DeviseUnlocksShow extends React.Component {
  componentDidMount() {
    const { dispatch, location: { search } } = this.props

    const urlParams = new URLSearchParams(search)
    const token = urlParams.get('unlock_token')

    dispatch(unlockActions.unlock(token))
  }

  render() {
    return (
      <>
        <h2>Unlocking your account ...</h2>
      </>
    )
  }
}

DeviseUnlocksShow.propTypes = {
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired
  }).isRequired
}

export default connect()(DeviseUnlocksShow)
