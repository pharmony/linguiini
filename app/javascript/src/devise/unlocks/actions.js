import deviseConstants from '../constants'
import unlockService from './service'

const resendUnlock = (user) => {
  const request = () => ({ type: deviseConstants.UNLOCK_RESEND_REQUEST })
  const success = () => ({ type: deviseConstants.UNLOCK_RESEND_SUCCESS })
  const failure = (error) => ({
    error,
    type: deviseConstants.UNLOCK_RESEND_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    unlockService.resendUnlock(user)
      .then(() => dispatch(success()))
      .catch((error) => dispatch(failure(error)))
  }
}

const unlock = (token) => {
  const request = () => ({ type: deviseConstants.UNCLOCK_REQUEST })
  const success = () => ({ type: deviseConstants.UNCLOCK_SUCCESS })
  const failure = (error) => ({ error, type: deviseConstants.UNCLOCK_FAILURE })

  return (dispatch) => {
    dispatch(request())

    unlockService.unlock(token)
      .then(() => dispatch(success()))
      .catch((error) => dispatch(failure(error)))
  }
}

export default {
  resendUnlock,
  unlock
}
