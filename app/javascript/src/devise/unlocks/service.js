import ApiUtils from '../../helpers/ApiUtils'
import FormCsrf from '../../helpers/FormCsrf'

function resendUnlock(email) {
  return fetch('/api/users/unlock', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': FormCsrf.csrfToken()
    },
    body: JSON.stringify({ user: { email } })
  }).then(ApiUtils.handleResponse)
}

function unlock(token) {
  return fetch(`/api/users/unlock?unlock_token=${token}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(ApiUtils.handleResponse)
}

export default {
  resendUnlock,
  unlock
}
