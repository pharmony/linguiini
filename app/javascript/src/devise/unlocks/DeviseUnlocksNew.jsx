import React from 'react'
import { Button, FormGroup } from 'reactstrap'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import DeviseLayout from '../DeviseLayout'
import EmailField from '../../components/EmailField'
import i18n from '../../config/i18n'
import PageLoader from '../../components/PageLoader'
import unlockActions from './actions'

class DeviseUnlocksNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: ''
    }
  }

  componentDidUpdate(prevProps) {
    const { sendingInstructionsStatus: prevSendingInstructionsStatus } = prevProps
    const { sendingInstructionsStatus } = this.props

    if (prevSendingInstructionsStatus === 'sending'
        && sendingInstructionsStatus === 'failed') {
      this.showFieldsError()
    }
  }

  handleEmailChange({ target: { value } }) {
    this.setState({
      email: value
    })
  }

  handleSubmit(event) {
    const { dispatch } = this.props
    const { email } = this.state

    event.preventDefault()

    if (email === '') {
      this.emailField.showAsInvalid()
    } else {
      dispatch(unlockActions.resendUnlock(email))
    }
  }

  showFieldsError() {
    const { sendingInstructionsError } = this.props

    if (sendingInstructionsError && 'email' in sendingInstructionsError) {
      this.emailField.showAsInvalid(sendingInstructionsError.email)
    }
  }

  render() {
    const {
      sendingInstructionsStatus
    } = this.props
    const { email } = this.state

    return (
      <DeviseLayout
        onSubmit={(event) => this.handleSubmit(event)}
        title={i18n.t('devise.DeviseUnlocksNew.title')}
      >
        <EmailField
          inputId="email"
          inputName="email"
          onChange={(event) => this.handleEmailChange(event)}
          ref={(element) => { this.emailField = element }}
          value={email}
        />
        <FormGroup>
          <div className="d-flex flex-column">
            {sendingInstructionsStatus === 'sending' && (
              <PageLoader
                message={i18n.t('devise.DeviseUnlocksNew.sending')}
              />
            )}
            <Button
              className="mb-3"
              color="primary"
            >
              {i18n.t('devise.DeviseUnlocksNew.sendButton')}
            </Button>
            <Link
              className="btn btn-secondary"
              to="/users/sign_in"
            >
              {i18n.t('buttons.back')}
            </Link>
          </div>
        </FormGroup>
      </DeviseLayout>
    )
  }
}

DeviseUnlocksNew.propTypes = {
  dispatch: PropTypes.func.isRequired,
  sendingInstructionsError: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  sendingInstructionsStatus: PropTypes.string
}

DeviseUnlocksNew.defaultProps = {
  sendingInstructionsError: null,
  sendingInstructionsStatus: 'idle'
}

const mapStateToProps = ({
  unlock: {
    sendingInstructionsError,
    sendingInstructionsStatus
  }
}) => ({
  sendingInstructionsError,
  sendingInstructionsStatus
})

export default connect(mapStateToProps)(DeviseUnlocksNew)
