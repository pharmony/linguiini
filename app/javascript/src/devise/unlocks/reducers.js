import dotProp from 'dot-prop-immutable'

import deviseConstants from '../constants'

const initialState = {
  sendingInstructionsError: null,
  sendingInstructionsStatus: 'idle'
}

export default (state = initialState, action) => {
  let newState

  switch (action.type) {
    case deviseConstants.UNLOCK_RESEND_REQUEST:
    case deviseConstants.DESTROY_REQUEST:
      newState = dotProp.set(state, 'sendingInstructionsError', null)
      return dotProp.set(newState, 'sendingInstructionsStatus', 'sending')
    case deviseConstants.UNLOCK_RESEND_SUCCESS:
      newState = dotProp.set(state, 'sendingInstructionsError', null)
      return dotProp.set(newState, 'sendingInstructionsStatus', 'succeeded')
    case deviseConstants.UNLOCK_RESEND_FAILURE:
      if (action.error.message === 'null') {
        newState = dotProp.set(
          state,
          'sendingInstructionsError',
          action.error.data
        )
      } else {
        newState = dotProp.set(
          state,
          'sendingInstructionsError',
          action.error.message
        )
      }
      return dotProp.set(newState, 'sendingInstructionsStatus', 'failed')
    default:
      return state
  }
}
