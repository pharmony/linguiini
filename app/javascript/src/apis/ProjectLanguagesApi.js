import RestfulClient from 'restful-json-api-client'

export default class ProjectLanguagesApi extends RestfulClient {
  constructor(projectId) {
    super(`/api/projects/${projectId}`, { resource: 'project_languages' })
  }
}
