import RestfulClient from 'restful-json-api-client'

export default class RepositoriesApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'repositories' })
  }
}
