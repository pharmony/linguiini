import RestfulClient from 'restful-json-api-client'

export default class ProjectStringsApi extends RestfulClient {
  constructor(projectId, language) {
    super(`/api/projects/${projectId}/project_languages/${language}`, {
      resource: 'project_strings'
    })
  }
}
