import RestfulClient from 'restful-json-api-client'

export default class RolesApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'roles' })
  }
}
