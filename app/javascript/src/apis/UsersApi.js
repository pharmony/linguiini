import RestfulClient from 'restful-json-api-client'

export default class UsersApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'users' })
  }
}
