import RestfulClient from 'restful-json-api-client'

export default class ProjectBranchesApi extends RestfulClient {
  constructor(projectId) {
    super(`/api/projects/${projectId}`, { resource: 'branches' })
  }
}
