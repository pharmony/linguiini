import RestfulClient from 'restful-json-api-client'

export default class UsersInvitationApi extends RestfulClient {
  constructor() {
    super('/users', { resource: 'invitation' })
  }
}
