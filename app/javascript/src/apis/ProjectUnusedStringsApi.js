import RestfulClient from 'restful-json-api-client'

export default class ProjectUnusedStringsApi extends RestfulClient {
  constructor(slug) {
    super(`/api/projects/${slug}`, { resource: 'unused_strings' })
  }
}
