import RestfulClient from 'restful-json-api-client'

export default class ProjectLanguageStatsApi extends RestfulClient {
  constructor(projectId, locale) {
    super(`/api/projects/${projectId}/project_languages/${locale}`, {
      resource: 'stats'
    })
  }
}
