import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from 'reactstrap'
import PropTypes from 'prop-types'

import AlertMessage from '../../components/AlertMessage'
import PageLoader from '../../components/PageLoader'
import i18n from '../../config/i18n'
import usersActions from './actions'

class RemoveConfirmationModal extends React.Component {
  componentDidUpdate(prevProps) {
    const { onToggle, removingStatus } = this.props
    const { removingStatus: prevRemovingStatus } = prevProps

    if (prevRemovingStatus === 'removing' && removingStatus === 'succeeded') {
      onToggle()
    }
  }

  handleSubmit(event) {
    const { id, dispatch } = this.props

    event.preventDefault()

    dispatch(usersActions.remove(id))
  }

  render() {
    const {
      removingError,
      removingStatus,
      modalIsOpen,
      onToggle
    } = this.props

    const i18nBase = 'UsersPage.RemoveConfirmationModal'

    return (
      <Modal isOpen={modalIsOpen} toggle={() => onToggle()}>
        <Form>
          <ModalHeader toggle={() => onToggle()}>
            {i18n.t(`${i18nBase}.title`)}
          </ModalHeader>
          <ModalBody>
            {i18n.t(`${i18nBase}.message`)}

            {(removingStatus === 'removing') && (
              <PageLoader message={i18n.t(`${i18nBase}.removing`)} />
            )}

            {removingStatus === 'failed' && (
              <AlertMessage>{removingError}</AlertMessage>
            )}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => onToggle()}>
              {i18n.t('buttons.cancel')}
            </Button>
            <Button
              color="danger"
              onClick={(event) => this.handleSubmit(event)}
              type="submit"
            >
              {i18n.t(`${i18nBase}.buttons.confirm`)}
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    )
  }
}

RemoveConfirmationModal.propTypes = {
  dispatch: PropTypes.func.isRequired,
  id: PropTypes.string,
  modalIsOpen: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired,
  removingError: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  removingStatus: PropTypes.string
}

RemoveConfirmationModal.defaultProps = {
  id: null,
  removingError: null,
  removingStatus: 'idle'
}

const mapStateToProps = ({
  users: {
    removingError,
    removingStatus
  }
}) => ({
  removingError,
  removingStatus
})

export default connect(mapStateToProps)(RemoveConfirmationModal)
