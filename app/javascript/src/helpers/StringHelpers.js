// eslint-disable-next-line import/prefer-default-export
export const truncateString = (string, length) => {
  if (string.length <= length) return string

  return `${string.slice(0, length)} ...`
}
