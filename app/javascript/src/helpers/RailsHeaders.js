import FormCsrf from './FormCsrf'

export default {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'X-CSRF-Token': FormCsrf.csrfToken()
}
