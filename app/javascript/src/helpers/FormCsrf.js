/*
** Form CSRF
*
*  Fetches the CSRF token, generated by Rails, from the page head meta field.
*/

const csrfToken = () => {
  const meta = document.querySelector('meta[name="csrf-token"]')
  return meta ? meta.getAttribute('content') : ''
}

export default {
  csrfToken
}
