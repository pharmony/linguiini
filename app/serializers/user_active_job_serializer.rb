# frozen_string_literal: true

class UserActiveJobSerializer < ActiveJob::Serializers::ObjectSerializer
  def serialize?(argument)
    argument.is_a? User
  end

  def serialize(user)
    super(id: user.id)
  end

  def deserialize(hash)
    User.find(hash['id'])
  end
end
