# frozen_string_literal: true

class ProjectExportSerializer < ActiveModel::Serializer
  attributes :created_at,
             :export_failure_reason,
             :export_failure_trace,
             :id,
             :prev_state,
             :state
end
