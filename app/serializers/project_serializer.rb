# frozen_string_literal: true

class ProjectSerializer < ActiveModel::Serializer
  attributes :id,
             :import_failure_reason,
             :import_stats,
             :import_step,
             :locales,
             :name,
             :paths,
             :repository_branch_name,
             :repository_id,
             :reference_languages,
             :state,
             :strings_stats,
             :to_be_deleted
end
