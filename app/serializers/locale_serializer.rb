# frozen_string_literal: true

class LocaleSerializer < ActiveModel::Serializer
  attributes  :id,
              :label,
              :name,
              :value

  def label
    object.english_name
  end

  def name
    object.english_name
  end

  def value
    object.code
  end
end
