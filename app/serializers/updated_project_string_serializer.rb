# frozen_string_literal: true

class UpdatedProjectStringSerializer < ActiveModel::Serializer
  attributes :id,
             :key,
             :locale,
             :old_value,
             :path,
             :value
end
