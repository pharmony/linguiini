# frozen_string_literal: true

class ProjectStringSerializer < ActiveModel::Serializer
  attributes :conflict,
             :created_at,
             :duplicated,
             :id,
             :key,
             :locale,
             :new_value,
             :path,
             :relative_key,
             :relative_path,
             :value

  def relative_path
    object.path.gsub(%r{^#{Regexp.escape(object.project.local_path)}/}, '')
  end
end
