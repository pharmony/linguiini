# frozen_string_literal: true

class IntegrationSerializer < ActiveModel::Serializer
  attributes :id,
             :platform,
             :state,
             :private_token

  def platform
    object._type.gsub(/Integration$/, '').underscore
  end

  def private_token
    object.token
  end

  def state
    if object.previous_changes&.key?('state')
      return object.previous_changes['state'][1]
    end

    object.state
  end
end
