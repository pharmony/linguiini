# frozen_string_literal: true

class UserSerializer < ActiveModel::Serializer
  attributes :created_at,
             :email,
             :id,
             :invitation,
             :locale,
             :role

  def invitation
    object.invitation_token.present?
  end
end
