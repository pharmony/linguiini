# frozen_string_literal: true

class ProjectsSerializer < ActiveModel::Serializer
  attributes :id,
             :import_failure_reason,
             :import_stats,
             :locales,
             :name,
             :paths,
             :reference_languages,
             :state,
             :strings_stats,
             :to_be_deleted
end
