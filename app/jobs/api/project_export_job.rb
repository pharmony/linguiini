# frozen_string_literal: true

module Api
  class ProjectExportJob < ApplicationJob
    queue_as :exports

    def perform(project_export_id)
      @project_export = find_project_export(project_export_id)

      transit_project_export_to!(:export)

      run_export!
    rescue StandardError => error
      trace = error.backtrace.join("\n")
      Rails.logger.error "ERROR: #{error.message}\n" \
                         "#{trace}"

      transit_project_export_to!(:export_failure, error)
    end

    private

    def broadcast_update_to_ui
      ProjectExportsChannel.broadcast_to(
        @project_export.id,
        ActiveModelSerializers::SerializableResource.new(@project_export)
      )
    end

    def build_export_organizer_class_name
      integration_name = @project_export.project.integration.platform_name
      "Api::Integrations::#{integration_name.classify}::OrganizeExport"
    end

    def build_payload_from(error)
      return unless error

      { reason: build_reason_from(error), trace: error.backtrace.join("\n") }
    end

    def build_reason_from(error)
      if error.respond_to?(:context)
        return error.context.errors if error.context.errors.is_a?(String)

        error.context.errors.map do |field, messages|
          "#{field}: #{messages.to_sentence}"
        end.to_sentence
      else
        error.message
      end.truncate(255) # Prevents state transition failure
    end

    def export_organizer_params
      {
        integration: @project_export.project.integration,
        project: @project_export.project,
        project_export: @project_export
      }
    end

    def find_project_export(id)
      interactor = ProjectExports::Find.call(id: id)

      return interactor.project_export if interactor.success?

      raise interactor.errors
    end

    def run_export!
      build_export_organizer_class_name.constantize.call!(
        export_organizer_params
      )

      transit_project_export_to!(:export_success)
    end

    def transit_project_export_to!(state, error = nil)
      payload = build_payload_from(error)

      @project_export.send("#{state}!", payload)

      broadcast_update_to_ui

      return if @project_export.save

      message = "Unable to transit project export to state '#{state}'"
      message += " with payload #{payload.inspect}" if payload
      message += " : #{@project_export.errors.full_messages}"

      raise message
    end
  end
end
