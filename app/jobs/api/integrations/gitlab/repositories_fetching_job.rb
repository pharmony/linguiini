# frozen_string_literal: true

module Api
  module Integrations
    module Gitlab
      class RepositoriesFetchingJob < ApplicationJob
        queue_as :integrations

        def perform(integration_id)
          @integration = find_integration(integration_id)

          initialize_gitlab
          authenticate!
          fetch!
        rescue StandardError => error
          Rails.logger.error error.message
        end

        private

        def authenticate!
          transit_integration_to!(:authenticate)

          # Retrieves the user from Gitlab (therefore it tries to authenticate)
          ::Gitlab.user

          transit_integration_to!(:authenticate_success)
        rescue ::Gitlab::Error::Unauthorized
          transit_integration_to!(:authenticate_failure)

          raise 'Unable to authenticate against Gitlab for the integration ' \
                "with ID '#{@integration.id}"
        end

        def fetch!
          transit_integration_to!(:fetch)

          projects = ::Gitlab.projects

          if projects.size.zero?
            transit_integration_to!(:fetch_failure)

            raise 'No repository found in Gitlab for the integration ' \
                  "with ID '#{@integration.id}"
          end

          transit_integration_to!(:fetch_success)
        end

        def find_integration(integration_id)
          interactor = Integrations::Find.call(id: integration_id)

          return interactor.integration if interactor.success?

          raise "We can't find the integration with ID '#{integration_id}': " \
                "#{interactor.errors}"
        end

        def initialize_gitlab
          ::Gitlab.configure do |config|
            config.endpoint = 'https://gitlab.com/api/v4'
            config.private_token = @integration.token
          end
        end

        def transit_integration_to!(state)
          @integration.send("#{state}!")

          return if @integration.save

          raise "Unable to transit state: #{@integration.errors.full_messages}"
        end
      end
    end
  end
end
