# frozen_string_literal: true

module Api
  class ProjectLanguageStatsCacheUpdateJob < ApplicationJob
    queue_as :stats

    # `on_conflict: :log` will only write a log and skip the job if there's
    # already a job running.
    unique :until_executing, on_conflict: :log

    def perform(locale_code, id)
      organizer = OrganizeFetchingProjectLanguageStats.call(
        locale_code: locale_code,
        project_id: id
      )

      return true unless organizer.failure?

      raise 'Unable to update ProjectLanguageStatsCache for project with ' \
            "ID #{id} and locale #{locale_code}: #{organizer.errors.inspect}"
    end
  end
end
