# frozen_string_literal: true

module Api
  class ProjectStringsStatsUpdateJob < ApplicationJob
    queue_as :stats

    # `on_conflict: :log` will only write a log and skip the job if there's
    # already a job running.
    unique :until_executed, on_conflict: :log

    def perform(id)
      project = Project.find(id)

      interactor = Projects::UpdateStringsStats.call(project: project)

      if interactor.failure?
        raise "Unable to update project strings_stats with ID #{id}: " \
              "#{interactor.errors.inspect}"
      end

      # Broadcast to the UI the updated stats
      ProjectStatsChannel.broadcast_to(project, project.strings_stats)
    end
  end
end
