# frozen_string_literal: true

module Api
  class ProjectForceImport < ApplicationJob
    queue_as :imports

    def perform(project_id, all_files, git_options)
      @all_files = all_files
      @git_options = git_options
      @project = find_project(project_id)

      transit_project_to!(:import)

      run_import!
    rescue StandardError => error
      Rails.logger.error "ERROR: #{error.message}\n" \
                         "#{error.backtrace.join("\n")}"

      transit_project_to!(:import_failure, error)
    end

    private

    def broadcast_update_to_ui
      ProjectsChannel.broadcast_to(
        'ProjectsChannel',
        ActiveModelSerializers::SerializableResource.new(@project)
      )
    end

    def find_project(project_id)
      interactor = Projects::Find.call(id: project_id)

      return interactor.project if interactor.success?

      raise "We can't find the project with ID '#{project_id}': " \
            "#{interactor.errors}"
    end

    def run_import!
      Api::StringsImports::OrganizeGitlabImport.call!(
        all_files: @all_files,
        git_options: @git_options,
        integration: @project.integration,
        project: @project
      )

      transit_project_to!(:import_success)
    end

    def transit_project_to!(state, error = nil)
      payload = { reason: error&.message }

      @project.send("#{state}!", payload)

      broadcast_update_to_ui

      return if @project.save

      message = "Unable to transit project to state '#{state}'"
      message += " with payload #{payload.inspect}" if payload
      message += " : #{@project.errors.full_messages}"

      raise message
    end
  end
end
