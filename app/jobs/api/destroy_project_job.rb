# frozen_string_literal: true

module Api
  class DestroyProjectJob < ApplicationJob
    queue_as :exports

    def perform(id)
      organizer = OrganizeProjectDestroy.call(id: id)

      return if organizer.success?

      raise "Unable to destroy project with ID #{id}: " \
            "#{organizer.errors.inspect}"
    end
  end
end
