# frozen_string_literal: true

# Add your own tasks in files placed in lib/tasks ending in .rake,
# for example lib/tasks/capistrano.rake, and they will automatically be
# available to Rake.

require_relative 'config/application'

Rails.application.load_tasks

begin
  require 'eslint-webpacker'
  require 'rspec/core/rake_task'
  require 'rubocop/rake_task'

  RSpec::Core::RakeTask.new(:spec)

  # As the following doesn't guarante to be executed in the given order:
  #
  # ```
  # task default: %i[spec cucumber]
  # ```
  #
  # We are clearing the default task and invoke them one by one
  Rake::Task['default'].clear

  task default: [] do
    RuboCop::RakeTask.new(:rubocop)
    Rake::Task['rubocop'].invoke
    Rake::Task['eslint:run'].invoke
    Rake::Task['spec'].invoke
    Rake::Task['cucumber'].invoke
  end
rescue LoadError => error
  unless Rails.env.production?
    puts "WARNING: Could not load testing tools: #{error.message}"
  end
end
