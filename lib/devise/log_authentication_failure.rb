# frozen_string_literal: true

module Devise
  #
  # Sends Slack notification on authentication failures to the app
  #
  class LogAuthenticationFailure < Devise::FailureApp
    def route(_)
      :new_user_sign_in_path
    end

    def respond
      if request.env.dig('warden.options', :action) == 'unauthenticated' &&
         request.env['HTTP_REFERER']&.ends_with?('/users/sign_in')

        Rails.logger.info(build_message)
      end

      super
    end

    private

    def build_message
      email = request.parameters['user'] && request.parameters['user']['email']
      email ||= 'Unknown'

      "Failed login for #{email.inspect} from #{request.remote_ip} at " \
        "#{Time.now.utc.iso8601}"
    end
  end
end
