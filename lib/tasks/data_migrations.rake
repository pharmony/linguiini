# frozen_string_literal: true

#
# The aim of this file is to declare data migration code to be executed after
# a successful deployment using a Kubernetes Job.
# See deploy/helm/templates/jobs.yaml.
#
# After the job has been successfully ran, you should remove your migration task
# but also harden it so that if it runs twice or more, you don't screw the data.
# Be careful with error management too.
#
namespace :data_migrations do # rubocop:disable Metrics/BlockLength
  desc 'Initialize the :created_at field from ProjectString documents'
  task initialize_project_string_created_at: :environment do
    result = nil

    realtime = Benchmark.realtime do
      query = ProjectString.where(:created_at.undefined => true)

      puts "INFO: #{query.count} documents are about to be updated ..."

      # rubocop:disable Rails/SkipsModelValidations
      result = query.update_all(
        created_at: Time.zone.now
      )
      # rubocop:enable Rails/SkipsModelValidations

      if result['errors'].positive?
        raise "ERROR: #{result['errors']} document(s) failed at being updated."
      end
    end

    puts "SUCCESS in #{realtime.round(2)}s (#{result.keys.map do |key|
      "#{key}: #{result[key]}"
    end.join(' | ')})"
  end

  desc 'Fills the missing Integration md5 and sha256 fingerprint fields'
  task update_integration_fingerprint_fields: :environment do # rubocop:disable Metrics/BlockLength
    Integration.all.to_a.each do |integration|
      all_fields_filled = %i[md5 sha256].all? do |type|
        integration.send("ssh_key_fingerprint_#{type}").present?
      end

      if all_fields_filled
        puts "SKIPPED: #{integration._type} fingerprint fields already " \
             'filled in.'

        next
      end

      public_key = integration.ssh_public_key

      unless public_key
        Rails.logger.warn "WARNING: Integration with ID #{integration.id} " \
                          'has `public_key` behing nil, therefore skipping.'

        next
      end

      if integration.update(
        ssh_key_fingerprint_md5: SSHKey.fingerprint(public_key),
        ssh_key_fingerprint_sha256: SSHKey.sha256_fingerprint(public_key)
      )
        puts "SUCCESS: #{integration._type} fingerprint fields " \
             'successfully updated.'
      else
        puts "ERROR: #{integration._type} fingerprint update " \
             "failed: #{integration.errors.full_messages.to_sentence}"
      end
    end
  end
end
