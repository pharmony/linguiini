# frozen_string_literal: true

module UserHelpers
  def create_new_valid_user(email = nil, password = nil)
    email ||= FFaker::Internet.email
    password ||= FFaker::Internet.password

    admin_user = User.new(email: email, password: password,
                          password_confirmation: password)
    admin_user.skip_confirmation!
    admin_user.confirmation_sent_at = Time.zone.now
    admin_user.save!

    admin_user
  end
end
