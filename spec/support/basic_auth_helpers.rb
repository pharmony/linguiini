# frozen_string_literal: true

module BasicAuthHelpers
  def set_request_basic_auth
    env_credentials = Rails.application.secrets
    api_basic_auth = env_credentials[:api_basic_auth]

    request.env['HTTP_AUTHORIZATION'] = encode(
      api_basic_auth[:username],
      api_basic_auth[:password]
    )
  end

  def encode(user, pass)
    ActionController::HttpAuthentication::Basic.encode_credentials(user, pass)
  end
end
