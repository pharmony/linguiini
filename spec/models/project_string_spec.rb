# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProjectString, type: :model do
  subject(:project_string) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    it do
      expect(project_string).to have_field(:conflict)
        .of_type(NoBrainer::Boolean).with_default_value_of(false).required
    end

    it do
      expect(project_string).to have_field(:duplicated)
        .of_type(NoBrainer::Boolean).with_default_value_of(false).required
    end

    it { is_expected.to have_field(:key).of_type(String).required }
    it { is_expected.to have_field(:locale).of_type(String).required }

    it do
      expect(project_string).to have_field(:nested_keys)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end

    it { is_expected.to have_field(:new_value) }
    it { is_expected.to have_field(:new_values_type).of_type(String) }
    it { is_expected.to have_field(:old_value) }
    it { is_expected.to have_field(:path).of_type(String).required }
    it { is_expected.to have_field(:project_id).of_type(String).required }
    it { is_expected.to have_field(:relative_key).of_type(String).required }

    it do
      expect(project_string).to have_field(:translated)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end

    it { is_expected.to have_field(:value) }
    it { is_expected.to have_field(:values_type).of_type(String) }

    it do
      expect(project_string).to have_field(:updated)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end

    it do
      expect(project_string).to have_field(:unused)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end

    it do
      expect(project_string).to have_field(:with_issue)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end
  end

  describe 'validations' do
    it do
      expect(project_string)
        .to validate_uniqueness_of(:key).scoped_to(%(project_id path))
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:project) }
  end

  describe 'indexes' do
    it do
      expect(project_string).to have_index_for(%i[project_id path])
        .named(:key_unicity_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id conflict])
        .named(:project_conflicts_stats_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id duplicated])
        .named(:project_duplicated_stats_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id locale])
        .named(:project_locales_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id conflict value])
        .named(:project_values_conflict_stats_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id updated])
        .named(:project_updated_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id unused])
        .named(:project_unused_compound)
    end

    it do
      expect(project_string).to have_index_for(%i[project_id with_issue])
        .named(:project_with_issues_compound)
    end
  end
end
