# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProjectLanguageStatsCache, type: :model do
  subject(:model) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    it { is_expected.to have_field(:locale_id).of_type(String).required }
    it { is_expected.to have_field(:project_id).of_type(String).required }
    it { is_expected.to have_field(:translateds).of_type(Integer).required }
    it { is_expected.to have_field(:translations).of_type(Integer).required }
  end

  describe 'validations' do
    it do
      expect(model)
        .to validate_uniqueness_of(:locale_id).scoped_to(%(project_id))
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:locale) }
    it { is_expected.to belong_to(:project) }
  end
end
