# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Project, type: :model do
  subject(:project) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    it { is_expected.to have_field(:files_md5s).of_type(Hash) }

    it do
      expect(project).to have_field(:import_failure_reason)
        .of_type(NoBrainer::Text)
    end

    it do
      expect(project).to have_field(:import_stats)
        .of_type(Hash)
        .with_default_value_of(start: nil, end: nil, duration: nil)
    end

    it { is_expected.to have_field(:import_step).of_type(String) }
    it { is_expected.to have_field(:integration_id).of_type(String).required }
    it { is_expected.to have_field(:local_path).of_type(String) }
    it { is_expected.to have_field(:locales).of_type(NoBrainer::Array) }
    it { is_expected.to have_field(:name).of_type(String).required.unique }
    it { is_expected.to have_field(:paths).of_type(String).required }

    it do
      expect(project).to have_field(:reference_languages)
        .of_type(NoBrainer::Array).required
    end

    it { is_expected.to have_field(:repository_id).of_type(Integer).required }

    it do
      expect(project).to have_field(:repository_branch_name).of_type(String)
    end

    it do
      expect(project).to have_field(:state)
        .of_type(String).required.with_default_value_of('pending')
    end

    it do
      expect(project).to have_field(:strings_stats)
        .of_type(Hash)
        .required.with_default_value_of(conflicts: 0, duplicates: 0,
                                        missing_locale_strings: 0,
                                        translated: 0, untranslated: 0)
    end

    it do
      expect(project).to have_field(:to_be_deleted)
        .of_type(NoBrainer::Boolean).with_default_value_of(false)
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:integration) }
    it { is_expected.to have_many(:project_exports) }
    it { is_expected.to have_many(:project_strings) }
  end
end
