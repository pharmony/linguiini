# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProjectExport, type: :model do
  subject(:project_export) { described_class.new }

  it { is_expected.to be_nobrainer_document }
  it { is_expected.to have_timestamps }

  describe 'fields' do
    it { is_expected.to have_field(:export_failure_reason).of_type(String) }

    it do
      expect(project_export).to have_field(:export_failure_trace)
        .of_type(NoBrainer::Text)
    end

    it { is_expected.to have_field(:project_id).of_type(String).required }
    it { is_expected.to have_field(:requester_id).of_type(String) }

    it do
      expect(project_export).to have_field(:state)
        .of_type(String).required.with_default_value_of('pending')
    end

    it { is_expected.to have_field(:updates).of_type(NoBrainer::Array) }

    it do
      expect(project_export).to have_field(:updated_project_string_ids)
        .of_type(NoBrainer::Array)
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:project) }
  end
end
