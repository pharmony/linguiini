# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject(:user) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    context 'when devise' do
      it do
        expect(user).to have_field(:email).of_type(String)
                                          .with_default_value_of('')
                                          .required
                                          .unique
      end

      it do
        expect(user)
          .to have_field(:encrypted_password).of_type(String)
                                             .with_default_value_of('')
                                             .required
      end

      ## Recoverable
      it do
        expect(user).to have_field(:reset_password_token).of_type(String)
                                                         .unique
      end

      it { is_expected.to have_field(:reset_password_sent_at).of_type(Time) }

      ## Rememberable
      it { is_expected.to have_field(:remember_created_at).of_type(Time) }

      ## Trackable
      it do
        expect(user).to have_field(:sign_in_count).of_type(Integer)
                                                  .with_default_value_of(0)
                                                  .required
      end

      it { is_expected.to have_field(:current_sign_in_at).of_type(Time) }
      it { is_expected.to have_field(:last_sign_in_at).of_type(Time) }
      it { is_expected.to have_field(:current_sign_in_ip).of_type(String) }
      it { is_expected.to have_field(:last_sign_in_ip).of_type(String) }

      ## Confirmable
      it do
        expect(user).to have_field(:confirmation_token).of_type(String)
                                                       .unique
      end

      it { is_expected.to have_field(:confirmed_at).of_type(Time) }
      it { is_expected.to have_field(:confirmation_sent_at).of_type(Time) }
      it { is_expected.to have_field(:unconfirmed_email).of_type(String) }

      ## Lockable
      it do
        expect(user).to have_field(:failed_attempts).of_type(Integer)
                                                    .with_default_value_of(0)
                                                    .required
      end

      it do
        expect(user).to have_field(:unlock_token).of_type(String)
                                                 .unique
      end

      it { is_expected.to have_field(:locked_at).of_type(Time) }

      it { is_expected.to have_field(:created_at).of_type(Time).required }
      it { is_expected.to have_field(:updated_at).of_type(Time).required }

      it { is_expected.to have_field(:invitation_token).of_type(String) }
      it { is_expected.to have_field(:invitation_created_at).of_type(Time) }
      it { is_expected.to have_field(:invitation_sent_at).of_type(Time) }
      it { is_expected.to have_field(:invitation_accepted_at).of_type(Time) }
      it { is_expected.to have_field(:invitation_limit).of_type(Integer) }
    end

    context 'when linguiini' do
      it do
        expect(user).to have_field(:locale).of_type(String)
                                           .with_default_value_of('en')
      end

      it do
        expect(user).to have_field(:role).of_type(String)
                                         .required
                                         .to_allow(%w[admin translator])
      end
    end
  end
end
