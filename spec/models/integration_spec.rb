# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Integration, type: :model do
  subject(:integration) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    it do
      expect(
        integration
      ).to have_field(:ssh_key_fingerprint_md5).of_type(NoBrainer::Text)
    end

    it do
      expect(
        integration
      ).to have_field(:ssh_key_fingerprint_sha256).of_type(NoBrainer::Text)
    end

    it { is_expected.to have_field(:ssh_private_key).of_type(NoBrainer::Text) }
    it { is_expected.to have_field(:ssh_public_key).of_type(NoBrainer::Text) }

    it do
      expect(
        integration
      ).to have_field(:state).of_type(String)
                             .required
                             .with_default_value_of('pending')
    end

    it { is_expected.to have_field(:token).of_type(String).required }
  end
end
