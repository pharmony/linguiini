# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Locale, type: :model do
  subject(:locale) { described_class.new }

  it { is_expected.to be_nobrainer_document }

  describe 'fields' do
    it do
      expect(locale).to have_field(:code).of_type(String)
                                         .required
                                         .unique
                                         .of_length((2..10))
    end

    it { is_expected.to have_field(:english_name).of_type(String).required }
  end
end
