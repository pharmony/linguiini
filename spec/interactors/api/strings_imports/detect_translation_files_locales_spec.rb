# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::StringsImports::DetectTranslationFilesLocales do
  let(:locale_codes) { Api::Locales::BuildLocaleCodes.call!.locale_codes }
  let(:project_local_path) { '/my/file/located' }

  before do
    {
      en: 'English',
      fr: 'French',
      fr_FR: 'French (France)',
      de: 'German'
    }.each { |code, name| Locale.create!(code: code, english_name: name) }
  end

  it 'has locale_codes' do
    expect(locale_codes).not_to eql('()')
  end

  describe 'calling the interactor' do
    context 'with a path of a file without any known locale' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/zz.yml' }
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it 'returns context.locale `nil`' do
        expect(context.locale).to be_nil
      end
    end

    context 'with a path where the filename is a known locale' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/fr.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/\w+\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path where the filename is a known locale using ' \
            'underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/fr_FR.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/\w+\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path where the filename is a known locale but using ' \
            'dashes instead of underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/fr-FR.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/\w+\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path including a known locale at the beginning of the ' \
            'filename' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/fr.my.file.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/\w+\.my\.file\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path including a known locale in the middle of the ' \
            'filename' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/my.fr.file.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/my\.\w+\.file\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path including a known locale at the end of the filename' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/my.file.fr.yml' }
      let(:regex_filename) do
        %r{/my/file/located/in/this/folder/my\.file\.\w+\.yml}
      end
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr',
                                        regex_filename: regex_filename,
                                        source: 'filename' })
      end
    end

    context 'with a path including a known locale in the path' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/fr/file.yml' }
      let(:regex_path) { %r{/my/file/located/in/this/\w+/file\.yml} }
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr', regex_path: regex_path,
                                        source: 'path' })
      end
    end

    context 'with a path including a known locale in the path using ' \
            'underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/fr_FR/file.yml' }
      let(:regex_path) { %r{/my/file/located/in/this/\w+/file\.yml} }
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR', regex_path: regex_path,
                                        source: 'path' })
      end
    end

    context 'with a path including a known locale in the path using ' \
            'dashes instead of underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/fr-FR/file.yml' }
      let(:regex_path) { %r{/my/file/located/in/this/\w+/file\.yml} }
      let(:strings) { { tested_with: 'rspec' } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR', regex_path: regex_path,
                                        source: 'path' })
      end
    end

    context 'with a path including a known locale in the strings' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/file.yml' }
      let(:strings) { { 'fr' => { 'tested_with' => 'rspec' } } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr', source: 'infile' })
      end
    end

    context 'with a path including a known locale in the strings using ' \
            'underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/file.yml' }
      let(:strings) { { 'fr_FR' => { 'tested_with' => 'rspec' } } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR', source: 'infile' })
      end
    end

    context 'with a path including a known locale in the strings using ' \
            'dashes instead of underscores' do
      subject(:context) do
        described_class.call(
          locale_codes: locale_codes,
          path: path,
          project_local_path: project_local_path,
          strings: strings
        )
      end

      let(:path) { '/my/file/located/in/this/folder/file.yml' }
      let(:strings) { { 'fr-FR' => { 'tested_with' => 'rspec' } } }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it "returns context.locale with code 'fr_FR' and source 'filename'" do
        expect(context.locale).to eql({ code: 'fr_FR', source: 'infile' })
      end
    end
  end
end
