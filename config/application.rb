# frozen_string_literal: true

require_relative 'boot'

# require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
# require 'active_record/railtie'
# require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
# require 'sprockets/railtie'
# require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Linguiini
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # When you'd like to create RethinkDB indexes, you need to uncomment the
    # following line
    # config.autoloader = :classic

    # Settings in config/environments/* take precedence over those specified
    # here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.autoload_paths << Rails.root.join('lib')

    # Don't generate system test files.
    config.generators.system_tests = nil

    # Make sure devise accepts json requests
    config.to_prepare do
      DeviseController.respond_to :json
    end

    # Use Sidekiq for background tasks
    config.active_job.queue_adapter = :sidekiq

    # When LINGUIINI_EXPORT_BRANCH_NAME is different than `nil`, `""` or `\"\"`
    # we use it, otherwise name the export branch `linguiini/export`.
    config.x.linguiini.branch_name = ENV.fetch(
      'LINGUIINI_EXPORT_BRANCH_NAME',
      ''
    ).gsub(/\"/, '').presence || 'linguiini/export'

    if defined?(Rails::Server)
      config.after_initialize do
        NoBrainer.sync_indexes
      rescue RethinkDB::ReqlOpFailedError,
             NoBrainer::Error::DocumentNotPersisted
        sleep 0.1 # sleep for 100ms
        retry
      end
    end
  end
end
