# frozen_string_literal: true

class Symbol
  delegate :number?,
           :to_i,
           to: :to_s
end
