# frozen_string_literal: true

Git.configure do |config|
  if Rails.env.test?
    # We're using a bash script to mock git clone/pull in the tests
    config.binary_path = '/application/bin/git'
  end
end
