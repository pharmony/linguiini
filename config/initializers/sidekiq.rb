# frozen_string_literal: true

require 'sidekiq'
require 'sidekiq/web'

Sidekiq.default_job_options = {
  backtrace: true,
  retry: false
}

redis_conf = Rails.application.config_for(:redis)

redis_url = 'redis://'
redis_url += ":#{redis_conf['password']}@" if redis_conf['password'].present?
redis_url += "#{redis_conf['host']}:#{redis_conf['port']}/"
redis_url += redis_conf['db'].to_s

message = 'Setting Redis at '
message += redis_url
if redis_conf['namespace']
  message += ' with namespace "'
  message += redis_conf['namespace']
  message += '"'
end
Rails.logger.info message

rails_credential = Rails.application.secrets
sidekiq_creds = rails_credential[:sidekiq] if rails_credential
if sidekiq_creds
  Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    # Protect against timing attacks:
    # - See https://codahale.com/a-lesson-in-timing-attacks/
    # - See https://thisdata.com/blog/timing-attacks-against-string-comparison/
    # - Use & (do not use &&) so that it doesn't short circuit.
    # - Use digests to stop length information leaking
    Rack::Utils.secure_compare(
      ::Digest::SHA256.hexdigest(user),
      ::Digest::SHA256.hexdigest(sidekiq_creds[:user])
    ) &
      Rack::Utils.secure_compare(
        ::Digest::SHA256.hexdigest(password),
        ::Digest::SHA256.hexdigest(sidekiq_creds[:password])
      )
  end
else
  Rails.logger.warn '[Sidekiq] WARNING: No credentials provided, accessing ' \
                    'the web UI is open to the world!'
end

Kernel.puts "Connecting Sidekiq to Redis URL #{redis_url} ..."

ENV['REDIS_URL'] = redis_url

Sidekiq.configure_server do |config|
  config.redis = { url: redis_url }
  config.average_scheduled_poll_interval = 5

  config.on(:startup) do
    FileUtils.touch(Rails.root.join('tmp/sidekiq_is_alive'))
  end

  config.on(:shutdown) do
    Kernel.puts '--> Sidekiq has received the USR1 signal!'
  end

  config.on(:shutdown) do
    Kernel.puts '--> Sidekiq has received the TERM signal!'
    FileUtils.rm_f(Rails.root.join('tmp/sidekiq_is_alive'))
  end
end

Sidekiq.configure_client do |config|
  config.redis = { url: redis_url }
end

schedule_file = 'config/schedule.yml'
if File.exist?(schedule_file) && Sidekiq.server?
  yaml = YAML.load_file(schedule_file)

  Sidekiq::Cron::Job.load_from_hash(yaml) if yaml
end
