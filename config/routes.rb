# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  devise_for :users,
             controllers: {
               invitations: 'devise/invitations'
             },
             skip: :sessions # Do not create all sessions routes
  # And re-create the sessions routes we need.
  # Otherwise when refreshing the login page, the browser access the devise gem
  # HTML views.
  devise_scope :user do
    post 'users/sign_in', to: 'devise/sessions#create'
    delete 'users/sign_out', to: 'devise/sessions#destroy'
  end

  # ~~~~ Application API ~~~~
  #
  # Declare all your resources in the `:api` scope.
  # The API scope calls controllers executing requested actions from the UI.
  # (Create a post, delete a user, and so on ...).
  #
  # Frontend routes are defined in the JavaScript application's router.
  # @see get '*path' at the bottom of this file.
  #
  # `scope :api, module: :api ...` instead of `namespace :api, ...` in order to
  # keep named routes without the `_api_` part.
  # (`new_user_session_path` instead of `new_api_user_session_path`).
  scope :api, module: :api, constraints: { format: 'json' } do
    # ~~~~ Application Resources ~~~~
    resources :integrations, only: %i[create index update] do
      collection do
        post :check_paths
      end
    end
    resources :locales, only: %i[create destroy index update] do
      collection do
        post :check
      end
    end
    resources :projects, only: %i[create destroy show index update] do
      collection do
        post :check
        post :force_import
      end

      member do
        put :fixes_alert_project
        post 'refresh-stats', action: 'refresh'
      end

      resources :branches, only: %i[index]
      resources :project_exports, only: %i[index show new create] do
        collection do
          get :diff
        end
        member do
          get :diff
          post :retry
        end
      end
      resources :project_languages, only: %i[create index show] do
        resources :project_strings, only: %i[index update]
        resource :stats, only: %i[show]
      end
      resources :project_strings, only: %i[update]
      resources :strings_alerts, only: %i[index create destroy] do
        post :keep
      end
      resources :unused_strings, only: %i[index destroy]
    end
    resources :repositories, only: %i[index]
    resources :roles, only: %i[index]
    resources :users, only: %i[index destroy update]
  end

  # ~~~~ Devise URL overrides ~~~~
  #
  # Override the Devise confirmation URL so that it doesn't include the `/api/`
  # part of the URL since the `devise_for` is included in the `:api` scope.
  # (See above)
  get '/users/:id/confirmation', to: 'home#index', as: :confirmation
  get '/users/:id/password/edit', to: 'home#index', as: :edit_password
  get '/users/:id/unlock', to: 'home#index', as: :unlock
  get '/users/:id/invitation/accept', to: 'home#index', as: :accept_invitation
  get '/users/sign_in', to: 'home#index', as: :new_user_sign_in

  # Application root is required.
  root to: 'home#index'

  # Forward everything else to the JavaScript application router
  get '*path', to: 'home#index'
end
