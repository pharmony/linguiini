process.env.NODE_ENV = process.env.NODE_ENV || 'production'

const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')

const environment = require('./environment')

environment.plugins.append('HardSourceWebpackPlugin', new HardSourceWebpackPlugin())

module.exports = environment.toWebpackConfig()
