const { environment } = require('@rails/webpacker')

const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

/*
** Plugins
*/
environment.plugins.prepend('MomentLocalesPlugin', new MomentLocalesPlugin({
  localesToKeep: [
    'es-us'
  ]
}))

environment.loaders.append('json-locales', {
  test: /locales/,
  loader: '@alienfast/i18next-loader'
})

module.exports = environment
