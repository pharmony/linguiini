FROM cablespaghetti/helmfile-docker:3.8.1

RUN --mount=type=secret,id=pharmony-gpg \
    gpg --import /run/secrets/pharmony-gpg

RUN apt update \
    && apt install nano
