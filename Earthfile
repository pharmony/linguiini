# This allows one to change the running Ruby version with:
#
VERSION 0.6

# `earthly --allow-privileged +rspec --EARTHLY_RUBY_VERSION=3`
ARG EARTHLY_RUBY_VERSION=2.7
FROM ruby:$EARTHLY_RUBY_VERSION-alpine3.14

ENV APP_HOME /application
ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL C.UTF-8
ENV TZ=Europe/Paris

WORKDIR /application

# This stage install/configure the base of the system, which will end in
# a Docker image used in any other stages.
deps:
    ARG DEV_PACKAGES="bash c-ares curl git openssh-client shared-mime-info"
    ARG PACKAGES="tzdata"

    RUN apk add --no-cache --update $DEV_PACKAGES \
                                    $PACKAGES \
        && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
        && echo $TZ > /etc/timezone \
        # Add a group or add a user to a group
        #
        #  -g GID   Group id
        #  -S       Create a system group
        #
        # Create new user, or add USER to GROUP
        #
        #  -h DIR   Home directory
        #  -g GECOS GECOS field
        #  -s SHELL Login shell
        #  -G GRP   Group
        #  -S       Create a system user
        #  -D       Don't assign a password
        #  -H       Don't create home directory
        #  -u UID   User id
        #  -k SKEL  Skeleton directory (/etc/skel)
        && addgroup -S linguiini \
        && adduser -s /bin/sh \
                   -G linguiini \
                   -S \
                   -u 1000 \
                   linguiini \
        && chown -R linguiini:linguiini $APP_HOME \
        && chown -R linguiini:linguiini $HOME

# This stage install all gem dependencies and then install the gems from
# the Gemfile and Gemfile.lock.
# Finally only the installed gems are persisted
gems:
    FROM +deps

    RUN apk add --no-cache --update build-base \
        && touch ~/.gemrc \
        && echo "gem: --no-ri --no-rdoc" >> ~/.gemrc \
        && gem install rubygems-update -v 3.4.22 \
        && update_rubygems

    ARG BUNDLER_VERSION=2.1.4

    RUN gem install bundler:${BUNDLER_VERSION} \
        && bundle config set without 'development test'

    COPY Gemfile* $APP_HOME

    RUN bundle install --jobs $(nproc)

    SAVE ARTIFACT /usr/local/bundle bundle
    SAVE ARTIFACT Gemfile
    SAVE ARTIFACT Gemfile.lock

# This target install Node JS and Yarn and saves it as an artifact
node:
    FROM +deps

    RUN apk add --no-cache --update nodejs \
                                    yarn

    SAVE ARTIFACT /usr/bin/node node
    SAVE ARTIFACT /usr/share/systemtap/tapset/node.stp node.stp
    # Yarn lives in the node_modules folder and a `yarn` and `yarnpkg` symblinks
    # are created in the `/usr/bin` folder.
    # Here we're just saving the `node_modules` folder and the symblinks will be
    # re-created at the right place, otherwise Earthly saves the content of the
    # file, not the symblink, which breaks Yarn
    SAVE ARTIFACT /usr/share/node_modules node_modules

# This target install project's Node packages and saves the generated packs
# folder as an artifact
yarn:
    FROM +node

    ENV RAILS_ENV production

    COPY app/javascript app/javascript
    COPY bin/webpack bin/webpack
    COPY config/webpack config/webpack
    COPY config/webpacker.yml config/webpacker.yml
    COPY babel.config.js babel.config.js
    COPY postcss.config.js postcss.config.js
    COPY package.json $APP_HOME
    COPY yarn.lock $APP_HOME
    COPY +gems/Gemfile $APP_HOME/Gemfile
    COPY +gems/Gemfile.lock $APP_HOME/Gemfile.lock
    COPY +gems/bundle /usr/local/bundle

    RUN yarn \
        && RAILS_ENV=production NODE_ENV=production bin/webpack

    SAVE ARTIFACT node_modules
    SAVE ARTIFACT public/packs packs
    SAVE ARTIFACT package.json
    SAVE ARTIFACT yarn.lock

# This target builds the production candidate Docker image
prod:
    FROM +deps

    ENV RAILS_ENV production

    COPY +node/node /usr/bin/node
    COPY +node/node.stp /usr/share/systemtap/tapset/node.stp

    COPY +node/node_modules /usr/share/node_modules
    RUN ln -s /usr/share/node_modules/yarn/bin/yarn /usr/bin/yarn \
        && ln -s /usr/share/node_modules/yarn/bin/yarn /usr/bin/yarnpkg

    COPY --chown linguiini:linguiini +gems/Gemfile Gemfile
    COPY --chown linguiini:linguiini +gems/Gemfile.lock Gemfile.lock
    COPY +gems/bundle /usr/local/bundle

    COPY --chown linguiini:linguiini public public
    COPY --chown linguiini:linguiini +yarn/packs public/packs

    COPY --chown linguiini:linguiini app app
    COPY --chown linguiini:linguiini bin bin
    COPY --chown linguiini:linguiini config config
    COPY --chown linguiini:linguiini db db
    COPY --chown linguiini:linguiini lib lib
    COPY --chown linguiini:linguiini storage storage
    COPY --chown linguiini:linguiini .browserslistrc .browserslistrc
    COPY --chown linguiini:linguiini .eslintrc .eslintrc
    COPY --chown linguiini:linguiini .rspec .rspec
    COPY --chown linguiini:linguiini .rubocop.yml .rubocop.yml
    COPY --chown linguiini:linguiini babel.config.js babel.config.js
    COPY --chown linguiini:linguiini config.ru config.ru
    COPY --chown linguiini:linguiini postcss.config.js postcss.config.js
    COPY --chown linguiini:linguiini Rakefile Rakefile

    USER linguiini

    RUN mkdir log/

    VOLUME $APP_HOME

    ENTRYPOINT ["bundle", "exec"]
    CMD ["bin/bootstrap"]

    ARG TAG=latest
    SAVE IMAGE --push registry.gitlab.com/pharmony/linguiini:$TAG

# This target installs the test suite dependencies
test:
    FROM +prod

    ENV NODE_ENV test
    ENV RAILS_ENV test

    USER root

    RUN rm -f /usr/local/bundle/config \
        && bundle config set without 'development' \
        && bundle install --jobs $(nproc)

    USER linguiini

    COPY --chown linguiini:linguiini package.json package.json
    COPY --chown linguiini:linguiini yarn.lock yarn.lock
    RUN rm -rf public/packs* \
        && yarn \
        && bin/webpack

    COPY features features
    COPY spec spec

    CMD ["rake"]

    ARG TAG=latest
    SAVE IMAGE --push registry.gitlab.com/pharmony/linguiini:$TAG-test

#
# This target is only used in order to publish the prod and test images, pushed
# to the registry.
#
# earthly +deploy --TAG=1.2.3
deploy:
   BUILD +prod
   BUILD +test

#
# This target runs all the tests suites.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +rake --COMPOSE_HTTP_TIMEOUT=120
rake:
    FROM earthly/dind:alpine

    ARG COMPOSE_HTTP_TIMEOUT

    COPY docker-compose.yml ./
    COPY docker-compose-earthly.yml ./

    # --load will load the given docker image in the Eartly's Docker instance.
    # The left part of the "=" is the name that the Eartly's Docker instance
    # will have, the right part of the "=" is from where this image is loaded.
    #
    # Here we are taking the image from the "test" target and name it
    # registry.gitlab.com/pharmony/linguiini:test which is the "image" name
    # defined in the docker-compose-earthly.yml file.
    #
    #
    # --pull Pull Docker images that the app depends on and cache them
    # in Earthly so that it will not re-download them again and again.
    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+test \
                --pull rethinkdb:2.4-buster-slim \
                --pull selenium/standalone-chrome:3.141.59-antimony
        RUN docker-compose -f docker-compose-earthly.yml run \
            --rm app \
            sh -c 'bundle exec rake'
    END

#
# This target runs the Rubocop static code analysis tool.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +rubocop --COMPOSE_HTTP_TIMEOUT=120
rubocop:
    FROM earthly/dind:alpine

    ARG COMPOSE_HTTP_TIMEOUT

    COPY docker-compose.yml ./
    COPY docker-compose-earthly.yml ./

    # --load will load the given docker image in the Eartly's Docker instance.
    # The left part of the "=" is the name that the Eartly's Docker instance
    # will have, the right part of the "=" is from where this image is loaded.
    #
    # Here we are taking the image from the "test" target and name it
    # registry.gitlab.com/pharmony/linguiini:test which is the "image" name
    # defined in the docker-compose-earthly.yml file.
    #
    #
    # --pull Pull Docker images that the app depends on and cache them
    # in Earthly so that it will not re-download them again and again.
    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+test \
                --pull rethinkdb:2.4-buster-slim \
                --pull selenium/standalone-chrome:3.141.59-antimony
        RUN docker-compose -f docker-compose-earthly.yml run \
            --rm app \
            sh -c 'bundle exec rubocop'
    END

#
# This target runs the Eslint static code analyser.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +eslint
eslint:
    FROM earthly/dind:alpine

    ARG COMPOSE_HTTP_TIMEOUT

    COPY docker-compose.yml ./
    COPY docker-compose-earthly.yml ./

    # --load will load the given docker image in the Eartly's Docker instance.
    # The left part of the "=" is the name that the Eartly's Docker instance
    # will have, the right part of the "=" is from where this image is loaded.
    #
    # Here we are taking the image from the "test" target and name it
    # registry.gitlab.com/pharmony/linguiini:test which is the "image" name
    # defined in the docker-compose-earthly.yml file.
    #
    #
    # --pull Pull Docker images that the app depends on and cache them
    # in Earthly so that it will not re-download them again and again.
    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+test \
                --pull rethinkdb:2.4-buster-slim \
                --pull selenium/standalone-chrome:3.141.59-antimony
        RUN docker-compose -f docker-compose-earthly.yml run \
            --rm app \
            sh -c 'bundle exec rake eslint:run'
    END

#
# This target runs the Rspec test suite.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +rspec --COMPOSE_HTTP_TIMEOUT=120
rspec:
    FROM earthly/dind:alpine

    ARG COMPOSE_HTTP_TIMEOUT

    COPY docker-compose.yml ./
    COPY docker-compose-earthly.yml ./

    # --load will load the given docker image in the Eartly's Docker instance.
    # The left part of the "=" is the name that the Eartly's Docker instance
    # will have, the right part of the "=" is from where this image is loaded.
    #
    # Here we are taking the image from the "test" target and name it
    # registry.gitlab.com/pharmony/linguiini:test which is the "image" name
    # defined in the docker-compose-earthly.yml file.
    #
    #
    # --pull Pull Docker images that the app depends on and cache them
    # in Earthly so that it will not re-download them again and again.
    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+test \
                --pull rethinkdb:2.4-buster-slim \
                --pull selenium/standalone-chrome:3.141.59-antimony
        RUN docker-compose -f docker-compose-earthly.yml run \
            --rm app \
            sh -c 'bundle exec rspec'
    END

#
# This target runs the Cucumber test suite.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +cucumber --COMPOSE_HTTP_TIMEOUT=120
cucumber:
    FROM earthly/dind:alpine

    ARG COMPOSE_HTTP_TIMEOUT
    ARG CUCUMBER_FILES

    COPY docker-compose.yml ./
    COPY docker-compose-earthly.yml ./

    # The docker-compose-earthly.yml file mounts this log folder in the `app`
    # service's container and will allow us to grab the Rails log file.
    RUN mkdir $APP_HOME/log \
      && touch $APP_HOME/log/test.log \
      && chown 1000:1000 $APP_HOME/log/test.log \
      && chmod 0664 $APP_HOME/log/test.log

    # --load will load the given docker image in the Eartly's Docker instance.
    # The left part of the "=" is the name that the Eartly's Docker instance
    # will have, the right part of the "=" is from where this image is loaded.
    #
    # Here we are taking the image from the "test" target and name it
    # registry.gitlab.com/pharmony/pharmony-one:test which is the "image" name
    # defined in the docker-compose-earthly.yml file.
    #
    #
    # --pull Pull Docker images that the app depends on and cache them
    # in Earthly so that it will not re-download them again and again.
    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+test \
                --pull rethinkdb:2.4-buster-slim \
                --pull selenium/standalone-chrome:3.141.59-antimony
        RUN set -e \
          && echo 0 > exit_code \
          && (docker-compose -f docker-compose-earthly.yml run \
              --rm \
              --use-aliases \
              app \
              sh -c "bundle exec rake nobrainer:sync_schema RAILS_ENV=test \
                      && bundle exec rake nobrainer:seed RAILS_ENV=test \
                      && bundle exec cucumber $CUCUMBER_FILES" \
              || echo $? > exit_code)
    END

    SAVE ARTIFACT exit_code AS LOCAL exit_code
    SAVE ARTIFACT ./log/* AS LOCAL ./log/

    IF [ "$(cat exit_code)" != "0" ]
        SAVE ARTIFACT ./tmp/capybara/* AS LOCAL ./tmp/capybara/
    END

#
# This target runs GitLab tool for running Code Quality checks on provided
# source code.
#
# See https://gitlab.com/gitlab-org/ci-cd/codequality
#
# Use the following command in order to run it:
# earthly --allow-privileged +codequality
codequality:
    FROM earthly/dind:alpine

    ARG APP_HOME=/application

    WORKDIR $APP_HOME

    ARG CODE_QUALITY_IMAGE_TAG=0.85.23

    ARG CI_PROJECT_DIR

    WITH DOCKER --load registry.gitlab.com/pharmony/linguiini:test=+prod \
                --pull registry.gitlab.com/gitlab-org/ci-cd/codequality:$CODE_QUALITY_IMAGE_TAG
        RUN docker run --name linguiini \
                       --entrypoint= \
                       registry.gitlab.com/pharmony/linguiini:test \
                       echo "Application source code loading success in $APP_HOME" \
            && echo "Running codequality against $APP_HOME ..." \
            && docker run --rm \
                          --env SOURCE_CODE=$APP_HOME \
                          --env REPORT_STDOUT=true \
                          --volumes-from linguiini \
                          --volume /var/run/docker.sock:/var/run/docker.sock \
                          registry.gitlab.com/gitlab-org/ci-cd/codequality:$CODE_QUALITY_IMAGE_TAG \
                          $APP_HOME > gl-code-quality-report.json
    END

    SAVE ARTIFACT gl-code-quality-report.json AS LOCAL $CI_PROJECT_DIR/gl-code-quality-report.json

# This target install all the development environment tools and creates the
# Docker image to be used with docker-compose.yml
dev:
    FROM +prod

    ENV NODE_ENV development
    ENV RAILS_ENV development

    COPY --chown linguiini:linguiini +yarn/node_modules node_modules
    COPY --chown linguiini:linguiini +yarn/package.json package.json
    COPY --chown linguiini:linguiini +yarn/yarn.lock yarn.lock

    USER root

    RUN apk add --no-cache --update build-base \
        && rm -f /usr/local/bundle/config \
        # Removes production packs folder
        && rm -rf public/packs \
        && bundle install --jobs $(nproc)

    USER linguiini

    RUN yarn

    COPY features features
    COPY spec spec

    ARG IMAGE_NAME=registry.gitlab.com/pharmony/linguiini:dev
    SAVE IMAGE $IMAGE_NAME

