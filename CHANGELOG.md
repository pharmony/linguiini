# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.8.1] - 2024-12-10
### Updated
- Gitlab Access Token to pull Linguiini's Docker images

## [0.2.8] - 2024-01-25
### Added
- Init container to `chown` the home folder in the Sidekiq Pod

## [0.2.7] - 2024-01-24
### Added
- `chown` for `linguiini` user on its home folder (`/home/linguiini`)

### Removed
- Kubernetes NetworkPolicy manifests

## [0.2.6] - 2024-01-23
### Added
- ingressClassName to ingress so that it works within the new Kubernetes cluster managed by OVH

## [0.2.5] - 2024-01-22
### Added
- `rubygems-update` gem version preventing the build from failing due to the Ruby version

## [0.2.4] - 2024-01-22
### Added
- Creation of translation files for other reference languages #8
- Tilt configuration for local dev in Kubernetes

### Changed
- Only tags are deployed to the production environment

### Fixed
- Strings import having a dash in its locale #7

## [0.2.3] - 2023-02-07
### Added
- Display a message when a project has no languages yet
- Translation string for the project's languages loader
- Searching translations by value #5

### Fixed
- Stat bar "Refresh" button style
- Conflict screen search form style

## [0.2.2] - 2023-02-03
### Fixed
- Detecting existing Gitlab Deploy Key #6

## [0.2.1] - 2023-02-03
### Added
- Locale auto detection on importing project strings
- Sorting strings by created_at #4

## [0.2.0] - 2022-03-10
### Added
- Force import button for the admin users
- Imports conflicts and errors page

### Changed
- Renamed the app to Linguiini

## [0.1.14] - 2021-06-17
### Added
- NetworkPolicy allowing ACME HTTP01 challenge

## [0.1.13] - 2021-06-16
### Removed
- App version label from Sidekiq StatefulSet

## [0.1.12] - 2021-06-16
### Fixed
- Pods labels and NetworkPolicy labelSelectors

## [0.1.11] - 2021-06-16
### Changed
- Updated Kubernetes NetworkPolicy files

## [0.1.10] - 2021-06-04
### Added
- Kubernetes NetworkPolicy files

## [0.1.9] - 2021-06-04
### Added
- Kubernetes PSP support

### Changed
- Gitlab CI pipeline improvment

## [0.1.8] - 2021-05-26
### Fixed
- Helm chart and Gitlab CI pipeline

## [0.1.7] - 2021-05-11
### Fixed
- Center login form

## [0.1.6] - 2021-05-11
### Fixed
- Fixes setting the Docker image tag

## [0.1.5] - 2021-05-11
### Fixed
- Fixes setting the Docker image tag

## [0.1.4] - 2021-05-11
### Fixed
- Fixes using Earthly explicit cache

## [0.1.3] - 2021-05-11
### Changed
- Improves the Helm/Helmfile stack

## [0.1.2] - 2021-05-10
### Fixed
- When no RAILS_SMTP_DOMAIN is defined

## [0.1.1] - 2021-05-10
### Added
- Gitlab CI workflow to build, test and deploy the app

## [0.1.0] - 2021-02-26
### Added
- Users page
- Settings/Integrations page
- Projects page

[Unreleased]: https://gitlab.com/pharmony/linguiini/compare/v0.2.8.1...master
[0.2.8.1]: https://gitlab.com/pharmony/linguiini/compare/v0.2.8...v0.2.8.1
[0.2.8]: https://gitlab.com/pharmony/linguiini/compare/v0.2.7...v0.2.8
[0.2.7]: https://gitlab.com/pharmony/linguiini/compare/v0.2.6...v0.2.7
[0.2.6]: https://gitlab.com/pharmony/linguiini/compare/v0.2.5...v0.2.6
[0.2.5]: https://gitlab.com/pharmony/linguiini/compare/v0.2.4...v0.2.5
[0.2.4]: https://gitlab.com/pharmony/linguiini/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/pharmony/linguiini/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/pharmony/linguiini/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/pharmony/linguiini/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/pharmony/linguiini/compare/v0.1.14...v0.2.0
[0.1.14]: https://gitlab.com/pharmony/linguiini/compare/v0.1.13...v0.1.14
[0.1.13]: https://gitlab.com/pharmony/linguiini/compare/v0.1.12...v0.1.13
[0.1.12]: https://gitlab.com/pharmony/linguiini/compare/v0.1.11...v0.1.12
[0.1.11]: https://gitlab.com/pharmony/linguiini/compare/v0.1.10...v0.1.11
[0.1.10]: https://gitlab.com/pharmony/linguiini/compare/v0.1.9...v0.1.10
[0.1.9]: https://gitlab.com/pharmony/linguiini/compare/v0.1.8...v0.1.9
[0.1.8]: https://gitlab.com/pharmony/linguiini/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/pharmony/linguiini/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/pharmony/linguiini/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/pharmony/linguiini/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/pharmony/linguiini/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/pharmony/linguiini/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/pharmony/linguiini/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/pharmony/linguiini/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/pharmony/linguiini/-/tags/v0.1.0
